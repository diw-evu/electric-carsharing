# %% Import required packages
import os
import pandas as pd
from pathlib import Path 

cwd = os.getcwd()
print("Current working directory: {0}".format(cwd))

# %% Load data
df = pd.read_csv('/temp/SeqResults/midtrips_clean.csv', sep=';', low_memory=False, decimal=',', index_col=[0])
df=pd.DataFrame(df)
seq = pd.read_csv('/temp/SeqResults/sequences_zweckmop.csv', sep=';', low_memory=False, index_col=[0])
# %% Select relevant variables and indexes
df=df[['HP_ID', 'W_ID', 'wegkm_imp', 'anzpers']] # Select relevant variable
ind = seq['HP_ID'].tolist() # Select indexes that are in the sequence dataset 
df = df.loc[df['HP_ID'].isin(ind)]
# %% Compute the total number of trips for each individual
ntrips = pd.DataFrame.groupby(df, df['HP_ID']).agg({'W_ID': 'nunique', 'wegkm_imp': 'sum', 'anzpers': 'mean'})
ntrips = pd.DataFrame.rename(ntrips, columns={'W_ID': 'ntrips', 'wegkm_imp': 'distance', 'anzpers': 'npers'}) 
# %% Export the dataset 
ntrips.to_csv('/temp/SeqResults/ntrips.csv', index=True, sep=';')

# %% Add ntrips and distance variables to the sequence dataset
sequences_zweckmop = pd.read_csv('/temp/SeqResults/sequences_zweckmop.csv', sep=';', low_memory=False, index_col=[0])
sequences_zweckmop = pd.merge(sequences_zweckmop, ntrips, how='left', left_on='HP_ID', right_on='HP_ID')

#%% Deleting sequences where people are idle all day long 
g = sequences_zweckmop.columns.get_loc('00:00')
h = sequences_zweckmop.columns.get_loc('23:55')
sequences_zweckmop['total'] = sequences_zweckmop.iloc[:,g:h].sum(axis=1)
always_idle = sequences_zweckmop.index[sequences_zweckmop['total']==0].tolist()
sequences_zweckmop_c = pd.DataFrame.drop(sequences_zweckmop, index=always_idle)
#%% Export the dataset
sequences_zweckmop_c.to_csv('/temp/SeqResults/sequences_zweckmop_c.csv', index=True, sep=';')

#%% Number of trips and total travelled distance (7am-10pm) 
# Load data
df = pd.read_csv('/temp/SeqResults/midtrips_clean.csv', sep=';', low_memory=False, decimal=',', index_col=[0])
df=pd.DataFrame(df)
seqc = pd.read_csv('/temp/SeqResults/sequences_zweckmop_c.csv', sep=';', low_memory=False, index_col=[0])
# %% Select relevant variables and indexes
df=df[['HP_ID', 'W_ID', 'wegkm_imp', 'W_SZS', 'W_AZS', 'anzpers']] # Select relevant variable
ind = seqc['HP_ID'].tolist() # Select indexes that are in the sequence dataset 
df = df.loc[df['HP_ID'].isin(ind)]
#%% Filter out trips 
before7 = df.index[(df['W_SZS'] < 7) & (df['W_AZS'] < 7)].tolist()  # Started and ended before 7am
after10 = df.index[(df['W_SZS'] > 21) & (df['W_AZS'] > 21)].tolist() # Started and ended after 10pm
list722 = before7 + after10
df = pd.DataFrame.drop(df, index=list722)
# %% Compute number of trips and total travelled distance between 7am and 10pm for each individual
dist722 = pd.DataFrame.groupby(df, df['HP_ID']).agg({'W_ID': 'nunique', 'wegkm_imp': 'sum', 'anzpers': 'mean'})
dist722 = pd.DataFrame.rename(dist722, columns={'W_ID': 'ntrips722_mid', 'wegkm_imp': 'distance722', 'anzpers': 'npers722'}) 
# %% Export the dataset 
dist722.to_csv('/temp/SeqResults/dist722.csv', index=True, sep=';')
# %% Add ntrips and distance variables to the sequence dataset
sequences_zweckmop_722 = pd.read_csv('/temp/SeqResults/sequences_zweckmop_c.csv', sep=';', low_memory=False, index_col=[0])
sequences_zweckmop_722 = pd.merge(sequences_zweckmop_722, dist722, how='left', left_on='HP_ID', right_on='HP_ID')
sequences_zweckmop_722['distance722'] = sequences_zweckmop_722['distance722'].fillna(0)
sequences_zweckmop_722['ntrips722_mid'] = sequences_zweckmop_722['ntrips722_mid'].fillna(0)
sequences_zweckmop_722['npers722'] = sequences_zweckmop_722['npers722'].fillna(0)
#%% Export the dataset
sequences_zweckmop_722.to_csv('/temp/SeqResults/sequences_zweckmop_c722.csv', index=True, sep=';')

# %%
