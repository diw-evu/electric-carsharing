#%% Load necessary packages
import os
import pandas as pd
import numpy as np
from itertools import *

cwd = os.getcwd()
print("Current working directory: {0}".format(cwd))

# %% Load the pre-processed data (sequences with purpose trip)
sequence = pd.read_csv('/temp/SeqResults/zweckmop_seq.csv', sep=';', low_memory=False, index_col=[0]) 

# %% Import list of indexes with overlapping trips 
from check_data import trip_overlap

# %% Concatenate trips made by one individual in the same row
sequences_travel = pd.DataFrame(sequence)
h = sequences_travel.columns.get_loc('HP_ID')
s = sequences_travel.columns.get_loc('00:00')
e = sequences_travel.columns.get_loc('23:55')
sequences_travel = sequences_travel.iloc[:,np.r_[h,s:e+1]]
sequences_travel = sequences_travel.groupby(['HP_ID']).sum()
sequences_travel = pd.DataFrame.drop(sequences_travel, index=trip_overlap)

# Select variables other than travel sequences
hh = sequence.columns.get_loc('HP_ID')
gg = sequence.columns.get_loc('hhgr_gr')
sequences_var = sequence.drop_duplicates(subset='HP_ID', keep='first').iloc[:,hh:gg+1] # only keep the first row as the value does not depend on the trip

#%% Merge the two datasets 
sequence_zweckmop = sequences_var.merge(sequences_travel, how='right', on='HP_ID')
#%% Export to csv file
sequence_zweckmop.to_csv('/temp/SeqResults/sequences_zweckmop.csv', index=True, sep=';')
