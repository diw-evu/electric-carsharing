#%% Load necessary packages
import os
import pandas as pd
import numpy as np
cwd = os.getcwd()
print("Current working directory: {0}".format(cwd))
#%% Load the MiD2017 data
df = pd.read_csv('/temp/MiD2017/CSV/MiD2017_Wege.csv', sep=';', low_memory=False)
#pd.read_csv('/home/agueret/data_encrypted/MiD2017/CSV/MiD2017_Wege.csv', sep=';', low_memory=False)

#%%Select trips made with a car (driver only)
#pkw = [8,9,10] trips made with a car (driver or passenger or carsharing)
pkw=[9] #%% driver
df = df[df['hvm_diff2'].isin(pkw)] 
#filtering out trips where no time data are available for trips
nodata = [99,701] # no time data , 701 mainly concerns professional trips
timevar = ['W_SZS','W_SZM','W_AZS','W_AZM']
for var in timevar: 
    for nan in nodata:
        df = df[df[var]!=nan] 
#filtering out trips made as a driver who declares not having a driving license 
no_driving = df.index[(df['hvm_diff2']==9) & ((df['P_FS_PKW']==2) | (df['P_FS_PKW']==403))].tolist()
df = pd.DataFrame.drop(df, index=no_driving)
#filtering out professional trips 
df=df[df['W_RBW']==0]
#filtering out trips which span two days (arrival after midnight)
df = df[df['W_FOLGETAG']==0]
#filtering out trips made as a passenger with at least one adult of the household
#HH_companion = [3,5,6,7]
#HH_comp_t = df.index[(df['hvm_diff2']==8) & df['begleit'].isin(HH_companion)].tolist()
#df = pd.DataFrame.drop(df, index=HH_comp_t)
#filtering out trips made as a passenger with no information on the driver 
#na_companion = [95, 202, 701, 703, 716, 717]
#na_comp_t = df.index[(df['hvm_diff2']==8) & df['begleit'].isin(na_companion)].tolist()
#df = pd.DataFrame.drop(df, index=na_comp_t)
#filtering out trips made with no information on the number of persons in the car 
na_persons = [99, 701, 703]
na_persons_t = df.index[df['anzpers'].isin(na_persons)].tolist()
df = pd.DataFrame.drop(df, index=na_persons_t)
df.to_csv('/temp/SeqResults/midtrips_clean.csv', index=True, sep=';')

#%% Select relevant variables
df = df[['HP_ID','H_ID','P_ID','W_ID','W_SZ','W_SZS','W_SZM','W_AZ','W_AZS','W_AZM','W_ZWECK', 'zweck_mop', 'ST_WOTAG', 'oek_status', 'haustyp', 'anzauto_gr3', 'hhgr_gr']]

#%% Convert the time of departure and arrival to a more convenient format
# Departure time
# replacing minutes 58 or 59 by 00 and augmenting the hour by 1
late=[58,59]
late_dtrips=df.index[(df['W_SZM'].isin(late))].tolist() #615 rows
for trip in late_dtrips:
    df.loc[trip, 'W_SZS'] = df.loc[trip,'W_SZS'] + 1
    df.loc[trip, 'W_SZM'] = 0
# Check whether some trips leave at 24:00
d_hours24=df.index[(df['W_SZS']==24)].tolist() #48 trips
# For trips starting at 24:00 (i.e. 23h58 or 23h59) we assume they start at 0:0 
for trip in d_hours24:
    df.loc[trip, 'W_SZS'] = 0
# Check
check_d=df.index[(df['W_SZS']==24)].tolist()
if check_d:
    print('List is not empty')
# converting to strings with leading zeros
df['W_SZS_str'] = pd.Series(df['W_SZS'].apply(str).str.zfill(2)) # formating hours with a leading zero
df['W_SZM_str'] = pd.Series(df['W_SZM'].apply(str).str.zfill(2)) # formating minutes with a leading zero
# replacing minutes ending by 1,2,3,4,6 or 7 by 0 or 5
df['W_SZM_str'] = pd.Series(df['W_SZM_str']).str.replace('[1,2]$','0',regex=True) #replace minutes ending by 1 or 2 by 0
df['W_SZM_str'] = pd.Series(df['W_SZM_str']).str.replace('[3,4,6,7]$','5',regex=True) # replace minutes ending by 3,4,6 or 7 by 5   
# replacing minutes ending by 8 or 9 (except 58 and 59)
min_dict = {  
    **dict.fromkeys(['08','09'],'10'),
    **dict.fromkeys(['18','19'],'20'),
    **dict.fromkeys(['28','29'],'30'),
    **dict.fromkeys(['38','39'],'40'),
    **dict.fromkeys(['48','49'],'50')
}
for key in min_dict.keys():
    df['W_SZM_str']=pd.Series(df['W_SZM_str']).str.replace(key,min_dict[key],regex=False)
# concatenating hours and minutes
df['W_SZ_str'] = df['W_SZS_str'] +':'+ df['W_SZM_str'] 

# Arrival time
# replacing minutes 58 or 59 by 00 and augmenting the hour by 1
late_atrips=df.index[(df['W_AZM'].isin(late))].tolist() #1501 trips
for trip in late_atrips:
    df.loc[trip, 'W_AZS'] = df.loc[trip,'W_AZS'] + 1
    df.loc[trip, 'W_AZM'] = 0
# Check whether some trips end at 24:00
a_hours24=df.index[(df['W_AZS']==24)].tolist() #157 trips
# For trips ending at 24:00 (i.e. 23h58 or 23h59) we assume they arrive at 23h55
for trip in a_hours24:
    df.loc[trip, 'W_AZS'] = 23
    df.loc[trip, 'W_AZM'] = 55
# Check
check_a=df.index[(df['W_AZS']==24)].tolist()
if check_a:
    print('List is not empty')
# converting to strings with leading zeros
df['W_AZS_str'] = pd.Series(df['W_AZS'].apply(str).str.zfill(2)) # formating hours with a leading zero
df['W_AZM_str'] = pd.Series(df['W_AZM'].apply(str).str.zfill(2)) # formating minutes with a leading zero
# replacing minutes ending by 1,2,3,4,6 or 7 by 0 or 5
df['W_AZM_str'] = pd.Series(df['W_AZM_str']).str.replace('[1,2]$','0',regex=True) #replace minutes ending by 1 or 2 by 0
df['W_AZM_str'] = pd.Series(df['W_AZM_str']).str.replace('[3,4,6,7]$','5',regex=True) # replace minutes ending by 3,4,6 or 7 by 5   
# replacing minutes ending by 8 or 9 (except 58 and 59)
for key in min_dict.keys():
    df['W_AZM_str']=pd.Series(df['W_AZM_str']).str.replace(key,min_dict[key],regex=False)
# concatenating hours and minutes
df['W_AZ_str'] = df['W_AZS_str'] +':'+ df['W_AZM_str']
# Select relevant variables
df = df[['HP_ID','H_ID','P_ID','W_ID','W_SZ_str','W_AZ_str','W_ZWECK', 'zweck_mop', 'ST_WOTAG', 'oek_status', 'haustyp', 'anzauto_gr3', 'hhgr_gr']]

#%% Clean the environment from useless objects
del(a_hours24,check_a,check_d,d_hours24,key,late,late_atrips,late_dtrips,min_dict,pkw,trip,nan,nodata,timevar,var)

#%% Create new columns with time of the day by 5 minutes-step
NaN=np.nan
n=12
m=23

hours=list(range(24)) #list of hours from 0 to 23
hours=[item for item in hours for i in range(n)] #duplicate each hour 12 times in a row
hours_str=[str(i).zfill(2) for i in hours] #convert to string with leading zeros

minutes=[] #initiliaze empty list
for x in range(0,60,5): #create a list of minutes from 0 to 55 by steps of 5
    minutes.append(x)
temp_minutes=list(minutes)
for i in range(m): #expand the minutes list 23 times in a row
    for min in temp_minutes:
        minutes.append(min)
minutes_str=[str(i).zfill(2) for i in minutes] #convert to string with leading zeros

h_m=list(map(":".join,zip(hours_str,minutes_str))) #concatenating the minutes and hours lists

for j in h_m: #create new variables with empty rows
    df[j]=NaN

#%% Clean the environment from useless objects   
del(i,m,n,x,j,min,temp_minutes,NaN,hours,hours_str,minutes,minutes_str) 

# %% Check that all observations have a valid trip purpose
zweckm = [1, 2, 3, 4, 5]
df = df[df['zweck_mop'].isin(zweckm)] 

# %% Fill hours:minutes columns with the value encapsulating the
#### purpose of the trip (zweck_mop) for each 5-minutes step of the trip
zweckmop=pd.DataFrame(df)
x=zweckmop.columns.get_loc('00:00')   # get the position of the column 00:00
p=zweckmop.columns.get_loc('zweck_mop')  # get the position of the column displaying the purpose of the trip
trips_indexes=zweckmop.index.tolist() # get the indexes of the rows (indexes are different from the number of the row)
length=len(trips_indexes)       # get the overall number of trips
checklist=[]                    # create empty list to check where the loop stops if it fails
for l in range(0,length,1):     # loop over rows (i.e. trips called by their position in the dataframe)
    t=trips_indexes[l]          # get the corresponding trip index
    departure=[]                # create empty departure list
    arrival=[]                  # create empty arrival list
    for i in range(0,288,1):    # loop over integers from 0 to 288
        departure.append(zweckmop.loc[t,'W_SZ_str']==h_m[i]) # expand the departure list with 0 if departure time does not equal the i-th element of hours:minutes list and with 1 if it is equal
        arrival.append(zweckmop.loc[t,'W_AZ_str']==h_m[i])   # same for arrival list
    index_d = departure.index(True) # get the index of the element (should be unique) of the departure list being equal to 1
    index_a = arrival.index(True)   # get the index of the element (should be unique) of the arrival list being equal to 1 
    y=x+index_d                # add to the departure time index of the positive element the index where hours:minutes columns start
    z=x+index_a                # same for arrival time index 
    if y != z:
        zweckmop.iloc[l,y:z]=zweckmop.iloc[l,p] # for every column located between the departure time and the arrival time, set value equal to the value of the purpose of the trip
    else:
        zweckmop.iloc[l,y]=zweckmop.iloc[l,p]
    checklist.append(t)        # expand the checklist with the trip index before starting the loop with a new trip 

# %% Export to csv file
drop=['H_ID','P_ID','W_ID','W_ZWECK', 'zweck_mop', 'W_SZ_str','W_AZ_str']
zweckmop=pd.DataFrame.drop(zweckmop,columns=drop) # duplicate the dataframe with only the necessary variables
zweckmop.to_csv('/temp/SeqResults/zweckmop_seq.csv', index=True, sep=';')
# %%
