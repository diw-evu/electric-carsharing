#%% Load necessary packages
import os
import pandas as pd
from pathlib import Path 

cwd = os.getcwd()
print("Current working directory: {0}".format(cwd))

# Define paths
results = Path('/temp/SeqResults')    # change this path when working on another operating system - the structure of the results folder should nevertheless be the same
sequences = results / 'sequences'

#%% Load the MiD2017 data
mid = pd.read_csv('/temp/MiD2017/CSV/MiD2017_Wege.csv', sep=';', low_memory=False) 
# %% Keep only identifiers and location variables 
keep=['HP_ID', 'H_ID', 'P_ID', 'W_ID', 'RegioStaRGem5', 'ST_WOTAG']
df = pd.DataFrame(mid, columns=keep)
mega = {}   # initialize an empty dictionary to collect lists of identifiers for various variables
# %% Compute lists of identifiers for each type of location 
locations_id = {}
locations = {'metro': [51], 
             'big': [52], 
             'middle': [53],
             'small': [54],
             'rural': [55]}

for s in list(locations.keys()):
    mega[s] = df.loc[df['RegioStaRGem5'].isin(locations[s])]  # associates to each key of the "locations" dict the dataset shrinked down to observations for which the RegioStaR7 takes given value
    locations_id[s] = list(mega[s]['HP_ID']) # creates list of identifiers of observations in the previously generated dataset
    locations_id[s] = pd.unique(locations_id[s]).tolist() # remove duplicate identifiers 

# %% Compute lists of identifiers for type of day (working day vs. weekend)
days_id = {}
days = {'wd' : [1,2,3,4,5], 
        'sa' : [6],
        'su' : [7]}

for s in list(days.keys()):
    mega[s] = df.loc[df['ST_WOTAG'].isin(days[s])]
    days_id[s] = list(mega[s]['HP_ID'])
    days_id[s] = pd.unique(days_id[s]).tolist()

# %% Create sub-datasets by iterating on each element of locations and days 
# in order to get a subdataset for each possible combination of these two elements
# e.g. (rural, working days) or (metro, saturday) and so forth
database = {} 
seq = pd.read_csv('/temp/SeqResults/sequences_zweckmop_c722.csv', sep=';', index_col=[0]) # Import the sequence dataset 
for l in locations_id.keys():
    for d in days_id.keys():
        database[(l,d)] = seq.loc[seq['HP_ID'].isin(locations_id[l]) & seq['HP_ID'].isin(days_id[d])]
        path_out = sequences / l
        name_out = '{}_{}.csv'.format(l,d)
        pd.DataFrame.from_dict(database[(l,d)]).to_csv(os.path.join(path_out, name_out), index=True, sep=';')
# %%
