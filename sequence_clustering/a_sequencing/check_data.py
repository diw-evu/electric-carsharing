#%% Load necessary packages
import os
import pandas as pd
import numpy as np
from itertools import *

cwd = os.getcwd()
print("Current working directory: {0}".format(cwd))

#%% Load the pre-processed data (sequences)
check_zweckmop = pd.read_csv('/temp/SeqResults/zweckmop_seq.csv', sep=';', low_memory=False, index_col=[0]) 

# %% Define the database to be checked
h=check_zweckmop.columns.get_loc('HP_ID')   # get the position of the column 00:00
s=check_zweckmop.columns.get_loc('00:00')   # get the position of the column 00:00
e=check_zweckmop.columns.get_loc('23:55') # get the position of the column 23:55
check=check_zweckmop.iloc[:,np.r_[h,s:e+1]]

#%% 
ss=check.columns.get_loc('00:00')   # get the position of the column 00:00
ee=check.columns.get_loc('23:55') # get the position of the column 23:55
# %% Check whether there are trips whose purpose is unspecified 
if 99 in check.iloc[:,ss:ee+1].values :
        print("exists in DataFrame")  
else :
        print("does not exist")

# %% Check whether there are trips overlapping for a given individual
check=check.replace([2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,99],1)  # for any value not equal to NaN or 1, replace by a 1
overlap=[2,3,4,5,6,7,8,9,10,99] # check whether there are values different from 0 or 1
for o in overlap:
    print(o)
    if o in check.iloc[:,ss:ee+1].values :
        print("exists in DataFrame")  
    else :
        print("does not exist")

# %%  Concatenate trips made by one individual in the same row
check=check.groupby(['HP_ID']).sum() 
overlap=[2,3,4,5,6,7,8,9,10]
for o in overlap:
    print(o)
    if o in check.iloc[:,ss:ee+1].values :
        print("exists in DataFrame")  # check whether there are values different from 0 or 1
    else :
        print("does not exist")

# %% Extract individual indexes for which trip overlap
trip_overlap2=[]
trip_overlap3=[]
trip_overlap4=[]
trip_overlap5=[]
trips_indexes=check.index.tolist()
length=len(trips_indexes)
for i in range(0,length,1):
    t=trips_indexes[i]
    if 2 in check.iloc[i,ss:ee+1].values :
        trip_overlap2.append(t)
    if 3 in check.iloc[i,ss:ee+1].values :
        trip_overlap3.append(t)
    if 4 in check.iloc[i,ss:ee+1].values :
        trip_overlap4.append(t)
    if 5 in check.iloc[i,ss:ee+1].values :
        trip_overlap5.append(t)

# %% Collect indexes in a single list
trip_overlap=list(chain(trip_overlap2,trip_overlap3,trip_overlap4,trip_overlap5))
trip_overlap=pd.unique(trip_overlap).tolist()

