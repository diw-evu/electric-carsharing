# %% Import required packages
import os
import pandas as pd
from pathlib import Path 

cwd = os.getcwd()
print("Current working directory: {0}".format(cwd))

# %% Define paths
results = Path('/temp/SeqResults')    # change this path when working on another operating system - the structure of the results folder should nevertheless be the same
clusters = results / 'clusters'
clust_metro = clusters / 'metro'
clust_big = clusters / 'big'
clust_middle = clusters / 'middle'
clust_small = clusters / 'small'
clust_rural = clusters / 'rural'

# %% Reshuffle cluster names to increase plots readability
# Metropolises
mtwd = pd.read_csv(os.path.join(clust_metro, 'cluster-mtwd-im24.csv'),sep=';', index_col=[0])
mtsa = pd.read_csv(os.path.join(clust_metro, 'cluster-mtsa-im24.csv'),sep=';', index_col=[0])
mtsu = pd.read_csv(os.path.join(clust_metro, 'cluster-mtsu-im24.csv'),sep=';', index_col=[0])
# %% Working days
mtwd['Cluster'] = mtwd['Cluster'].replace(to_replace=[0,1,2,3,4,5], value=[0,1,3,4,2,5])
mtwd.to_csv(os.path.join(clust_metro, 'cluster-mtwd-im24.csv'), index=True, sep=';')
# %% Saturdays
mtsa['Cluster'] = mtsa['Cluster'].replace(to_replace=[0,1,2,3], value=[0,3,1,2])
mtsa.to_csv(os.path.join(clust_metro, 'cluster-mtsa-im24.csv'), index=True, sep=';')
# %% Sundays
mtsu['Cluster'] = mtsu['Cluster'].replace(to_replace=[0,1,2,3], value=[0,3,1,2])
mtsu.to_csv(os.path.join(clust_metro, 'cluster-mtsu-im24.csv'), index=True, sep=';')

# %% Big cities
bwd = pd.read_csv(os.path.join(clust_big, 'cluster-bwd-im24.csv'),sep=';', index_col=[0])
bsa = pd.read_csv(os.path.join(clust_big, 'cluster-bsa-im24.csv'),sep=';', index_col=[0])
bsu = pd.read_csv(os.path.join(clust_big, 'cluster-bsu-im24.csv'),sep=';', index_col=[0])
#%% Working days
bwd['Cluster'] = bwd['Cluster'].replace(to_replace=[0,1,2,3,4,5], value=[3,2,0,5,1,4])
bwd.to_csv(os.path.join(clust_big, 'cluster-bwd-im24.csv'), index=True, sep=';')
#%% Saturdays
bsa['Cluster'] = bsa['Cluster'].replace(to_replace=[0,1,2,3,4], value=[0,1,4,2,3])
bsa.to_csv(os.path.join(clust_big, 'cluster-bsa-im24.csv'), index=True, sep=';')
#%% Sundays
bsu['Cluster'] = bsu['Cluster'].replace(to_replace=[0,1,2,3,4], value=[1,3,0,2,4])
bsu.to_csv(os.path.join(clust_big, 'cluster-bsu-im24.csv'), index=True, sep=';')

# %% Middle-size cities
mdwd = pd.read_csv(os.path.join(clust_middle, 'cluster-mdwd-im24.csv'),sep=';', index_col=[0])
mdsa = pd.read_csv(os.path.join(clust_middle, 'cluster-mdsa-im24.csv'),sep=';', index_col=[0])
mdsu = pd.read_csv(os.path.join(clust_middle, 'cluster-mdsu-im24.csv'),sep=';', index_col=[0])
#%% Working days
mdwd['Cluster'] = mdwd['Cluster'].replace(to_replace=[0,1,2,3,4,5], value=[0,4,2,5,3,1])
mdwd.to_csv(os.path.join(clust_middle, 'cluster-mdwd-im24.csv'), index=True, sep=';')
#%% Saturdays
mdsa['Cluster'] = mdsa['Cluster'].replace(to_replace=[0,1,2,3], value=[0,3,1,2])
mdsa.to_csv(os.path.join(clust_middle, 'cluster-mdsa-im24.csv'), index=True, sep=';')
#%%Sundays
mdsu['Cluster'] = mdsu['Cluster'].replace(to_replace=[0,1,2,3], value=[2,0,3,1])
mdsu.to_csv(os.path.join(clust_middle, 'cluster-mdsu-im24.csv'), index=True, sep=';')

# %% Small cities
swd = pd.read_csv(os.path.join(clust_small, 'cluster-swd-im24.csv'),sep=';', index_col=[0])
ssa = pd.read_csv(os.path.join(clust_small, 'cluster-ssa-im24.csv'),sep=';', index_col=[0])
ssu = pd.read_csv(os.path.join(clust_small, 'cluster-ssu-im24.csv'),sep=';', index_col=[0])
#%% Working days
swd['Cluster'] = swd['Cluster'].replace(to_replace=[0,1,2,3,4,5], value=[2,3,1,5,0,4])
swd.to_csv(os.path.join(clust_small, 'cluster-swd-im24.csv'), index=True, sep=';')
#%% Saturdays
ssa['Cluster'] = ssa['Cluster'].replace(to_replace=[0,1,2,3,4], value=[0,2,3,1,4])
ssa.to_csv(os.path.join(clust_small, 'cluster-ssa-im24.csv'), index=True, sep=';')
#%% Sundays
ssu['Cluster'] = ssu['Cluster'].replace(to_replace=[0,1,2,3,4], value=[0,3,1,4,2])
ssu.to_csv(os.path.join(clust_small, 'cluster-ssu-im24.csv'), index=True, sep=';')

#%% Rural areas
rwd = pd.read_csv(os.path.join(clust_rural, 'cluster-rwd-im24.csv'),sep=';', index_col=[0])
rsa = pd.read_csv(os.path.join(clust_rural, 'cluster-rsa-im24.csv'),sep=';', index_col=[0])
rsu = pd.read_csv(os.path.join(clust_rural, 'cluster-rsu-im24.csv'),sep=';', index_col=[0])
#%% Working days
rwd['Cluster'] = rwd['Cluster'].replace(to_replace=[0,1,2,3,4,5], value=[1,5,0,4,2,3])
rwd.to_csv(os.path.join(clust_rural, 'cluster-rwd-im24.csv'), index=True, sep=';')
#%% Saturdays
rsa['Cluster'] = rsa['Cluster'].replace(to_replace=[0,1,2,3,4], value=[3,1,4,0,2])
rsa.to_csv(os.path.join(clust_rural, 'cluster-rsa-im24.csv'), index=True, sep=';')
#%% Sundays
rsu['Cluster'] = rsu['Cluster'].replace(to_replace=[0,1,2,3], value=[0,3,1,2])
rsu.to_csv(os.path.join(clust_rural, 'cluster-rsu-im24.csv'), index=True, sep=';')
# %%
