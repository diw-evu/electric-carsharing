# %% Import required packages
import os
import pandas as pd
import numpy as np
import copy
from Levenshtein import distance as ldistance
from sklearn.cluster import AgglomerativeClustering
from scipy.spatial.distance import squareform
from scipy.cluster.hierarchy import ward, fcluster
from matplotlib import pyplot as plt
from scipy.cluster.hierarchy import dendrogram as dend
from pathlib import Path 
from collections import defaultdict
from functions.dendogram import plot_dendrogram

cwd = os.getcwd()
print("Current working directory: {0}".format(cwd))

# %% Define paths
results = Path('/temp/SeqResults')    # change this path when working on another operating system - the structure of the results folder should nevertheless be the same
sequences = results / 'sequences'
seq_metro = sequences / 'metro'
seq_big = sequences / 'big'
seq_middle = sequences / 'middle' 
seq_small = sequences / 'small' 
seq_rural = sequences / 'rural' 
dendograms = results / 'dendograms'
dend_metro = dendograms / 'metro'
dend_big = dendograms / 'big'
dend_middle = dendograms / 'middle'
dend_small = dendograms / 'small'
dend_rural = dendograms / 'rural'
clusters = results / 'clusters'
clust_metro = clusters / 'metro'
clust_big = clusters / 'big'
clust_middle = clusters / 'middle'
clust_small = clusters / 'small'
clust_rural = clusters / 'rural'

# %% List files for sequences of various subdatasets
files = {}

seq = [seq_metro, seq_big, seq_middle, seq_small, seq_rural]

for l in seq:
    for f in os.listdir(l):
        if f.endswith(".csv"):
            files.setdefault(l, []).append(f)

metro_dict = {'mtsa': [],'mtsu': [], 'mtwd': []}
big_dict = {'bsa': [],'bsu': [], 'bwd': []}
middle_dict = {'mdsa': [],'mdsu': [], 'mdwd': []}
small_dict = {'ssa': [],'ssu': [], 'swd': []}
rural_dict = {'rsa': [],'rsu': [], 'rwd': []}


loc_dict = [metro_dict, big_dict, middle_dict, small_dict, rural_dict]

for s,l in zip(seq, loc_dict):
    for k,j in enumerate(list(l.keys())):
        l[j] = files[s][k]

metro_paths =[]
big_paths = []
middle_paths = []
small_paths = []
rural_paths = []
paths = [metro_paths, big_paths, middle_paths, small_paths, rural_paths]

for p,q,r in zip(paths, loc_dict, seq):
    for v in list(q.values()):
        p.append(r / v)

dict_path = {'metro': (metro_dict, metro_paths, dend_metro, clust_metro), 
            'big': (big_dict, big_paths, dend_big, clust_big),
            'middle': (middle_dict, middle_paths, dend_middle, clust_middle),
            'small': (small_dict, small_paths, dend_small, clust_small),
            'rural': (rural_dict, rural_paths, dend_rural, clust_rural)}

del(f,j,k,l,p,q,r,s,v)

# %% ===> SELECT THE LOCATION OF YOUR CHOICE <===
# options: 'metro', 'big', 'middle', 'small' or 'rural'
set_locations = ['rural']   #['metro', 'big', 'middle', 'small', 'rural']

for location in set_locations:
    # Load the raw working data 
    x=dict_path[location][0] 
    y=dict_path[location][1] 
    z=dict_path[location][2]
    w=dict_path[location][3]

    original_data={}

    for i,filename in zip(list(x.keys()), y): 
        original_data[i] = pd.read_csv(filename, sep=';', low_memory=False, index_col=[0])

    # Load the raw working data 
    data={}

    for i,filename in zip(list(x.keys()), y): 
        data[i] = pd.read_csv(filename, sep=';', low_memory=False, index_col=[0])

    # ===> Here you need to define manually the parameters <===
    # ===> purpose, salphabet, lalphabet, taxo, start_time <===
    # ===> and end_time                                    <===
    purpose = [0,1]                     # alternative could be e.g.  [0, 1, 2, 3, 4, 5]
    salphabet = ['I', 'M']              # alternative could be e.g.  ['I', 'W', 'S', 'E', 'L', 'H']
    lalphabet = ['Idle', 'Moving']      # alternative could be e.g.  ['Idle', 'Work', 'School', 'Errands', 'Leisure', 'Home']
    taxo = 'im'                         # when modifying the taxonomy, attribute a distinct name e.g. 'purp'
    start_time = '00:00'                # In order to get the full sequence, set start time = '00:00'
    end_time = '23:55'                  # and end_time = '23:55'
    time = '24'                       # when modifying the time frame, attribute a distinct name e.g. '00/23'

    # Check whether there are some sequences with no trips between start and end time
    # If yes, remove them
    index={}
    data_ni=defaultdict(dict)
    for i in list(x.keys()):
        data[i] = pd.DataFrame.replace(data[i], to_replace = [2,3,4,5], value = 1)      # Taxonomy with only one destination type # mute this line if you want the distance to be computed on differences in trip purposes as well
        start = data[i].columns.get_loc(start_time)                                        
        end = data[i].columns.get_loc(end_time)
        data[i]['idle'] = data[i].iloc[:, start:end].sum(axis=1)
        ind_idle = []
        for k in data[i]['idle'].keys():
            if data[i]['idle'][k]==0:
                ind_idle.append(k)
        index.setdefault(i, ind_idle)
        data_ni[i] = copy.deepcopy(data[i])
        data_ni[i] = pd.DataFrame.drop(data_ni[i], index=index[i]).reset_index(drop=True)

    # Convert numbers to letters along the right taxonomy
    # and pick the time frame of the sequences to analyse 
    for i in list(x.keys()):
        data_ni[i] = pd.DataFrame.replace(data_ni[i], to_replace = purpose, value = salphabet) 
        start = data_ni[i].columns.get_loc(start_time)                                        
        end = data_ni[i].columns.get_loc(end_time)
        core=[0]
        for c in range(start,end+1,1):
            core.append(c)
        data_ni[i] = data_ni[i].iloc[:,core]      # Truncate the time sequences for a specific time frame 

    # Convert each row into a sequence of characters
    for i in list(x.keys()):
        timeslots = data_ni[i].columns.values.tolist()
        timeslots = timeslots[1:len(data_ni[i].columns)]     
        data_ni[i]['Sequence'] = data_ni[i].iloc[:,1].str.cat(data_ni[i][t] for t in timeslots)
        data_ni[i] = pd.DataFrame(data_ni[i], columns = ('HP_ID', 'Sequence'))

    # Collect character sequences in a dictionnary (key: row, value: HP_ID, sequence)
    seq_dict =  defaultdict(dict)
    size=[]
    for h,i in enumerate(list(x.keys())):
        seq_dict[i] = data_ni[i].to_dict('index') 
        size.append(len(seq_dict[i])) 
        seq_dict[i]['dist'] = np.empty((size[h], size[h])) # Create empty square matrix 
        for r in range(0, size[h], 1): # Compute the Levenshtein distance matrix for each pair of sequences 
            for c in range(0, r, 1):
                seq_dict[i]['dist'][r,c] = ldistance(seq_dict[i][r]['Sequence'], seq_dict[i][c]['Sequence'])
                seq_dict[i]['dist'][c,r] = seq_dict[i]['dist'][r,c] 

    #  ===> SELECT THE NUMBER OF CLUSTERS OF YOUR CHOICE <===
    # Perform the clustering specifying the number of clusters
    #'metro': [5,4,6], #im722: [5,4,6], # define the number of clusters for each subdataset in the order [sa,su,wd]
    #'big' : [4,3,6]}   #im722:[4,3,6],
    #'middle': [5,4,6], #im722 [4,5,6]
    #'small': [5,4,6],  #im722 [5,5,6]
    nclusters = {'rural': [5,5,6]}  #im722 [5,5,6]
    for h,i in enumerate(list(x.keys())):
        clustering = AgglomerativeClustering(n_clusters=nclusters[location][h], affinity='euclidean', linkage='ward')
        groups = clustering.fit(seq_dict[i]['dist']) # clustering on dissimilarity matrix
        labels = groups.labels_                          # assigning cluster number to each sequence
        for k in range(0, size[h], 1):
            seq_dict[i][k]['Cluster'] = labels[k]  
        
    # Merge datasets (clusters, sequences, ID)   
    output_cluster = defaultdict(dict)
    full_data = defaultdict(dict)
    for i in list(x.keys()):
        output_cluster = copy.deepcopy(seq_dict[i])
        del output_cluster['dist']
        for k in list(output_cluster.keys()):
            del output_cluster[k]['Sequence']
        full_data[i] = pd.DataFrame.from_dict(output_cluster,orient='index')
        full_data[i] = pd.merge(original_data[i],full_data[i],how='right',left_on='HP_ID', right_on='HP_ID')
        full_data[i] = pd.DataFrame.drop(full_data[i], columns = 'total')
        
    # Export clusters database
    for h,i in enumerate(list(x.keys())):
        short = list(x.keys())[h]
        cluster_output = "cluster-{}-{}{}.csv".format(short, taxo, time)
        full_data[i].to_csv(os.path.join(w, cluster_output), index=True, sep=';')
# %%
