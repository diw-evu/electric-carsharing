###############################################
###### Distribution of trip destination  ######
###############################################

#%% Load packages and define paths 
import os
import pandas as pd
import numpy as np
from pathlib import Path 
from collections import defaultdict
from seqdeparture import dataset_seqdeparture

cwd = os.getcwd()
print("Current working directory: {0}".format(cwd))

# Define paths
results = Path('/Adeline/ecarsharing/SeqResults') #change this path when working on another operating system - the structure of the results folder should nevertheless be the same
clusters = results / 'clusters'
clust_metro = clusters / 'metro'
clust_big = clusters / 'big'
clust_middle = clusters / 'middle'
clust_small = clusters / 'small'
clust_rural = clusters / 'rural'
dist = results / 'distributions' 
distdest_cs_slow = dist / 'carsharing' / 'slow' / 'tripdest'
distdest_cs_fast = dist / 'carsharing' / 'fast' / 'tripdest'
distdest_cs_week_slow = distdest_cs_slow / 'week'
distdest_cs_week_fast = distdest_cs_fast / 'week'

#%% Define a function: generate probability dictionnary containing probabilities at the cluster level for a given location/day
def prob_tables_clust_cs(data: pd.DataFrame, cluster_carsharing: list, start_time: str ="00:00", end_time: str = "23:55") -> dict:
    """
    Function that compute probabilities of trip destination for all timesteps at 5-min resolution for each cluster. 
    The output is a dictionnary whose keys are every 5-minutes time steps from start_time until end_time. 
    The value for each key is a dataframe whose rows are clusters and whose columns are probabilities. 

    Parameters
    ----------

    data : dataframe 
        Name of the dataframe to be used.
        Should contain a column for each time step whose value encodes the trip destination. 
        Should contain a column named 'Cluster' allocating each row to a given cluster.
    start_time : string
        Name of the 5-minute time step to be considered as the first one. Format is 'hh:ss'. Typically '00:00'.
    end_time : string
        Name of the 5-minute time step to be considered as the last one. Format is 'hh:ss'. Typically '23:55'.
    """
    g = data.columns.get_loc(start_time)
    h = data.columns.get_loc(end_time)
    namedest = [0,1,2,3,4] #needs to be changed if number of destinations is not 4
    probdest_clust=defaultdict(dict)
    for i in range(g,h+1):
        time = data.columns[i]
        data.iloc[:,g] = data.iloc[:,i].astype(int)
        dest = pd.DataFrame(index=namedest)
        dest[0]=data[data['Cluster'].isin(cluster_carsharing)].iloc[:,i].value_counts().to_frame()
        dest=dest.T
        dest=pd.DataFrame.rename(dest, columns={0: 'idle', 1: 'work', 2:'errands', 3:'leisure', 4:'home'}) #needs to be changed if number of destinations is not 4
        dist_dest = pd.DataFrame(index=[0])
        dist_dest['nb_rows_clust'] = data[data['Cluster'].isin(cluster_carsharing)]['HP_ID'].nunique()
        dist_dest['nb_trips_clust'] = data[data['Cluster'].isin(cluster_carsharing)]['ntrips'].sum()
        dist_dest['nb_idle_clust'] = data[data['Cluster'].isin(cluster_carsharing) & (data[time]==0)][time].value_counts()
        dist_dest['nb_moving_clust'] = dist_dest['nb_rows_clust'] - dist_dest['nb_idle_clust']
        dist_dest['pb_moving_clust'] = 1 - (dist_dest['nb_idle_clust'] / dist_dest['nb_rows_clust'])
        dist_dest = dist_dest.join(dest, how='inner')
        dist_dest['pb_work'] = (dist_dest['work'] / dist_dest['nb_trips_clust']) 
        dist_dest['pb_errands'] = (dist_dest['errands'] / dist_dest['nb_trips_clust'])
        dist_dest['pb_leisure'] = (dist_dest['leisure'] / dist_dest['nb_trips_clust'])
        dist_dest['pb_home'] = (dist_dest['home'] / dist_dest['nb_trips_clust'])
        probdest_clust[time] = dist_dest
    return probdest_clust

#%% Define a function: generate probability dictionnary containing probabilities at the dataset level for a given location/day
def prob_tables_dataset_cs(data: pd.DataFrame, start_time: str ='00:00', end_time: str = '23:55') -> dict:
    """
    Function that compute probabilities of trip destination for all timesteps at 5-min resolution for the whole dataset. 
    The output is a dictionnary whose keys are every 5-minutes time steps from start_time until end_time. 
    The value for each key is a dataframe with one row and whose columns are probabilities. 

    Parameters
    ----------

    data : dataframe 
        Name of the dataframe to be used.
        Should contain a column for each time step whose value encodes the trip destination. 
    start_time : string
        Name of the 5-minute time step to be considered as the first one. Format is 'hh:ss'. Typically '00:00'.
    end_time : string
        Name of the 5-minute time step to be considered as the last one. Format is 'hh:ss'. Typically '23:55'.
    """
    g = data.columns.get_loc(start_time)
    h = data.columns.get_loc(end_time)
    namedest = [0,1,2,3,4] #needs to be changed if number of destinations is not 4
    probdest_dataset=defaultdict(dict)
    for i in range(g,h+1):
        time = data.columns[i]
        data.iloc[:,g] = data.iloc[:,i].astype(int)
        dest = pd.DataFrame(index=namedest)
        dest[0]=data.iloc[:,i].value_counts().to_frame()
        dest=dest.T
        dest=pd.DataFrame.rename(dest, columns={0: 'idle', 1: 'work', 2:'errands', 3:'leisure', 4:'home'}) #needs to be changed if number of destinations is not 4
        dist_dest = pd.DataFrame(index=[0])
        dist_dest['nb_rows'] = data['HP_ID'].nunique()
        dist_dest['nb_trips'] = data['ntrips'].sum()
        dist_dest['nb_idle'] = data[data[time]==0][time].value_counts()
        dist_dest['nb_moving'] = dist_dest['nb_rows'] - dist_dest['nb_idle']
        dist_dest['pb_moving'] = 1 - (dist_dest['nb_idle'] / dist_dest['nb_rows'])
        dist_dest = dist_dest.join(dest, how='inner')
        dist_dest['pb_work'] = (dist_dest['work'] / dist_dest['nb_trips'])
        dist_dest['pb_errands'] = (dist_dest['errands'] / dist_dest['nb_trips']) 
        dist_dest['pb_leisure'] = (dist_dest['leisure'] / dist_dest['nb_trips']) 
        dist_dest['pb_home'] = (dist_dest['home'] / dist_dest['nb_trips'])
        probdest_dataset[time] = dist_dest
    return probdest_dataset

#%% Define a function: generate probability dictionnary containing probabilities at the level of a *group of clusters* for a given location/day
def prob_tables_groupclust_cs(data: pd.DataFrame, clustergroup: list, start_time: str ='00:00', end_time: str = '23:55') -> dict:
    """
    Function that compute probabilities of trip destination for all timesteps at 5-min resolution for a dataset filtered for a group of clusters. 
    The output is a dictionnary whose keys are every 5-minutes time steps from start_time until end_time. 
    The value for each key is a dataframe with one row and whose columns are probabilities. 

    Parameters
    ----------

    data : dataframe 
        Name of the dataframe to be used.
        Should contain a column for each time step whose value encodes the trip destination.
    clustergroup: list
        List of the clusters that have to be kept in the dataset. 
    start_time : string
        Name of the 5-minute time step to be considered as the first one. Format is 'hh:ss'. Typically '00:00'.
    end_time : string
        Name of the 5-minute time step to be considered as the last one. Format is 'hh:ss'. Typically '23:55'.
    """
    data = data[data['Cluster'].isin(clustergroup)]
    g = data.columns.get_loc(start_time)
    h = data.columns.get_loc(end_time)
    namedest = [0,1,2,3,4] #needs to be changed if number of destinations is not 4
    probdest_groupclust=defaultdict(dict)
    for i in range(g,h+1):
        time = data.columns[i]
        data.iloc[:,g] = data.iloc[:,i].astype(int)
        dest = pd.DataFrame(index=namedest)
        dest[0]=data.iloc[:,i].value_counts().to_frame()
        dest=dest.T
        dest=pd.DataFrame.rename(dest, columns={0: 'idle', 1: 'work', 2:'errands', 3:'leisure', 4:'home'}) #needs to be changed if number of destinations is not 4
        dist_dest = pd.DataFrame(index=[0])
        dist_dest['nb_rows'] = data['HP_ID'].nunique()
        dist_dest['nb_trips'] = data['ntrips'].sum()
        dist_dest['nb_idle'] = data[data[time]==0][time].value_counts()
        dist_dest['nb_moving'] = dist_dest['nb_rows'] - dist_dest['nb_idle']
        dist_dest['pb_moving'] = 1 - (dist_dest['nb_idle'] / dist_dest['nb_rows'])
        dist_dest = dist_dest.join(dest, how='inner')
        dist_dest['pb_work'] = (dist_dest['work'] / dist_dest['nb_trips'])
        dist_dest['pb_errands'] = (dist_dest['errands'] / dist_dest['nb_trips']) 
        dist_dest['pb_leisure'] = (dist_dest['leisure'] / dist_dest['nb_trips']) 
        dist_dest['pb_home'] = (dist_dest['home'] / dist_dest['nb_trips'])
        probdest_groupclust[time] = dist_dest
    return probdest_groupclust

#%% Define a function: generate probabilities for the whole dataset of a given location/day for all timesteps and destinations in emobpy format
def probtripdest_dataset(probdict: dict, location: str, day_full: str, day_short: str, frame: str, scope:str, folder: Path) -> pd.DataFrame:
    """ Function that computes a dataframe for the full dataset for all time steps. 
    Rows are 5-minutes time steps from 00:00 until 23:55. 
    Columns are day, hour (in a decimal format), and probabilities for going to work, school, errands, leisure, home.
    Probabilities are normalized to sum up to 1. 
    The dataframe is exported to a csv format in the specified folder. 

    Parameters
    ----------

    probdict : dictionnary 
        Output from the prob_tables_dataset function.
        Keys are 5-minute time steps from 00:00 until 23:55.
    location : string 
        Short name for the location (e.g. 'mt' for metropolises) to be used as a suffix in the exported csv file name.
    day_full : string 
        Full name of type of day (e.g. 'working day', 'saturday' or 'sunday') to fill the column 'day' of the dataframe.
    day_short : string 
        Short name for the type of day (e.g. 'wd, 'sa' or 'su') to be used as a suffix in the exported csv file name.
    frame : str
        Suffix to the name of the exported csv file to indicate sequences' destination typology and timeframe (e.g. 'im24')
    scope: int
        Suffix to the name of the exported csv file to indicate whether the probability distribution is at the cluster (clust), group of 
        clusters (group) or dataset (full) level.
    folder : path
        Path to the folder where the csv file is exported
    """
    timesteps=[]
    for t in range(1,289):
        timesteps.append(t)
    timesteps_0 = [x - 1 for x in timesteps]
    timesteps_dec = np.array(timesteps_0)
    timesteps_dec = timesteps_dec * (1/12)
    timesteps_dec = timesteps_dec.tolist()
    probtripdest_dataset=pd.DataFrame(index=timesteps, columns=['days', 'time', 'work', 'errands', 'leisure', 'home'])
    probtripdest_dataset['days'] = day_full
    probtripdest_dataset['time'] = timesteps_dec
    for (r,k) in zip(timesteps, probdict.keys()):
        probtripdest_dataset.iloc[r-1, 2:6] = probdict[k].iloc[0, 10:14]
    s = probtripdest_dataset.iloc[:, 2:6].sum().sum()
    name_csv = "disttripdest-{}{}-{}-{}.csv".format(location, day_short, frame, scope)
    probtripdest_dataset.to_csv(os.path.join(folder, name_csv), index=False, sep=';')
    return probtripdest_dataset

#%% Load databases
metro_wd = pd.read_csv(os.path.join(clust_metro, 'cluster-mtwd-im24.csv'), sep=';', index_col=[0])
big_wd = pd.read_csv(os.path.join(clust_big, 'cluster-bwd-im24.csv'), sep=';', index_col=[0])
middle_wd = pd.read_csv(os.path.join(clust_middle, 'cluster-mdwd-im24.csv'), sep=';', index_col=[0])
small_wd = pd.read_csv(os.path.join(clust_small, 'cluster-swd-im24.csv'), sep=';', index_col=[0])
rural_wd = pd.read_csv(os.path.join(clust_rural, 'cluster-rwd-im24.csv'), sep=';', index_col=[0])

metro_sa = pd.read_csv(os.path.join(clust_metro, 'cluster-mtsa-im24.csv'), sep=';', index_col=[0])
big_sa = pd.read_csv(os.path.join(clust_big, 'cluster-bsa-im24.csv'), sep=';', index_col=[0])
middle_sa = pd.read_csv(os.path.join(clust_middle, 'cluster-mdsa-im24.csv'), sep=';', index_col=[0])
small_sa = pd.read_csv(os.path.join(clust_small, 'cluster-ssa-im24.csv'), sep=';', index_col=[0])
rural_sa = pd.read_csv(os.path.join(clust_rural, 'cluster-rsa-im24.csv'), sep=';', index_col=[0])

metro_su = pd.read_csv(os.path.join(clust_metro, 'cluster-mtsu-im24.csv'), sep=';', index_col=[0])
big_su = pd.read_csv(os.path.join(clust_big, 'cluster-bsu-im24.csv'), sep=';', index_col=[0])
middle_su = pd.read_csv(os.path.join(clust_middle, 'cluster-mdsu-im24.csv'), sep=';', index_col=[0])
small_su = pd.read_csv(os.path.join(clust_small, 'cluster-ssu-im24.csv'), sep=';', index_col=[0])
rural_su = pd.read_csv(os.path.join(clust_rural, 'cluster-rsu-im24.csv'), sep=';', index_col=[0])

#%% Change destination taxonomy 
#(group work and school together under the name 'work' i.e. transform 2 in 1 for destination variable)
databases= [metro_wd, metro_sa,  metro_su, big_wd, big_sa, big_su, middle_wd, small_wd, rural_wd, middle_sa, small_sa, rural_sa, middle_su, small_su, rural_su]
for db in databases:
    g = db.columns.get_loc('00:00')
    h = db.columns.get_loc('23:55')
    db.iloc[:,g:h+1] = pd.DataFrame.replace(db.iloc[:,g:h+1], to_replace=2, value=1)
    db.iloc[:,g:h+1] = pd.DataFrame.replace(db.iloc[:,g:h+1], to_replace=3, value=2)
    db.iloc[:,g:h+1] = pd.DataFrame.replace(db.iloc[:,g:h+1], to_replace=4, value=3)
    db.iloc[:,g:h+1] = pd.DataFrame.replace(db.iloc[:,g:h+1], to_replace=5, value=4)

#%% Convert into departure slot datasets
metro_wd = dataset_seqdeparture(metro_wd)
big_wd = dataset_seqdeparture(big_wd)
middle_wd = dataset_seqdeparture(middle_wd)
small_wd = dataset_seqdeparture(small_wd)
rural_wd = dataset_seqdeparture(rural_wd)

metro_sa = dataset_seqdeparture(metro_sa)
big_sa = dataset_seqdeparture(big_sa)
middle_sa = dataset_seqdeparture(middle_sa)
small_sa = dataset_seqdeparture(small_sa)
rural_sa = dataset_seqdeparture(rural_sa)

metro_su = dataset_seqdeparture(metro_su)
big_su = dataset_seqdeparture(big_su)
middle_su = dataset_seqdeparture(middle_su)
small_su = dataset_seqdeparture(small_su)
rural_su = dataset_seqdeparture(rural_su)

#%% Generate probabilities at the cluster level for a given location (working days only)
probdest_metro_wd = prob_tables_clust_cs(metro_wd, [5])
probdest_big_wd = prob_tables_clust_cs(big_wd, [5])
probdest_middle_wd = prob_tables_clust_cs(middle_wd, [5])
probdest_small_wd = prob_tables_clust_cs(small_wd, [5])
probdest_rural_wd = prob_tables_clust_cs(rural_wd, [5])

#%% Generate probabilities for a group of clusters for a given location (working days only)
probdest_metro_wd_group = prob_tables_groupclust_cs(metro_wd, [2,3,4,5])
probdest_big_wd_group = prob_tables_groupclust_cs(big_wd, [3,4,5])
probdest_middle_wd_group = prob_tables_groupclust_cs(middle_wd, [4,5])
probdest_small_wd = prob_tables_clust_cs(small_wd, [5])
#%% Generate probabilities at the dataset level for a given location (saturday and sunday)
probdest_metro_sa = prob_tables_dataset_cs(metro_sa)
probdest_big_sa = prob_tables_dataset_cs(big_sa)
probdest_middle_sa = prob_tables_dataset_cs(middle_sa)
probdest_small_sa = prob_tables_dataset_cs(small_sa)
probdest_rural_sa = prob_tables_dataset_cs(rural_sa)

probdest_metro_su = prob_tables_dataset_cs(metro_su)
probdest_big_su = prob_tables_dataset_cs(big_su)
probdest_middle_su = prob_tables_dataset_cs(middle_su)
probdest_small_su = prob_tables_dataset_cs(small_su)
probdest_rural_su = prob_tables_dataset_cs(rural_su)

#%% Sort probabilities for each cluster/location or dataset/location in emobpy format (slow carsharing uptake scenario)
probtripdest_dataset(probdest_metro_wd, 'mt', 'weekday', 'wd', 'im24', 'clust5', distdest_cs_slow)
probtripdest_dataset(probdest_big_wd, 'b', 'weekday', 'wd', 'im24', 'clust5', distdest_cs_slow)
probtripdest_dataset(probdest_middle_wd, 'md', 'weekday', 'wd', 'im24', 'clust5', distdest_cs_slow)
probtripdest_dataset(probdest_small_wd, 's', 'weekday', 'wd', 'im24', 'clust5', distdest_cs_slow)
probtripdest_dataset(probdest_rural_wd, 'r', 'weekday', 'wd', 'im24', 'clust5', distdest_cs_slow)

probtripdest_dataset(probdest_metro_sa, 'mt', 'saturday', 'sa', 'im24', 'full', distdest_cs_slow)
probtripdest_dataset(probdest_big_sa, 'b', 'saturday', 'sa', 'im24', 'full', distdest_cs_slow)
probtripdest_dataset(probdest_middle_sa, 'md', 'saturday', 'sa', 'im24', 'full', distdest_cs_slow)
probtripdest_dataset(probdest_small_sa, 's', 'saturday', 'sa', 'im24', 'full', distdest_cs_slow)
probtripdest_dataset(probdest_rural_sa, 'r', 'saturday', 'sa', 'im24', 'full', distdest_cs_slow)

probtripdest_dataset(probdest_metro_su, 'mt', 'sunday', 'su', 'im24', 'full', distdest_cs_slow)
probtripdest_dataset(probdest_big_su, 'b', 'sunday', 'su', 'im24', 'full', distdest_cs_slow)
probtripdest_dataset(probdest_middle_su, 'md', 'sunday', 'su', 'im24', 'full', distdest_cs_slow)
probtripdest_dataset(probdest_small_su, 's', 'sunday', 'su', 'im24', 'full', distdest_cs_slow)
probtripdest_dataset(probdest_rural_su, 'r', 'sunday', 'su', 'im24', 'full', distdest_cs_slow)

#%% Sort probabilities for each group of clusters/location in emobpy format (fast carsharing uptake scenario)
probtripdest_dataset(probdest_metro_wd_group, 'mt', 'weekday', 'wd', 'im24', 'group25', distdest_cs_fast)
probtripdest_dataset(probdest_big_wd_group, 'b', 'weekday', 'wd', 'im24', 'group35', distdest_cs_fast)
probtripdest_dataset(probdest_middle_wd_group, 'md', 'weekday', 'wd', 'im24', 'group45', distdest_cs_fast)
probtripdest_dataset(probdest_small_wd, 's', 'weekday', 'wd', 'im24', 'clust5', distdest_cs_fast)

probtripdest_dataset(probdest_metro_sa, 'mt', 'saturday', 'sa', 'im24', 'full', distdest_cs_fast)
probtripdest_dataset(probdest_big_sa, 'b', 'saturday', 'sa', 'im24', 'full', distdest_cs_fast)
probtripdest_dataset(probdest_middle_sa, 'md', 'saturday', 'sa', 'im24', 'full', distdest_cs_fast)
probtripdest_dataset(probdest_small_sa, 's', 'saturday', 'sa', 'im24', 'full', distdest_cs_fast)

probtripdest_dataset(probdest_metro_su, 'mt', 'sunday', 'su', 'im24', 'full', distdest_cs_fast)
probtripdest_dataset(probdest_big_su, 'b', 'sunday', 'su', 'im24', 'full', distdest_cs_fast)
probtripdest_dataset(probdest_middle_su, 'md', 'sunday', 'su', 'im24', 'full', distdest_cs_fast)
probtripdest_dataset(probdest_small_su, 's', 'sunday', 'su', 'im24', 'full', distdest_cs_fast)

#%% Define a function: collect probabilities for a given cluster for the whole week (working days, saturday, sunday)
def dist_tripdestination_week_cs(location : str, frame : str, folder : Path, cluster: str) -> pd.DataFrame:
    """ Concatenates trip destination distributions for working days, saturday and sunday for a given cluster
    and export the output to a csv file in a specified folder.

    Parameters
    ----------

    location : str
        String to specify the location under scrutiny. 
        Can be mt, b, md, s or r.
        Used as a suffix in the name of the exported csv file.
    cluster : str 
        Used as a suffix in the name of the exported csv file. 
        Should be the same as the suffix used to generate probabilities at the day level in the previous stage using probtripdest_dataset.
    frame : str
        String to specify the destination typology and timeframe for the sequence clustering.
        Used as a suffix in the name of the exported csv file.
    folder : Path 
        Path to the folder where input csv files are taken from and to which the output csv file is exported.
    """
    name_wd = "disttripdest-{}wd-{}-{}.csv".format(location, frame, cluster)
    name_sa = "disttripdest-{}sa-{}-full.csv".format(location, frame)
    name_su = "disttripdest-{}su-{}-full.csv".format(location, frame)
    wd = pd.read_csv(os.path.join(folder, name_wd), sep=';', index_col=[0], low_memory=False)  
    sa = pd.read_csv(os.path.join(folder, name_sa), sep=';', index_col=[0], low_memory=False)
    su = pd.read_csv(os.path.join(folder, name_su), sep=';', index_col=[0], low_memory=False)
    week = pd.concat([sa, su, wd]) 
    week = week.fillna(0)
    name_csv = "cs-disttripdest-week-{}-{}-{}.csv".format(location, frame, cluster)
    week.to_csv(os.path.join(folder / 'week', name_csv), index=True, sep=',')
    return week

#%% Compute excel files for the whole week for all five regions at the cluster level
dist_tripdestination_week_cs('mt', 'im24', distdest_cs_slow, 'clust5')
dist_tripdestination_week_cs('b', 'im24', distdest_cs_slow, 'clust5')
dist_tripdestination_week_cs('md',  'im24', distdest_cs_slow, 'clust5')
dist_tripdestination_week_cs('s', 'im24', distdest_cs_slow, 'clust5')
dist_tripdestination_week_cs('r', 'im24', distdest_cs_slow, 'clust5')

#%% Compute excel files for the whole week for all five regions for groups of clusters 
dist_tripdestination_week_cs('mt', 'im24', distdest_cs_fast, 'group25')
dist_tripdestination_week_cs('b', 'im24', distdest_cs_fast, 'group35')
dist_tripdestination_week_cs('md',  'im24', distdest_cs_fast, 'group45')
dist_tripdestination_week_cs('s', 'im24', distdest_cs_fast, 'clust5')