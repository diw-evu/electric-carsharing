######################################################
###### Distribution of number of trips per cars ######
################# for shared cars ####################

#%% Load packages and define paths 
import os 
import numpy as np
import pandas as pd 
from pathlib import Path 
from seqdeparture import dataset_seqdeparture
import copy

# Define paths
results = Path('/Adeline/ecarsharing/SeqResults') #change this path when working on another operating system - the structure of the results folder should nevertheless be the same
clusters = results / 'clusters'
clust_metro = clusters / 'metro'
clust_big = clusters / 'big'
clust_middle = clusters / 'middle'
clust_small = clusters / 'small'
clust_rural = clusters / 'rural'
dist = results / 'distributions' 
distntrips_week = results / 'distributions' / 'private' / 'ntrips' / 'week'
distntrips_cs_slow = dist / 'carsharing' / 'slow' / 'ntrips'
distntrips_cs_fast = dist / 'carsharing' / 'fast' / 'ntrips'
distntrips_cs_week_slow = distntrips_cs_slow / 'week'
distntrips_cs_week_fast = distntrips_cs_fast / 'week'

#%% Define a function: get the average number of trips per shared cars at the *cluster level* assuming a given substitution rate (sigma)
def avg_cartrips_cs_clust(location: str, day: str, frame: str, cluster: int, sigma: int =5) -> int:
    """
    Function that computes the average number of trips per shared cars assuming a given substitution rate (sigma)
    at the cluster level for a given location/day/frame. 
    For each cluster, it gets the overall number of sequences and divide by sigma, which gives the number of shared cars.
    It then computes the overall number of trips and divide by the number of shared cars to get the average number of trips
    per shared cars.

    Parameters
    ----------
    location: str
        Can be 'mt', 'b', 'md', 's' or 'r'. 
    day: str
        Short version of the day type i.e. 'wd' for weekday, 'sa' for saturday, 'su' for sunday.
    frame: str
        Refers to the way sequences were computed and clustered. Usually 'im24'.
    cluster: int
        Cluster that is under scrutiny. Should not exceed the overall number of clusters in a dataframe.
    sigma: optional, int
        By default set to 5 i.e. one shared cars replaces 5 privately-owned cars.
    """
    # Define cluster folder dictionnary
    folder_clust = {
    'mt': clust_metro,
    'b' : clust_big,
    'md': clust_middle,
    's': clust_small,
    'r': clust_rural
    }
    data = 'cluster-{}{}-{}.csv'.format(location, day, frame)
    sequence = dataset_seqdeparture(pd.read_csv(os.path.join(folder_clust[location], data), sep=';', index_col=[0]))
    nb_rows = sequence.groupby('Cluster')['HP_ID'].nunique()
    nb_trips = sequence.groupby('Cluster')['ntrips'].sum()
    nb_shared_cars = (nb_rows / sigma).round(0).astype(int)
    av_ntrips_cs_clust = (nb_trips[cluster] / nb_shared_cars[cluster]).round(0).astype(int)
    return av_ntrips_cs_clust

#%% Define a function: get the average number of trips per shared cars for a *group of clusters* for a given location,
#   assuming a given substitution rate (sigma)
def avg_cartrips_cs_groupclust(location: str, day: str, frame: str, clustergroup: list, sigma: int =5) -> int:
    """
    Function that computes the average number of trips per shared cars assuming a given substitution rate (sigma)
    at the level of dataset filtered to a group of clusters for a given location/day/frame. 
    For the filtered dataset, it gets the overall number of sequences and divide by sigma, which gives the number of shared cars.
    It then computes the overall number of trips and divide by the number of shared cars to get the average number of trips
    per shared cars.

    Parameters
    ----------
    location: str
        Can be 'mt', 'b', 'md', 's' or 'r'. 
    day: str
        Short version of the day type i.e. 'wd' for weekday, 'sa' for saturday, 'su' for sunday.
    frame: str
        Refers to the way sequences were computed and clustered. Usually 'im24'.
    clustergroup: list
        Clusters that are to be included in the dataset. Should not exceed the overall number of clusters in a dataframe.
    sigma: optional, int
        By default set to 5 i.e. one shared cars replaces 5 privately-owned cars.
    """
    # Define cluster folder dictionnary
    folder_clust = {
    'mt': clust_metro,
    'b' : clust_big,
    'md': clust_middle,
    's': clust_small,
    'r': clust_rural
    }
    data = 'cluster-{}{}-{}.csv'.format(location, day, frame)
    sequence = dataset_seqdeparture(pd.read_csv(os.path.join(folder_clust[location], data), sep=';', index_col=[0]))
    sequence = sequence[sequence['Cluster'].isin(clustergroup)]
    nb_rows = sequence['HP_ID'].nunique()
    nb_trips = sequence['ntrips'].sum()
    nb_shared_cars = int(round((nb_rows / sigma), 0))
    av_ntrips_cs_groupclust = int(round((nb_trips / nb_shared_cars), 0))
    return av_ntrips_cs_groupclust

#%% Define a function: get the average number of trips per shared cars at the *dataset level* assuming a given substitution rate (sigma)
def avg_cartrips_cs_dataset(location: str, day: str, frame: str, sigma: int = 5) -> int:
    """
    Function that computes the average number of trips per shared cars assuming a given substitution rate (sigma)
    at the dataset level for a given location/day/frame. 
    For each dataset, it gets the overall number of sequences and divide by sigma, which gives the number of shared cars.
    It then computes the overall number of trips and divide by the number of shared cars to get the average number of trips
    per shared cars.

    Parameters
    ----------
    location: str
        Can be 'mt', 'b', 'md', 's' or 'r'. 
    day: str
        Short version of the day type i.e. 'wd' for weekday, 'sa' for saturday, 'su' for sunday.
    frame: str
        Refers to the way sequences were computed and clustered. Usually 'im24'.
    sigma: optional, int
        By default set to 5 i.e. one shared cars replaces 5 privately-owned cars.
    """
    # Define cluster folder dictionnary
    folder_clust = {
    'mt': clust_metro,
    'b' : clust_big,
    'md': clust_middle,
    's': clust_small,
    'r': clust_rural
    }
    data = 'cluster-{}{}-{}.csv'.format(location, day, frame)
    sequence = dataset_seqdeparture(pd.read_csv(os.path.join(folder_clust[location], data), sep=';', index_col=[0]))
    nb_rows = sequence['HP_ID'].nunique()
    nb_trips = sequence['ntrips'].sum()
    nb_shared_cars = int(round((nb_rows / sigma), 0))
    av_ntrips_cs_dataset = int(round((nb_trips / nb_shared_cars), 0))
    return av_ntrips_cs_dataset

#%% Define a function: get the median number of trips for privately-owned cars for each cluster/location/day/frame.

def avg_cartrips_poc(location: str, day_long: str, frame: str, cluster: int, folder_in: Path):
    """
    Function that computes the most frequent number of trips at the cluster level for privately-owned cars.

    Parameters
    ----------

    location: str
        Can be 'mt', 'b', 'md', 's' or 'r'.
    day_long: str
        Day type. Can be 'weekday', 'saturday' or 'sunday'
    frame: str
        Refers to the way sequences were computed and clustered. Usually 'im24'
    cluster: int
         Cluster that is under scrutiny. Should not exceed the overall number of clusters in a dataframe.
    folder_in: Path
    """
    data = 'distntrips-week-{}-{}-{}.csv'.format(location, frame, cluster)
    proba = pd.read_csv(os.path.join(folder_in, data), sep=',', index_col=[0])
    avg_ntrips_poc = proba[day_long].idxmax().astype(int)
    return avg_ntrips_poc

#%% Define a function: compute the distribution of number of trips for shared cars for a given cluster/day/location/frame assuming a given substitution rate (sigma)

def ntrips_cs_clust(location: str, day_short: str, frame: str, cluster: int, folder_ntrips_poc: Path, sigma: int =5):
    """
    Function that computes the distribution of number of trips for shared cars at the cluster level
    under a given assumption for the substitution rate between private-owned and shared cars (sigma).
    This substitution rate can be changed when adding a last argument (int) to the function. Defaullt value is 5.

    Parameters 
    ----------

    location: str
        Can be 'mt', 'b', 'md', 's' or 'r'. 
    day_short: str
        Short version of the day type i.e. 'wd' for weekday, 'sa' for saturday, 'su' for sunday.
    frame: str
        Refers to the way sequences were computed and clustered. Usually 'im24'.
    cluster: str
        Cluster that is under scrutiny. Should not exceed the overall number of clusters in a dataframe.
    folder_ntrips_poc: Path
        Path to the folder where distribution for number of trips for privately-owned cars are stored.
    sigma: optional, int 
        Substitution rate between privately-owned cars and shared cars.
        By default set to 5 i.e. one shared cars replaces 5 privately-owned cars.
    """
    # Define dictionary of correspondence between short and long names for days
    dict_days = {
    'wd': 'weekday',
    'sa': 'saturday',
    'su': 'sunday'
    }
    # Define list of columns to be removed
    days = ['weekday', 'saturday', 'sunday']
    day_chosen = [dict_days[day_short]]
    remove_col = list(set(days) - set(day_chosen))
    # Computations
    data = 'distntrips-week-{}-{}-{}.csv'.format(location, frame, cluster)
    avg = avg_cartrips_poc(location, dict_days[day_short], frame, cluster, folder_ntrips_poc)
    new_avg = avg_cartrips_cs_clust(location, day_short, frame, cluster, sigma)
    proba = pd.read_csv(os.path.join(folder_ntrips_poc, data), sep=',', index_col=[0]).reset_index().drop(columns=remove_col)
    diff = new_avg - avg
    proba.loc[:, 'trip'] = proba.loc[:, 'trip'] + diff
    proba = proba.set_index('trip')
    return proba

#%% Define a function: compute the distribution of number of trips for shared cars for a group of clusters for a given day/location/frame 
#   assuming a given substitution rate (sigma)

def ntrips_cs_groupclust(location: str, day_short: str, frame: str, clustergroup: list, folder_ntrips_poc: Path,  sigma: int =5):
    """
    Function that computes the distribution of number of trips for shared cars at the level of a dataset filtered to a group of clusters 
    for a given location/day under a given assumption for the substitution rate between private-owned and shared cars (sigma).
    This substitution rate can be changed when adding a last argument (int) to the function. Default value is 5.

    Parameters 
    ----------

    location: str
        Can be 'mt', 'b', 'md', 's' or 'r'. 
    day_short: str
        Short version of the day type i.e. 'wd' for weekday, 'sa' for saturday, 'su' for sunday.
    frame: str
        Refers to the way sequences were computed and clustered. Usually 'im24'.
    clustergroup: list
        List of clusters to be included in the dataset.
    folder_ntrips_poc: Path
        Path to the folder where distribution for number of trips for privately-owned cars are stored.
    sigma: optional, int 
        Substitution rate between privately-owned cars and shared cars.
        By default set to 5 i.e. one shared cars replaces 5 privately-owned cars.
    """
    # Define dictionary of correspondence between short and long names for days
    dict_days = {
    'wd': 'weekday',
    'sa': 'saturday',
    'su': 'sunday'
    }
    # Retrieve information on the clusters that are in the group
    clustermin = str(clustergroup[0])
    clustermax = str(clustergroup[-1])
    cluster = clustermin + clustermax
    cluster = int(cluster)
    # Define list of columns to be removed
    days = ['weekday', 'saturday', 'sunday']
    day_chosen = [dict_days[day_short]]
    remove_col = list(set(days) - set(day_chosen))
    # Computations
    data = 'distntrips-week-{}-{}-{}.csv'.format(location, frame, cluster)
    avg = avg_cartrips_poc(location, dict_days[day_short], frame, cluster, folder_ntrips_poc)
    new_avg = avg_cartrips_cs_groupclust(location, day_short, frame, clustergroup, sigma)
    proba = pd.read_csv(os.path.join(folder_ntrips_poc, data), sep=',', index_col=[0]).reset_index().drop(columns=remove_col)
    diff = new_avg - avg
    proba.loc[:, 'trip'] = proba.loc[:, 'trip'] + diff
    proba = proba.set_index('trip')
    return proba

#%% Define a function: compute the distribution of number of trips for shared cars for a given day/location/frame assuming a given substitution rate (sigma)

def ntrips_cs_dataset(location: str, day_short: str, frame: str, folder_ntrips_poc: Path, sigma: int =5, cluster=2):
    """
    Function that computes the distribution of number of trips for shared cars at the dataset level
    under a given assumption for the substitution rate between private-owned and shared cars (sigma).
    This substitution rate can be changed when adding a last argument (int) to the function. Default value is 5.

    Parameters 
    ----------

    location: str
        Can be 'mt', 'b', 'md', 's' or 'r'. 
    day_short: str
        Short version of the day type i.e. 'wd' for weekday, 'sa' for saturday, 'su' for sunday.
    frame: str
        Refers to the way sequences were computed and clustered. Usually 'im24'.
    folder_ntrips_poc: Path
        Path to the folder where distribution for number of trips for privately-owned cars are stored.
    sigma: optional, int 
        Substitution rate between privately-owned cars and shared cars.
        By default set to 5 i.e. one shared cars replaces 5 privately-owned cars.
    """
    # Define dictionary of correspondence between short and long names for days
    dict_days = {
    'wd': 'weekday',
    'sa': 'saturday',
    'su': 'sunday'
    }
    # Define list of columns to be removed
    days = ['weekday', 'saturday', 'sunday']
    day_chosen = [dict_days[day_short]]
    remove_col = list(set(days) - set(day_chosen))
    # Computations
    data = 'distntrips-week-{}-{}-{}.csv'.format(location, frame, cluster)
    avg = avg_cartrips_poc(location, dict_days[day_short], frame, cluster, folder_ntrips_poc)
    new_avg = avg_cartrips_cs_dataset(location, day_short, frame, sigma)
    proba = pd.read_csv(os.path.join(folder_ntrips_poc, data), sep=',', index_col=[0]).reset_index().drop(columns=remove_col)
    diff = new_avg - avg
    proba.loc[:, 'trip'] = proba.loc[:, 'trip'] + diff
    proba = proba.set_index('trip')
    return proba

#%% Define a function: compute dataframe collecting number of trips distribution for shared cars for all day type over the week at the cluster level.

def ntrips_cs_week_clust(location: str, frame: str, cluster: int, sigma: int = 5, folder_out: Path = distntrips_cs_week_slow) -> pd.DataFrame:
    """
    Function that computes a dataframe collecting the distribution of number of trips for shared cars for all day type over the week.
    Substitution rate between privately-owned and shared cars can be customized.
    Exports the dataframe to a csv file in the specified folder_out.

    Parameters
    ----------

    location: str
        Can be 'mt', 'b', 'md', 's' or 'r'. 
    frame: str
        Refers to the way sequences were computed and clustered. Usually 'im24'.
    cluster: str
        Cluster that is under scrutiny. Should not exceed the overall number of clusters in a dataframe.
    folder_out: Path
        Path to the folder where the csv file of the distribution for number of trips for shared cars is exported.
    sigma: optional, int 
        Substitution rate between privately-owned cars and shared cars.
        By default set to 5 i.e. one shared cars replaces 5 privately-owned cars.
    """
    weekday = ntrips_cs_clust(location, 'wd', frame, cluster, distntrips_week, sigma)
    saturday = ntrips_cs_dataset(location, 'sa', frame, distntrips_week, sigma)
    sunday = ntrips_cs_dataset(location, 'su', frame, distntrips_week, sigma)
    week = weekday.merge(saturday, left_on='trip', right_on='trip', how='outer')
    week = week.merge(sunday, right_on='trip', left_on='trip', how='outer')
    week = week.fillna(0)
    week = week.sort_index(ascending=True)
    name_csv = 'cs-distntrips-week-{}-{}-{}-sr{}.csv'.format(location, frame, cluster, sigma)
    week.to_csv(os.path.join(folder_out, name_csv), index=True, sep=',')
    return week

#%% Define a function: compute dataframe collecting number of trips distribution for shared cars for all day type over the week at the level of a group of clusters.

def ntrips_cs_week_groupclust(location: str, frame: str, clustergroup: list, sigma: int = 5, folder_out: Path = distntrips_cs_week_fast) -> pd.DataFrame:
    """
    Function that computes a dataframe collecting the distribution of number of trips for shared cars for all day type over the week.
    Substitution rate between privately-owned and shared cars can be customized.
    Exports the dataframe to a csv file in the specified folder_out.

    Parameters
    ----------

    location: str
        Can be 'mt', 'b', 'md', 's' or 'r'. 
    frame: str
        Refers to the way sequences were computed and clustered. Usually 'im24'.
    clustergroup: list
        Clusters that need to be kept in the dataset.
    folder_out: Path
        Path to the folder where the csv file of the distribution for number of trips for shared cars is exported.
    sigma: optional, int 
        Substitution rate between privately-owned cars and shared cars.
        By default set to 5 i.e. one shared cars replaces 5 privately-owned cars.
    """
    weekday = ntrips_cs_groupclust(location, 'wd', frame, clustergroup, distntrips_week, sigma)
    saturday = ntrips_cs_dataset(location, 'sa', frame, distntrips_week, sigma)
    sunday = ntrips_cs_dataset(location, 'su', frame, distntrips_week, sigma)
    week = weekday.merge(saturday, left_on='trip', right_on='trip', how='outer')
    week = week.merge(sunday, right_on='trip', left_on='trip', how='outer')
    week = week.fillna(0)
    week = week.sort_index(ascending=True)
    # Retrieve information on the clusters that are in the group
    clustermin = str(clustergroup[0])
    clustermax = str(clustergroup[-1])
    cluster = clustermin + clustermax
    cluster = int(cluster)
    name_csv = 'cs-distntrips-week-{}-{}-{}-sr{}.csv'.format(location, frame, cluster, sigma)
    week.to_csv(os.path.join(folder_out, name_csv), index=True, sep=',')
    return week

#%% Load data and compute probability distribution for the slow uptake scenario
locations = ['mt', 'b', 'md', 's', 'r']

for l in locations:
    for c in range(2,6):
        ntrips_cs_week_clust(l, 'im24', c)

#%% Load data and compute probability distribution for the fast uptake scenario
ntrips_cs_week_groupclust('mt', 'im24', [2,3,4,5])
ntrips_cs_week_groupclust('b', 'im24',  [3,4,5])
ntrips_cs_week_groupclust('md', 'im24', [4,5])
