###############################################
##### Distribution of the number of trips #####
###############################################
#%% Load packages 
import os
import pandas as pd
from pathlib import Path 
from seqdeparture import dataset_seqdeparture

cwd = os.getcwd()
print("Current working directory: {0}".format(cwd))

# Define paths
results = Path('/Adeline/ecarsharing/SeqResults')    # change this path when working on another operating system - the structure of the results folder should nevertheless be the same
clusters = results / 'clusters'
clust_metro = clusters / 'metro'
clust_big = clusters / 'big'
clust_middle = clusters / 'middle'
clust_small = clusters / 'small'
clust_rural = clusters / 'rural'
dist = results / 'distributions' 
distntrips = dist / 'private' / 'ntrips'
distntrips_week = distntrips / 'week'


#%% Function to get the distribution of trips *for each cluster* of a given location/day and export to csv file

def df_dist_ntrips_clust(data : pd.DataFrame, location : str, day : str, frame : str, folder : Path) -> pd.DataFrame:
    """ Compute the distribution of number of trips for each cluster in a given location and export to a csv file.

    Parameters
    -----------
    data : dataframe 
        The name of the dataset to be used 
    location : string 
        suffix to the name of the exported csv file to know which location it concerns
    day : string
        suffix to the name of the exported csv file to know which day it concerns
    frame : string
        suffix to the name of the exported csv file to indicate the sequence timeframe and destination typology
    folder: path
        path to the folder where the csv file is exported
    """ 

    nclust = data['Cluster'].nunique()
    names_int = []
    for c in range(0,nclust):
        names_int.append(c)
    names = map(str, names_int)
    names = list(names)
    names = list(map(lambda x: 'Cluster ' + x, names))
    data['ntrips'] = data['ntrips'].astype(int)
    max_ntrips = data['ntrips'].max()
    ind = []
    for n in range(0, max_ntrips+1):
        ind.append(n)
    dist_ntrips_clust = pd.DataFrame(index=ind, columns=names)
    for (n,c) in zip(names, range(0, nclust)):
        dist_ntrips_clust[n] = data[data['Cluster']==c]['ntrips'].value_counts(normalize=True).to_frame()
        # Remove probabilities smaller than 0.01
        small_proba = dist_ntrips_clust[dist_ntrips_clust[n] <= 0.015].index.to_list()
        dist_ntrips_clust.iloc[small_proba, c] = 0
        # Rescale
        s= dist_ntrips_clust[n].sum()
        dist_ntrips_clust[n] = dist_ntrips_clust[n] / s
        # Replace NaN by 0
    dist_ntrips_clust = dist_ntrips_clust.fillna(0)
    name_csv = "distntrips-{}{}-{}.csv".format(location, day, frame)
    dist_ntrips_clust.to_csv(os.path.join(folder, name_csv), index=True, sep=';')
    return dist_ntrips_clust

#%% Function to get the distribution of trips *for the whole dataset* of a given location/day and export to csv file

def df_dist_ntrips(data : pd.DataFrame, location : str, day : str, frame : str, folder : Path) -> pd.DataFrame:
    """ Compute the distribution of number of trips in a given location and export to a csv file.

    Parameters
    -----------
    data : dataframe 
        The name of the dataset to be used 
    location : string 
        suffix to the name of the exported csv file to know which location it concerns
    day : string
        suffix to the name of the exported csv file to know which day it concerns
    frame : string
        suffix to the name of the exported csv file to indicate the sequence timeframe and destination typology
    folder: path
        path to the folder where the csv file is exported
    """ 
    data['ntrips'] = data['ntrips'].astype(int)
    max_ntrips = data['ntrips'].max()
    ind = []
    for n in range(0, max_ntrips+1):
        ind.append(n)
    dist_ntrips = pd.DataFrame(index=ind)
    dist_ntrips['whole sample'] = data['ntrips'].value_counts(normalize=True).to_frame()
    # Remove probabilities smaller than 0.01
    small_proba = dist_ntrips[dist_ntrips['whole sample'] <= 0.015].index.to_list()
    dist_ntrips.iloc[small_proba, 0] = 0
    # Rescale
    s = dist_ntrips['whole sample'].sum()
    dist_ntrips['whole sample'] = dist_ntrips['whole sample'] / s
    # Replace NaN by 0
    dist_ntrips = dist_ntrips.fillna(0)
    name_csv = "distntrips-{}{}-{}.csv".format(location, day, frame)
    dist_ntrips.to_csv(os.path.join(folder, name_csv), index=True, sep=';')
    return dist_ntrips

#%% Function to get the distribution of trips *for a group of clusters* of a given location/day and export to csv file

def df_dist_ntrips_clustgroup(data : pd.DataFrame, location : str, day : str, clustergroup: list, frame : str, folder : Path) -> pd.DataFrame:
    """ Compute the distribution of number of trips in a given location and export to a csv file.

    Parameters
    -----------
    data : dataframe 
        The name of the dataset to be used 
    location : string 
        suffix to the name of the exported csv file to know which location it concerns
    day : string
        suffix to the name of the exported csv file to know which day it concerns
    clustergroup: list 
        List of groups to be included in the dataset.
    frame : string
        suffix to the name of the exported csv file to indicate the sequence timeframe and destination typology
    folder: path
        path to the folder where the csv file is exported
    """ 
    data = data[data['Cluster'].isin(clustergroup)]
    data['ntrips'] = data['ntrips'].astype(int)
    max_ntrips = data['ntrips'].max()
    ind = []
    for n in range(0, max_ntrips+1):
        ind.append(n)
    dist_ntrips = pd.DataFrame(index=ind)
    dist_ntrips['clustergroup'] = data['ntrips'].value_counts(normalize=True).to_frame()
    # Remove probabilities smaller than 0.01
    small_proba = dist_ntrips[dist_ntrips['clustergroup'] <= 0.015].index.to_list()
    dist_ntrips.iloc[small_proba, 0] = 0
    # Rescale
    s = dist_ntrips['clustergroup'].sum()
    dist_ntrips['clustergroup'] = dist_ntrips['clustergroup'] / s
    # Replace NaN by 0
    dist_ntrips = dist_ntrips.fillna(0)
    name_csv = "distntrips-{}{}-clustgroup-{}.csv".format(location, day, frame)
    dist_ntrips.to_csv(os.path.join(folder, name_csv), index=True, sep=';')
    return dist_ntrips

#%% Load databases 
#%% Working days
metro_wd = dataset_seqdeparture(pd.read_csv(os.path.join(clust_metro, 'cluster-mtwd-im24.csv'), sep=';', index_col=[0]))
big_wd = dataset_seqdeparture(pd.read_csv(os.path.join(clust_big, 'cluster-bwd-im24.csv'), sep=';', index_col=[0]))
middle_wd = dataset_seqdeparture(pd.read_csv(os.path.join(clust_middle, 'cluster-mdwd-im24.csv'), sep=';', index_col=[0]))
small_wd = dataset_seqdeparture(pd.read_csv(os.path.join(clust_small, 'cluster-swd-im24.csv'), sep=';', index_col=[0]))
rural_wd = dataset_seqdeparture(pd.read_csv(os.path.join(clust_rural, 'cluster-rwd-im24.csv'), sep=';', index_col=[0]))
#%% Saturday
metro_sa = dataset_seqdeparture(pd.read_csv(os.path.join(clust_metro, 'cluster-mtsa-im24.csv'), sep=';', index_col=[0]))
big_sa = dataset_seqdeparture(pd.read_csv(os.path.join(clust_big, 'cluster-bsa-im24.csv'), sep=';', index_col=[0]))
middle_sa = dataset_seqdeparture(pd.read_csv(os.path.join(clust_middle, 'cluster-mdsa-im24.csv'), sep=';', index_col=[0]))
small_sa = dataset_seqdeparture(pd.read_csv(os.path.join(clust_small, 'cluster-ssa-im24.csv'), sep=';', index_col=[0]))
rural_sa = dataset_seqdeparture(pd.read_csv(os.path.join(clust_rural, 'cluster-rsa-im24.csv'), sep=';', index_col=[0]))
#%% Sunday
metro_su = dataset_seqdeparture(pd.read_csv(os.path.join(clust_metro, 'cluster-mtsu-im24.csv'), sep=';', index_col=[0]))
big_su = dataset_seqdeparture(pd.read_csv(os.path.join(clust_big, 'cluster-bsu-im24.csv'), sep=';', index_col=[0]))
middle_su = dataset_seqdeparture(pd.read_csv(os.path.join(clust_middle, 'cluster-mdsu-im24.csv'), sep=';', index_col=[0]))
small_su = dataset_seqdeparture(pd.read_csv(os.path.join(clust_small, 'cluster-ssu-im24.csv'), sep=';', index_col=[0]))
rural_su = dataset_seqdeparture(pd.read_csv(os.path.join(clust_rural, 'cluster-rsu-im24.csv'), sep=';', index_col=[0]))

#%% Compute distribution of number of trips for all locations at the cluster level (working days)
dataframes_wd = [metro_wd, big_wd, middle_wd, small_wd, rural_wd]
locations = ['mt', 'b', 'md', 's', 'r']

for (df, loc) in zip(dataframes_wd, locations):
    df_dist_ntrips_clust(df, loc, 'wd', 'im24', distntrips)

#%% Compute distribution of number of trips for all locations for a group of clusters at the location level (working day)
# Clusters 2 to 5 from metropolises
df_dist_ntrips_clustgroup(metro_wd, 'mt', 'wd', [2,3,4,5],'im24', distntrips)
# Clusters 3 to 5 from big cities
df_dist_ntrips_clustgroup(big_wd, 'b', 'wd', [3,4,5],'im24', distntrips)
# Clusters 4 and 5 from middle-size cities
df_dist_ntrips_clustgroup(middle_wd, 'md', 'wd', [4,5],'im24', distntrips)

#%% Compute distribution of number of trips for all locations at the dataset level (saturday and sunday)

dataframes_sa = [metro_sa, big_sa, middle_sa, small_sa, rural_sa]
dataframes_su = [metro_su, big_su, middle_su, small_su, rural_su]
locations = ['mt', 'b', 'md', 's', 'r']

for (df, loc) in zip(dataframes_sa, locations):
    df_dist_ntrips(df, loc, 'sa', 'im24', distntrips)

for (df, loc) in zip(dataframes_su, locations):
    df_dist_ntrips(df, loc, 'su', 'im24', distntrips)

#%% Define a function: collect distribution of number of trips for a given cluster for all days in one dataframe

def dist_ntrips_week_clust(location : str, cluster : int, frame : str, folder_in : Path, folder_out: Path) -> pd.DataFrame:
    """ Compute distribution of number of trips for a given cluster and location for all days in one dataframe
    and export output dataframe to a csv file in a specified folder.

    Parameters
    ----------

    location: str
    cluster: int
    frame: str
    folder: Path
    """
    name_wd = "distntrips-{}wd-{}.csv".format(location, frame)
    name_sa = "distntrips-{}sa-{}.csv".format(location, frame)
    name_su = "distntrips-{}su-{}.csv".format(location, frame)
    wd = pd.read_csv(os.path.join(folder_in, name_wd), sep=';', index_col=[0], low_memory=False)
    sa = pd.read_csv(os.path.join(folder_in, name_sa), sep=';', index_col=[0], low_memory=False)
    su = pd.read_csv(os.path.join(folder_in, name_su), sep=';', index_col=[0], low_memory=False)
    ntrips_weekend = sa.merge(su, left_index=True, right_index=True, how='outer')
    ntrips_week_clust = ntrips_weekend.merge(wd.iloc[:, cluster], left_index=True, right_index=True, how='outer')
    cluster_name = "Cluster {}".format(cluster)
    ntrips_week_clust = pd.DataFrame.rename(ntrips_week_clust, columns={'whole sample_x':'saturday', 'whole sample_y':'sunday', cluster_name:'weekday'})
    ntrips_week_clust = pd.DataFrame(ntrips_week_clust, 
                                     columns=['weekday', 'saturday', 'sunday'])
    ntrips_week_clust.index.name = "trip"
    ntrips_week_clust = ntrips_week_clust.fillna(0).round(3)
    ntrips_week_clust = ntrips_week_clust.sort_index(ascending=True)
    name_csv = "distntrips-week-{}-{}-{}.csv".format(location, frame, cluster)
    ntrips_week_clust.to_csv(os.path.join(folder_out, name_csv), index=True, sep=',')
    return ntrips_week_clust

#%% Define a function: collect distribution of number of trips for a group of clusters for all days in one dataframe

def dist_ntrips_week_clustgroup(location : str, clustergroup : list, frame : str, folder_in : Path, folder_out: Path) -> pd.DataFrame:
    """ Compute distribution of number of trips for a given cluster and location for all days in one dataframe
    and export output dataframe to a csv file in a specified folder.

    Parameters
    ----------

    location: str
    clustergroup: list
    frame: str
    folder: Path
    """
    name_wd = "distntrips-{}wd-clustgroup-{}.csv".format(location, frame)
    name_sa = "distntrips-{}sa-{}.csv".format(location, frame)
    name_su = "distntrips-{}su-{}.csv".format(location, frame)
    clustermin = clustergroup[0]
    clustermax = clustergroup[-1]
    wd = pd.read_csv(os.path.join(folder_in, name_wd), sep=';', index_col=[0], low_memory=False)
    sa = pd.read_csv(os.path.join(folder_in, name_sa), sep=';', index_col=[0], low_memory=False)
    su = pd.read_csv(os.path.join(folder_in, name_su), sep=';', index_col=[0], low_memory=False)
    ntrips_weekend = sa.merge(su, left_index=True, right_index=True, how='outer')
    ntrips_week_clust = ntrips_weekend.merge(wd, left_index=True, right_index=True, how='outer')
    ntrips_week_clust = pd.DataFrame.rename(ntrips_week_clust, columns={'whole sample_x':'saturday', 'whole sample_y':'sunday', 'clustergroup':'weekday'})
    ntrips_week_clust = pd.DataFrame(ntrips_week_clust, 
                                     columns=['weekday', 'saturday', 'sunday'])
    ntrips_week_clust.index.name = "trip"
    ntrips_week_clust = ntrips_week_clust.fillna(0).round(3)
    ntrips_week_clust = ntrips_week_clust.sort_index(ascending=True)
    name_csv = "distntrips-week-{}-{}-{}{}.csv".format(location, frame, clustermin, clustermax)
    ntrips_week_clust.to_csv(os.path.join(folder_out, name_csv), index=True, sep=',')
    return ntrips_week_clust

#%% Compute csv files for probability distributions at the cluster level 
for c in range(2,6):
    dist_ntrips_week_clust('mt', c,  'im24', distntrips, distntrips_week)
    dist_ntrips_week_clust('b', c,  'im24', distntrips, distntrips_week)
    dist_ntrips_week_clust('md', c,  'im24', distntrips, distntrips_week)
    dist_ntrips_week_clust('r', c,  'im24', distntrips, distntrips_week)
    dist_ntrips_week_clust('s', c,  'im24', distntrips, distntrips_week)
#%% Compute csv files for probability distributions for groups of clusters 
dist_ntrips_week_clustgroup('mt', [2,3,4,5],  'im24', distntrips, distntrips_week)
dist_ntrips_week_clustgroup('b',  [3,4,5],  'im24', distntrips, distntrips_week)
dist_ntrips_week_clustgroup('md', [4,5],  'im24', distntrips, distntrips_week)
