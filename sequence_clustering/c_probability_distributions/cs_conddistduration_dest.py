###########################################################################
###### Joint conditional distribution of trip distance and duration  ######
#################### conditional on: trip destination #####################
######################  for carsharing time series   ######################
###########################################################################
#%% Load packages and define paths 
import os
import pandas as pd
from pathlib import Path 
from numpy import NaN
from seqdeparture import dataset_seqdeparture

# Define paths
results = Path('/Adeline/ecarsharing/SeqResults') #change this path when working on another operating system - the structure of the results folder should nevertheless be the same
clusters = results / 'clusters'
clust_metro = clusters / 'metro'
clust_big = clusters / 'big'
clust_middle = clusters / 'middle'
clust_small = clusters / 'small'
clust_rural = clusters / 'rural'
dist = results / 'distributions' 
conddurdist_cs_slow = dist / 'carsharing' / 'slow' / 'conddurdist'
conddurdist_cs_week_slow = conddurdist_cs_slow / 'week'
conddurdist_cs_fast = dist / 'carsharing' / 'fast' / 'conddurdist'
conddurdist_cs_week_fast = conddurdist_cs_fast / 'week'


#%% Load cluster sequences
metro_wd = dataset_seqdeparture(pd.read_csv(os.path.join(clust_metro, 'cluster-mtwd-im24.csv'), sep=';', index_col=[0]))
big_wd = dataset_seqdeparture(pd.read_csv(os.path.join(clust_big, 'cluster-bwd-im24.csv'), sep=';', index_col=[0]))
middle_wd = dataset_seqdeparture(pd.read_csv(os.path.join(clust_middle, 'cluster-mdwd-im24.csv'), sep=';', index_col=[0]))
small_wd = dataset_seqdeparture(pd.read_csv(os.path.join(clust_small, 'cluster-swd-im24.csv'), sep=';', index_col=[0]))
rural_wd = dataset_seqdeparture(pd.read_csv(os.path.join(clust_rural, 'cluster-rwd-im24.csv'), sep=';', index_col=[0]))

metro_sa = dataset_seqdeparture(pd.read_csv(os.path.join(clust_metro, 'cluster-mtsa-im24.csv'), sep=';', index_col=[0]))
big_sa = dataset_seqdeparture(pd.read_csv(os.path.join(clust_big, 'cluster-bsa-im24.csv'), sep=';', index_col=[0]))
middle_sa = dataset_seqdeparture(pd.read_csv(os.path.join(clust_middle, 'cluster-mdsa-im24.csv'), sep=';', index_col=[0]))
small_sa = dataset_seqdeparture(pd.read_csv(os.path.join(clust_small, 'cluster-ssa-im24.csv'), sep=';', index_col=[0]))
rural_sa = dataset_seqdeparture(pd.read_csv(os.path.join(clust_rural, 'cluster-rsa-im24.csv'), sep=';', index_col=[0]))

metro_su = dataset_seqdeparture(pd.read_csv(os.path.join(clust_metro, 'cluster-mtsu-im24.csv'), sep=';', index_col=[0]))
big_su = dataset_seqdeparture(pd.read_csv(os.path.join(clust_big, 'cluster-bsu-im24.csv'), sep=';', index_col=[0]))
middle_su = dataset_seqdeparture(pd.read_csv(os.path.join(clust_middle, 'cluster-mdsu-im24.csv'), sep=';', index_col=[0]))
small_su = dataset_seqdeparture(pd.read_csv(os.path.join(clust_small, 'cluster-ssu-im24.csv'), sep=';', index_col=[0]))
rural_su = dataset_seqdeparture(pd.read_csv(os.path.join(clust_rural, 'cluster-rsu-im24.csv'), sep=';', index_col=[0]))

#%% Keep only three variables (HP_ID, Cluster and ntrips) and concatenate databases for all regions
idclust_wd = pd.concat([metro_wd.loc[:, ('HP_ID', 'Cluster', 'ntrips')], 
                        big_wd.loc[:, ('HP_ID', 'Cluster', 'ntrips')], 
                        middle_wd.loc[:, ('HP_ID', 'Cluster', 'ntrips')], 
                        small_wd.loc[:, ('HP_ID', 'Cluster', 'ntrips')], 
                        rural_wd.loc[:, ('HP_ID', 'Cluster', 'ntrips')], 
                        metro_sa.loc[:, ('HP_ID', 'Cluster', 'ntrips')], 
                        big_sa.loc[:, ('HP_ID', 'Cluster', 'ntrips')], 
                        middle_sa.loc[:, ('HP_ID', 'Cluster', 'ntrips')], 
                        small_sa.loc[:, ('HP_ID', 'Cluster', 'ntrips')], 
                        rural_sa.loc[:, ('HP_ID', 'Cluster', 'ntrips')],
                        metro_su.loc[:, ('HP_ID', 'Cluster', 'ntrips')], 
                        big_su.loc[:, ('HP_ID', 'Cluster', 'ntrips')], 
                        middle_su.loc[:, ('HP_ID', 'Cluster', 'ntrips')], 
                        small_su.loc[:, ('HP_ID', 'Cluster', 'ntrips')], 
                        rural_su.loc[:, ('HP_ID', 'Cluster', 'ntrips')]],
                        ignore_index=True).reset_index(drop=True)

#%% Load database (trip database and not sequence database)
trips = pd.read_csv(os.path.join(results, 'midtrips_clean.csv'), sep=';', low_memory=False, decimal=',', index_col=[0])
# %% Select relevant variables and indexes (only indexes that are used in the sequence databases)
trips=trips[['HP_ID', 'W_ID', 'wegkm_imp', 'wegkm_imp_gr', 'wegmin_imp1', 'wegmin_imp1_gr', 'tempo', 'RegioStaRGem5', 'ST_WOTAG', 'zweck_mop']] # Select relevant variable
ind = idclust_wd['HP_ID'].tolist() # Select indexes that are in the sequence dataset 
trips = trips.loc[trips['HP_ID'].isin(ind)]
#%% Add Cluster and ntrips variable 
trips = trips.join(idclust_wd.set_index('HP_ID'), on='HP_ID')

#%% Discretize distance and duration variables 
distance = ['0.5', '1', '2', '5', '10', '20', '50', '100', '200']
duration = ['5', '10', '15', '20', '30', '45', '60', '120']
trips['Distance'] = trips['wegkm_imp_gr'].replace(to_replace=[1,2,3,4,5,6,7,8,9], value=distance)
trips['Duration'] = trips['wegmin_imp1_gr'].replace(to_replace=[1,2,3,4,5,6,7,8], value=duration)

#%% Function: joint *conditional* probability distribution of trip duration and trip distance 
# for all days/destinations/ntrips given a location on the whole dataset
def joint_cond_distribution_distance_duration_cs(data: pd.DataFrame, location : str, frame: str, folder: Path) -> pd.DataFrame: 
    """ Computes a dataframe of the joint probability of trip distance and trip duration conditional on trip destination
    and exports the result to a specified csv file.

    Parameters
    ----------

    data : pd.DataFrame
        Should contain a variable for the location and a column for the day of the week. 
    location : str
        Can be chosen among the keys of the locations dictionary.
        Used as a suffix in the name of the exported csv file.
    folder : Path 
        Defines where the csv file is exported.
        Used as a suffix in the name of the exported csv file.
    """
    days = {'weekday' : [1,2,3,4,5], 
        'saturday' : [6],
        'sunday' : [7]}
    locations = {'mt': [51], 
             'b': [52], 
             'md': [53],
             's': [54],
             'r': [55]}
    destinations = {'home': [5],
                    'work': [1,2], 
                    'leisure': [4],
                    'errands': [3]} 
    jointconddist = pd.DataFrame()
    for d in destinations.keys():
        for day_long in days.keys():
            filter_trips = data[(data['RegioStaRGem5'].isin(locations[location])) & (data['ST_WOTAG'].isin(days[day_long])) & (data['zweck_mop'].isin(destinations[d]))]
            if len(filter_trips.index) != 0:
                distj = pd.crosstab(filter_trips['Distance'], filter_trips['Duration'], margins=False, normalize='all')
                distj = pd.DataFrame(distj,
                            index= ['0.5', '1', '2', '5', '10', '20', '50', '100', '200'],
                            columns=['5', '10', '15', '20', '30', '45', '60', '120', 'days', 'destination', 'Cluster'])
                distj['days']=day_long
                distj['destination']=d
                distj['Cluster']=99   #We tag cluster as 99 to remember probabilities are at the dataset level and ease the merge with cluster datasets for the week.
                # set unlikely speed values to 0
                distj.iloc[3:9,0] = 0
                distj.iloc[5:9,1] = 0
                distj.iloc[6:9,2] = 0
                distj.iloc[0,3] = 0
                distj.iloc[6:9,3] = 0
                distj.iloc[0:2,4] = 0
                distj.iloc[7:9,4] = 0
                distj.iloc[0:3,5] = 0
                distj.iloc[7:9,5] = 0
                distj.iloc[0:4,6] = 0
                distj.iloc[8,6] = 0
                distj.iloc[0:7,7] = 0
                #Rescale to 1
                s = distj.iloc[:, 0:8].sum().sum()
                distj.iloc[:, 0:8] = distj.iloc[:, 0:8] / s
                jointconddist = pd.concat([jointconddist, distj])
                jointconddist.index.name = "km"
                jointconddist = jointconddist.fillna(0).round(3)
            else:
                distj = pd.DataFrame(distj,
                            index=['0.5', '1', '2', '5', '10', '20', '50', '100', '200'],
                            columns=['5', '10', '15', '20', '30', '45', '60', '120', 'days', 'destination', 'Cluster'])
                distj = distj.fillna(NaN)
                distj['days']=day_long
                distj['destination']=d
                distj['Cluster']=99 #We tag cluster as 99 to remember probabilities are at the dataset level and ease the merge with cluster datasets for the week.
                jointconddist = pd.concat([jointconddist, distj])
                jointconddist.index.name = "km"
                jointconddist = jointconddist.fillna(0).round(3)
    name_csv = "cs_conddistduration-{}-{}-full.csv".format(location, frame)
    jointconddist.to_csv(os.path.join(folder, name_csv), sep=';', index=True)
    return jointconddist

#%% Compute the joint *conditional* distribution csv files for any location
#locations = {'mt': [51], 
#             'b': [52]}

locations = {'mt': [51], 
             'b': [52], 
             'md': [53],
             's': [54],
             'r': [55]}

for l in locations.keys():
    joint_cond_distribution_distance_duration_cs(trips, l, 'im24', conddurdist_cs_fast)
    joint_cond_distribution_distance_duration_cs(trips, l, 'im24', conddurdist_cs_slow)

#%% Function: joint *conditional* probability distribution of trip duration and trip distance at the cluster level
def joint_cond_distribution_distance_duration_cluster_cs(data: pd.DataFrame, location : str, frame: str, folder : Path, cluster_carsharing: list) -> pd.DataFrame:
    """ Computes a dataframe of the joint probability of trip distance and trip duration conditional on the trip destination 
    for all combinations of days and locations and exports the result to a specified csv file.

    Parameters
    ----------

    data : pd.DataFrame
        Should contain a variable for the location and a column for the day of the week. 
    location : str
        Can be chosen among the keys of the locations dictionary.
        Used as a suffix in the name of the exported csv file.
    folder : Path 
        Defines where the csv file is exported.
        Used as a suffix in the name of the exported csv file.
    cluster_carsharing: list 
        List of integers containing the clusters that are assumed to switch to carsharing. 
    """
    days = {'weekday' : [1,2,3,4,5], 
        'saturday' : [6],
        'sunday' : [7]}
    locations = {'mt': [51], 
             'b': [52], 
             'md': [53],
             's': [54],
             'r': [55]}
    destinations = {'home': [5],
                    'work': [1,2], 
                    'leisure': [4],
                    'errands': [3]} 
    jointconddist = pd.DataFrame()
    for day_long in days.keys():
        for d in destinations.keys():
            filter_trips = data[(data['RegioStaRGem5'].isin(locations[location])) & (data['ST_WOTAG'].isin(days[day_long])) & (data['Cluster'].isin(cluster_carsharing)) \
                                & (data['zweck_mop'].isin(destinations[d]))]
            if len(filter_trips.index) != 0:
                distj = pd.crosstab(filter_trips['Distance'], filter_trips['Duration'], margins=False, normalize='all')
                distj = pd.DataFrame(distj,
                            index=['0.5', '1', '2', '5', '10', '20', '50', '100', '200'],
                            columns=['5', '10', '15', '20', '30', '45', '60', '120', 'days', 'destination', 'Cluster'])
                distj['days']=day_long
                distj['destination']=d
                distj['Cluster']= distj.apply(lambda _: cluster_carsharing, axis=1)
                # set unlikely speed values to 0
                distj.iloc[3:9,0] = 0
                distj.iloc[5:9,1] = 0
                distj.iloc[6:9,2] = 0
                distj.iloc[0,3] = 0
                distj.iloc[6:9,3] = 0
                distj.iloc[0:2,4] = 0
                distj.iloc[7:9,4] = 0
                distj.iloc[0:3,5] = 0
                distj.iloc[7:9,5] = 0
                distj.iloc[0:4,6] = 0
                distj.iloc[8,6] = 0
                distj.iloc[0:7,7] = 0
                #Rescale to 1
                s = distj.iloc[:, 0:8].sum().sum()
                distj.iloc[:, 0:8] = distj.iloc[:, 0:8] / s
                jointconddist = pd.concat([jointconddist, distj])
                jointconddist.index.name = "km"
                jointconddist = jointconddist.fillna(0).round(3)
            else:
                distj = pd.DataFrame(distj,
                            index=['0.5', '1', '2', '5', '10', '20', '50', '100', '200'],
                            columns=['5', '10', '15', '20', '30', '45', '60', '120', 'days', 'destination', 'Cluster'])
                distj = distj.fillna(NaN)
                distj['days']=day_long
                distj['destination']=d
                distj['Cluster']= distj.apply(lambda _: cluster_carsharing, axis=1)
                jointconddist = pd.concat([jointconddist, distj])
                jointconddist.index.name = "km"
                jointconddist = jointconddist.fillna(0).round(3)
    name_csv = "cs_conddistduration-{}-{}-clust{}{}.csv".format(location, frame, cluster_carsharing[0], cluster_carsharing[-1])
    jointconddist.to_csv(os.path.join(folder, name_csv), sep=';', index=True)
    return jointconddist
#%% Compute the joint *conditional* distribution csv files at the cluster level for all (day X location X destination) combinations
#locations = {'mt': [51], 
#             'b': [52]}

locations = {'mt': [51], 
             'b': [52], 
             'md': [53],
             's': [54],
             'r': [55]}

for l in locations.keys():
    joint_cond_distribution_distance_duration_cluster_cs(trips, l, 'im24', conddurdist_cs_slow, [5])

#%% Compute the joint conditional distribution csv files for a group of clusters for a given location and type of day
joint_cond_distribution_distance_duration_cluster_cs(trips, 'mt', 'im24', conddurdist_cs_fast, [2,3,4,5])
joint_cond_distribution_distance_duration_cluster_cs(trips, 'b', 'im24', conddurdist_cs_fast,  [3,4,5])
joint_cond_distribution_distance_duration_cluster_cs(trips, 'md', 'im24', conddurdist_cs_fast, [4,5])

#%% Function: create dataframe for the whole week at the cluster level (weekday) and dataset level (saturday, sunday)

def conddistduration_week_cs(location: str, frame: str, cluster_carsharing: list, folder_in: Path, folder_out: Path) -> pd.DataFrame:
    weekend=['saturday', 'sunday']
    clust = "cs_conddistduration-{}-{}-clust{}{}.csv".format(location, frame, cluster_carsharing[0], cluster_carsharing[-1])
    full = "cs_conddistduration-{}-{}-full.csv".format(location, frame)
    data_clust = pd.read_csv(os.path.join(folder_in, clust), sep=';', index_col=[0])
    data_full = pd.read_csv(os.path.join(folder_in, full), sep=';', index_col=[0])
    data_clust = data_clust[(data_clust['days']=='weekday')]
    data_full = data_full[(data_full['days'].isin(weekend))]
    data_week = pd.concat([data_clust, data_full])
    data_week = data_week.drop(columns='Cluster')
    data_week = data_week.reset_index()
    cols = data_week.columns.tolist()
    new_cols = cols[-2:] + cols[0:-2]
    data_week = data_week[new_cols]
    name_csv = "cs-conddistduration-week-{}-{}-{}{}.csv".format(location, frame, cluster_carsharing[0], cluster_carsharing[-1]) #Needs to be adapted to the number of clusters switching to cs
    data_week.to_csv(os.path.join(folder_out, name_csv), index=False, sep=',')
    return data_week

#%% Compute the joint *conditional* distribution csv files for the whole week with cluster probabilities for weekdays and dataset probabilities for weekends (saturday, sunday)
#locations = {'mt': [51], 
#             'b': [52]}
             
locations = {'mt': [51], 
             'b': [52], 
             'md': [53],
             's': [54],
             'r': [55]}

for l in locations.keys():
    conddistduration_week_cs(l, 'im24', [5], conddurdist_cs_slow, conddurdist_cs_week_slow)
# %% Compute the joint *conditional* distribution csv files for the whole week with probabilities for a group of clusters for weekdays and dataset probabilities for weekends (saturday, sunday)
conddistduration_week_cs('mt', 'im24', [2,3,4,5], conddurdist_cs_fast, conddurdist_cs_week_fast)
conddistduration_week_cs('b',  'im24', [3,4,5], conddurdist_cs_fast, conddurdist_cs_week_fast)
conddistduration_week_cs('md', 'im24', [4,5], conddurdist_cs_fast, conddurdist_cs_week_fast)


