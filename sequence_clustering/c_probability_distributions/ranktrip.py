################################################
####### Distribution of trip destination  ######
###### conditional on number of trips made #####
############# and rank of the trip #############
################################################

#%% Load packages and define paths 
import os
import pandas as pd
import numpy as np
from pathlib import Path 
from collections import defaultdict
from numpy import NaN

# Define paths
results = Path('/temp/SeqResults') #change this path when working on another operating system - the structure of the results folder should nevertheless be the same
clusters = results / 'clusters'
clust_metro = clusters / 'metro'
clust_big = clusters / 'big'
clust_middle = clusters / 'middle'
clust_small = clusters / 'small'
clust_rural = clusters / 'rural'
conddistdest = results / 'distributions' / 'condtripdest'

#%% Define a function: generate a subdataset of trip sequences conditional on overall number of daily trips and trip rank
def dataset_ntrips_triprank(data: pd.DataFrame, total_ntrips: int, trip_rank: int) -> pd.DataFrame:
    """ Generate the subdataset of trip sequences for a given number of total daily trips and a given trip rank (first trip, second trip...)

    Parameters
    ----------

    data: pd.DataFrame
        Cluster dataframe.
        Should contain trip information in a sequence format.
    total_ntrips: integer
        Number of total daily trips undertaken.
    trip_rank: integer
        Position of the trip.
        Must be smaller than or equal to total_ntrips.
    """
    # Find column where sequences start and end and make a copy of the dataset with only 0-1 for the trips (without trip purpose)
    g = data.columns.get_loc('00:00')
    h = data.columns.get_loc('23:55')
    data_binary = data.copy(deep=True)
    data_binary.iloc[:, g:h+1] = pd.DataFrame.replace(data_binary.iloc[:, g:h+1], to_replace = [2,3,4,5], value = 1) 
    # For both data inputs (full trip data and binary data), compute a clone where each column is 
    # the result of iterative substraction of columns from the original dataset.
    # This allows to get the start and end position of each trip sequence.
    clone = pd.DataFrame(columns=data.columns)
    for i in range(g,h):
        clone.iloc[:,i] = data.iloc[:,i+1] - data.iloc[:,i]
    clone_binary = pd.DataFrame(columns=data_binary.columns)
    for i in range(g,h):
        clone_binary.iloc[:,i] = data_binary.iloc[:,i+1] - data_binary.iloc[:,i]
    # Convert dataframes to arrays to be able to use numpy functions.
    array = clone.iloc[:, g:h].values
    array_binary = clone_binary.iloc[:,g:h].values
    # With numpy np.nonzero, retrieve the indexes where trip sequences start and end,
    # convert to DataFrame and for each sequence, gather indexes in a list.
    index_full = pd.DataFrame(np.transpose(np.nonzero(array)), columns=['seq_ID', 'Time'])
    index_full = index_full.groupby('seq_ID').agg({'Time': lambda x: x.tolist()})
    # Do the same with the binary trip sequence.
    index_binary = pd.DataFrame(np.transpose(np.nonzero(array_binary)), columns=['seq_ID', 'Time'])
    index_binary = index_binary.groupby('seq_ID').agg({'Time': lambda x: x.tolist()})
    # Join both dataframes (full and binary) on the sequence index.
    index_time = index_full.join(index_binary, on='seq_ID', lsuffix='_full', rsuffix='_binary')
    # Convert lists to sets in order to apply the symmetric_difference function
    index_time['Time_full'] = index_time.apply(lambda row: set(row['Time_full']), axis=1)
    index_time['Time_binary'] = index_time.apply(lambda row: set(row['Time_binary']), axis=1)
    # For each trip sequence, compute a column containing indexes that are not in both lists of indexes.
    # This allows to detect sequences where trips with two distinct destinations were undertaken one after the other
    # without any break between the two (at least breaks smaller than 5 minutes).
    index_time['Time_diff'] = index_time.apply(lambda row: set(row['Time_full']).symmetric_difference(row['Time_binary']), axis=1)
    # Input in the list of indexes from the full datframe the difference between the two lists.
    # This allows to take one index as both the end and start position of two different trips undertaken one after the other.
    # Convert sets to list again and sort the list by ascending order.
    index_time['Time_solve'] = index_time.apply(lambda row: sorted(list(row['Time_full']) + list(row['Time_diff'])), axis=1)
    # Explode the list of indexes with one row for each index 
    # Clean the dataframe -- remove unnecessary columns and rename variables.
    trips_seq = index_time.copy(deep=True)
    trips_seq = trips_seq.drop(columns=['Time_full', 'Time_binary', 'Time_diff']).explode('Time_solve').rename(columns={'Time_solve': 'Time'})
    # Check total number of trip indexes for each sequence:
    # In particular, check whether this number is odd or even.
    # If odd (euclidean remainder is 1), remove the sequence.
    # Clean the dataframe.
    trips_seq['nslots'] = trips_seq.groupby('seq_ID')['Time'].size()
    trips_seq['rem'] = trips_seq['nslots'] % 2
    trips_seq = trips_seq[trips_seq['rem']==0]
    trips_seq = trips_seq.drop(columns=['nslots', 'rem'])
    # Split the indexes into two groups based on their position.
    # Every second index starting from 0 is a departure index.
    # Every second index starting from 1 in an arrival index.
    slot_departure = trips_seq[::2].rename_axis('seq_ID').reset_index()
    slot_arrival = trips_seq[1::2].rename_axis('seq_ID').reset_index()
    # Join departure and arrival indexes in a commun dataframe 
    # Joining is made on the index of both dataframes
    slot_trips = slot_departure.join(slot_arrival, lsuffix='_dep', rsuffix='_arr').drop(columns=['seq_ID_arr']).rename(columns={'seq_ID_dep': 'seq_ID', 'Time_dep': 'step_dep', 'Time_arr': 'step_arr'})
    # Generate necessary information on trip length, trip rank, total number of trips.
    slot_trips['trip_length'] = slot_trips.iloc[:,2] - slot_trips.iloc[:,1]
    slot_trips['step_dep'] = slot_trips['step_dep'] + (g+1) # readjust the index taking into account other variables in the original dataframe and the fact that substracting columns gives the departure index -1
    slot_trips['step_arr'] = slot_trips['step_arr'] + g # readjust the index to match with sequence position in the original dataframe
    slot_trips['trip_rank'] = slot_trips.groupby('seq_ID').cumcount()
    slot_trips['trip_rank'] = slot_trips['trip_rank'] + 1 # start counting at 1 and not 0
    slot_trips['ntrips'] = slot_trips.groupby('seq_ID')['trip_rank'].transform('max')
    # Retrieve HP_ID and cluster and destination data from the original dataset. 
    slot_trips = slot_trips.join(data.loc[:, ('HP_ID', 'Cluster')], on='seq_ID', how='inner')
    slot_trips['destination'] = pd.Series(dtype='int')
    for i in range(0, len(slot_trips)):
        slot_trips.iloc[i,-1] = data.iloc[slot_trips['seq_ID'].iloc[i], slot_trips['step_dep'].iloc[i]]
    # Generate subdataset filtered out for the overall number of trips and the trip rank.
    if total_ntrips <= 4:
        subdataset_ntrips_triprank = slot_trips[(slot_trips['ntrips']==total_ntrips) & (slot_trips['trip_rank']==trip_rank)].reset_index()
        subdataset_ntrips_triprank['step_dep'] = subdataset_ntrips_triprank['step_dep'] - g # in the new subdataset there are no columns before the sequence starts
        subdataset_ntrips_triprank['step_arr'] = subdataset_ntrips_triprank['step_arr'] - g # in the new subdataset there are no columns before the sequence starts
    else:
        if trip_rank <= 4:
            subdataset_ntrips_triprank = slot_trips[(slot_trips['ntrips']>4) & (slot_trips['trip_rank']==trip_rank)].reset_index()
            subdataset_ntrips_triprank['step_dep'] = subdataset_ntrips_triprank['step_dep'] - g # in the new subdataset there are no columns before the sequence starts
            subdataset_ntrips_triprank['step_arr'] = subdataset_ntrips_triprank['step_arr'] - g # in the new subdataset there are no columns before the sequence starts
        else:
            subdataset_ntrips_triprank = slot_trips[(slot_trips['ntrips']>4) & (slot_trips['trip_rank']>4)].reset_index()
            subdataset_ntrips_triprank['step_dep'] = subdataset_ntrips_triprank['step_dep'] - g # in the new subdataset there are no columns before the sequence starts
            subdataset_ntrips_triprank['step_arr'] = subdataset_ntrips_triprank['step_arr'] - g # in the new subdataset there are no columns before the sequence starts
    # Create new columns with time of the day by 5 minutes-step
    NaN=np.nan
    n=12
    m=23
    hours=list(range(24)) #list of hours from 0 to 23
    hours=[item for item in hours for i in range(n)] #duplicate each hour 12 times in a row
    hours_str=[str(i).zfill(2) for i in hours] #convert to string with leading zeros
    minutes=[] #initiliaze empty list
    for x in range(0,60,5): #create a list of minutes from 0 to 55 by steps of 5
        minutes.append(x)
    temp_minutes=list(minutes)
    for i in range(m): #expand the minutes list 23 times in a row
        for min in temp_minutes:
            minutes.append(min)
    minutes_str=[str(i).zfill(2) for i in minutes] #convert to string with leading zeros
    h_m=list(map(":".join,zip(hours_str,minutes_str))) #concatenating the minutes and hours lists
    # Generate trips dataset for a given overall number of trips and a given trip rank
    additional_columns = ['HP_ID', 'Cluster', 'ntrips']
    c = subdataset_ntrips_triprank.columns.get_loc('Cluster')
    hp = subdataset_ntrips_triprank.columns.get_loc('HP_ID')
    nt = subdataset_ntrips_triprank.columns.get_loc('ntrips')
    dest = subdataset_ntrips_triprank.columns.get_loc('destination')
    dep = subdataset_ntrips_triprank.columns.get_loc('step_dep')
    arr = subdataset_ntrips_triprank.columns.get_loc('step_arr')
    data_col = h_m + additional_columns
    data_ntrips_triprank=pd.DataFrame(index=subdataset_ntrips_triprank.index.tolist(), columns=data_col).fillna(0)
    for i in range(0,len(subdataset_ntrips_triprank)):
        data_ntrips_triprank.iloc[i, subdataset_ntrips_triprank.iloc[i,dep]] = subdataset_ntrips_triprank.iloc[i,dest] # Only take the departure slot
        #data_ntrips_triprank.iloc[i, subdataset_ntrips_triprank.iloc[i,dep]:subdataset_ntrips_triprank.iloc[i,arr]+1] = subdataset_ntrips_triprank.iloc[i,dest] # Recreate the full trip sequence
        data_ntrips_triprank.iloc[i, 288] = subdataset_ntrips_triprank.iloc[i,hp]
        data_ntrips_triprank.iloc[i, 289] = subdataset_ntrips_triprank.iloc[i,c]
        data_ntrips_triprank.iloc[i, 290] = subdataset_ntrips_triprank.iloc[i,nt]
    return data_ntrips_triprank