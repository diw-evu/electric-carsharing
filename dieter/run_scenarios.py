import os
import shutil
from dieterpy.scripts import runopt
from dieterpy.config import settings

# Settings
settings.PROJECT_DIR_ABS = os.getcwd()
settings.update_changes()

# Run model
#runopt.main('project_variables_main_76_slow.csv')
#runopt.main('project_variables_main_76_fast.csv')
#runopt.main('project_variables_main_76_slow_h2.csv')
#runopt.main('project_variables_main_76_fast_h2.csv')
#runopt.main('project_variables_sensitivity_76_fast_cost.csv')
#runopt.main('project_variables_sensitivity_76_fast_weather1996.csv')
#runopt.main('project_variables_sensitivity_76_fast_weather2000.csv')
#runopt.main('project_variables_sensitivity_76_fast_weather2003.csv')
#runopt.main('project_variables_sensitivity_76_fast_weather2006.csv')
#runopt.main('project_variables_sensitivity_76_fast_weather2009.csv')
#runopt.main('project_variables_sensitivity_76_fast_weather2012.csv')
#runopt.main('project_variables_sensitivity_76_fast_weather2015.csv')
#runopt.main('project_variables_sensitivity_76_fast_weather2016.csv')
#runopt.main('project_variables_sensitivity_76_fast_weather2017.csv')
#runopt.main('project_variables_sensitivity_76_fast_eved.csv')

#runopt.main('project_variables_sensitivity_76_fast_weather1997.csv')
#runopt.main('project_variables_sensitivity_76_fast_weather1998.csv')
#runopt.main('project_variables_sensitivity_76_fast_weather1999.csv')
#runopt.main('project_variables_sensitivity_76_fast_weather2001.csv')
#runopt.main('project_variables_sensitivity_76_fast_weather2002.csv')
#runopt.main('project_variables_sensitivity_76_fast_weather2004.csv')
#runopt.main('project_variables_sensitivity_76_fast_weather2005.csv')
#runopt.main('project_variables_sensitivity_76_fast_weather2007.csv')
#runopt.main('project_variables_sensitivity_76_fast_weather2008.csv')
#runopt.main('project_variables_sensitivity_76_fast_weather2010.csv')
#runopt.main('project_variables_sensitivity_76_fast_weather2011.csv')
#runopt.main('project_variables_sensitivity_76_fast_weather2013.csv')
#runopt.main('project_variables_sensitivity_76_fast_weather2014.csv')


#runopt.main('project_variables_main_76_slow_pecd.csv')
#runopt.main('project_variables_main_76_fast_pecd.csv')

# Delete temp folder at the end
if os.path.exists(settings.TMP_DIR_ABS):
    shutil.rmtree(settings.TMP_DIR_ABS)
    print("======= TMP deleted =======")