

%infeasibility%G_INFES.fx(n,h) = 0 ;
%infeasibility%H_INFES.fx(n,bu,ch,h) = 0 ;
%infeasibility%H_DHW_INFES.fx(n,bu,ch,h) = 0 ;

Parameter
m_exog_p(n,tech)
m_exog_sto_e(n,sto)
m_exog_sto_p_in(n,sto)
m_exog_sto_p_out(n,sto)
m_exog_ntc(l)
m_exog_h2_ely(n,h2_ely)
m_exog_h2_sto(n,h2_sto)
m_exog_h2_recon(n,h2_recon)
;

m_exog_p(n,tech)            = technology_data(n,tech,'fixed_capacities') ;
m_exog_sto_e(n,sto)         = storage_data('fixed_capacities_energy',n,sto) ;
m_exog_sto_p_in(n,sto)      = storage_data('fixed_capacities_power_in',n,sto) ;
m_exog_sto_p_out(n,sto)     = storage_data('fixed_capacities_power_out',n,sto) ;
m_exog_ntc(l)               = topology_data('fixed_capacities_ntc',l) ;
m_exog_h2_ely(n,h2_ely)     = h2_ely_data_upload('fixed_capacities',n,h2_ely) ;
m_exog_h2_sto(n,h2_sto)     = h2_sto_data_upload('fixed_capacities',n,h2_sto) ;
m_exog_h2_recon(n,h2_recon) = h2_recon_data_upload('fixed_capacities',n,h2_recon) ;


*** Dispatch model
%dispatch_only%$ontext
N_TECH.fx(n,tech)           = m_exog_p(n,tech) ;
N_STO_P_IN.fx(n,sto)        = m_exog_sto_p_in(n,sto) ;
N_STO_P_OUT.fx(n,sto)       = m_exog_sto_p_out(n,sto) ;
N_STO_E.fx(n,sto)           = m_exog_sto_e(n,sto) ;
NTC.fx(l)                   = m_exog_ntc(l) ;
H2_N_ELY.fx(n,h2_ely)       = m_exog_h2_ely(n,h2_ely);
H2_N_STO_E.fx(n,h2_sto)     = m_exog_h2_sto(n,h2_sto);
H2_N_RECON.fx(n,h2_recon)   = m_exog_h2_recon(n,h2_recon);
$ontext
$offtext

*** Investment model
%investment%$ontext

* fixed bounds
* N_TECH.fx(n,tech)         = m_exog_p(n,tech) ; 
* N_STO_E.fx(n,sto)         = m_exog_sto_e(n,sto) ;
* N_STO_P_IN.fx(n,sto)      = m_exog_sto_p_in(n,sto) ;
* N_STO_P_OUT.fx(n,sto)     = m_exog_sto_p_out(n,sto) ;
$ontext
$offtext


*** No network transfer
%net_transfer%NTC.fx(l) = 0 ;
%net_transfer%F.fx(l,h) = 0 ;


***** Data iteration **********************************************************

%iter_data_switch%$ontext

Sets
scenario
identifier
;

Parameters
iter_data(h,identifier,scenario)
;

$GDXin "%data_it_gdx%"

$load scenario
$load identifier
$load iter_data
;

$ontext
$offtext

*******************************************************************************

*set used for report
Set feat_included(features);
feat_included(features) = yes$(sum(n, feat_node(features,n))) ;
