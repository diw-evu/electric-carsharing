min_res_demand(n)$(phi_min_res(n)>0)..
    sum(h , G_L(n,'bio',h) + sum( map_n_tech(n,nondis) , G_RES(n,nondis,h)) + sum( map_n_sto(n,sto),  sto_flow_in(n,sto,h)))

    =G= 

    phi_min_res(n) * (sum( h , d(n,h)) + sum((h,ev) , EV_GED(n,ev,h)/eta_ev_in(n,ev))) + sum( (sto,h), STO_IN(n,sto,h) + sto_flow_in(n,sto,h) - STO_OUT(n,sto,h) )

%EV%$ontext
%EV_EXOG%   + sum( (ev,h), EV_CHARGE(n,ev,h) - EV_GED(n,ev,h)/eta_ev_in(n,ev) - EV_DISCHARGE(n,ev,h) - (EV_L(n,ev,h)$(ord(h)=card(h)) - EV_L(n,ev,h)$(ord(h)=1)) )
$ontext
$offtext

%P2H2P_parsimonious%$ontext
            + sum( (map_n_h2_ely(n,h2_ely),map_n_h2_sto(n,h2_sto),map_n_h2_recon(n,h2_recon),h), H2_ELY_IN(n,h2_ely,h) + H2_STO_IN(n,h2_sto,h) * h2_comp_ed_ratio(n,h2_ely) - H2_RECON_OUT(n,h2_recon,h) )
$ontext
$offtext
;


min_res_generation(n)..
    sum(h , G_L(n,'bio',h) 
    + sum( map_n_tech(n,nondis) , G_RES(n,nondis,h))
    )

    =G= 

    phi_min_res(n) * sum( h , sum( map_n_tech(n,dis) , G_L(n,dis,h)) + sum( map_n_tech(n,nondis) , G_RES(n,nondis,h)))
;