from emobpy import Availability, DataBase

# Initialize seed
from emobpy.tools import set_seed

set_seed()
if __name__ == '__main__':
    databases_cluster1=['db_mt_po_1', 
                        'db_b_po_1',  
                        'db_md_po_1', 
                        'db_s_po_1', 
                        'db_r_po_1']
    databases_cluster234=['db_b_po_2', 'db_b_po_3', 'db_b_po_4', 
                          'db_md_po_2', 'db_md_po_3', 'db_md_po_4',
                          'db_s_po_2', 'db_s_po_3', 'db_s_po_4',
                          'db_r_po_2', 'db_r_po_3', 'db_r_po_4']  #'db_mt_po_2', 'db_mt_po_3', 'db_mt_po_4', 
    for db_one in databases_cluster1:
        DB = DataBase(db_one)                                  # Instance of profiles' database whose input is the pickle files' folder
        DB.loadfiles_batch(kind="consumption")                # loading consumption pickle files to the database
        cnames = list(DB.db.keys())                       # getting the id of the first consumption profile

        for i in cnames:
            station_distribution = {                                   # Dictionary with charging stations type probability distribution per the purpose of the trip (location or destination)
                'prob_charging_point': {
                    'home': {'public': 0.3, 'home': 0.6, 'none': 0.1},
                    'work': {'public': 0.5, 'fast75': 0.1, 'workplace': 0.3, 'none': 0.1},
                    'errands': {'public': 0.5, 'fast75': 0.1, 'workplace': 0.2, 'none': 0.2},
                    'leisure': {'public': 0.5, 'fast75': 0.1, 'workplace': 0.2, 'none': 0.2},
                    'driving': {'none': 0.99, 'fast75': 0.005, 'fast150': 0.005}},
                'capacity_charging_point': {                                       # Nominal power rating of charging station in kW
                    'public': 22,
                    'home': 3.7,
                    'workplace': 11,
                    'none': 0,  # dummy station
                    'fast75': 75,
                    'fast150': 150}
            }

            ga = Availability(i, DB)
            ga.set_scenario(station_distribution)
            ga.battery_capacity = 100
            ga.run()
            ga.save_profile(db_one)
    for db in databases_cluster234:
        DB = DataBase(db)                                  # Instance of profiles' database whose input is the pickle files' folder
        DB.loadfiles_batch(kind="consumption")                # loading consumption pickle files to the database
        cnames = list(DB.db.keys())                       # getting the id of the first consumption profile

        for i in cnames:
            station_distribution = {                                   # Dictionary with charging stations type probability distribution per the purpose of the trip (location or destination)
                'prob_charging_point': {
                    'home': {'public': 0.3, 'home': 0.6, 'none': 0.1},
                    'work': {'public': 0.5, 'fast75': 0.1, 'workplace': 0.3, 'none': 0.1},
                    'errands': {'public': 0.5, 'fast75': 0.1, 'workplace': 0.2, 'none': 0.2},
                    'leisure': {'public': 0.5, 'fast75': 0.1, 'workplace': 0.2, 'none': 0.2},
                    'driving': {'none': 0.99, 'fast75': 0.005, 'fast150': 0.005}},
                'capacity_charging_point': {                                       # Nominal power rating of charging station in kW
                    'public': 22,
                    'home': 3.7,
                    'workplace': 11,
                    'none': 0,  # dummy station
                    'fast75': 75,
                    'fast150': 150}
            }

            ga = Availability(i, DB)
            ga.set_scenario(station_distribution)
            ga.run()
            ga.save_profile(db)

