###################################
####### Mobility time series ######
###################################

#%% Load emobpy and initialize seed
from emobpy import Mobility
from emobpy.tools import set_seed
set_seed()

#%% Redefine typology of day and destination types
WEEKS = {
    0:{'day': 'Sunday',    'week': 'sunday',   'day_code': 'sunday'},
    1:{'day': 'Monday',    'week': 'weekday',  'day_code': 'weekday'},
    2:{'day': 'Tuesday',   'week': 'weekday',  'day_code': 'weekday'},
    3:{'day': 'Wednesday', 'week': 'weekday',  'day_code': 'weekday'},
    4:{'day': 'Thursday',  'week': 'weekday',  'day_code': 'weekday'},
    5:{'day': 'Friday',    'week': 'weekday',  'day_code': 'weekday'},
    6:{'day': 'Saturday',  'week': 'saturday', 'day_code': 'saturday'}
}

RULE = {
    'weekday':
            {'n_trip_out': [],
             'max_same_trip_times': False,
             'last_trip_to':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'first_trip_to':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'not_last_trip_to':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'at_least_one_trip':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'overall_min_time_at':{'home': False,'work': False, 'errands': False,'leisure': False, 'driving':False},
             'overall_max_time_at':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'min_state_duration':{'home':False,'work':False, 'errands':False,'leisure':False,'driving':False},
             'max_state_duration':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'equal_state_and_destination':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False}
            },
    'saturday':
            {'n_trip_out': [],
             'max_same_trip_times': False,
             'last_trip_to':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'first_trip_to':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'not_last_trip_to':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'at_least_one_trip':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'overall_min_time_at':{'home': False,'work': False, 'errands': False,'leisure': False, 'driving':False},
             'overall_max_time_at':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'min_state_duration':{'home':False,'work':False, 'errands':False,'leisure':False,'driving':False},
             'max_state_duration':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'equal_state_and_destination':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False}
            },
    'sunday':
            {'n_trip_out': [],
             'max_same_trip_times': False,
             'last_trip_to':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'first_trip_to':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'not_last_trip_to':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'at_least_one_trip':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'overall_min_time_at':{'home': False,'work': False, 'errands': False,'leisure': False, 'driving':False},
             'overall_max_time_at':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'min_state_duration':{'home':False,'work':False,'errands':False,'leisure':False,'driving':False},
             'max_state_duration':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'equal_state_and_destination':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False}
            },
       }

if __name__ == '__main__':
        n_profiles_clust1 = 20 #20
        n_profiles_clust2 = 30 #30 
        n_profiles_clust3 = 60 #60
        n_profiles_clust4 = 60 #60 
        # Metropolises, cluster 1
        for i in range(n_profiles_clust1):
                try:
                        m = Mobility(config_folder='config_files')
                        # Customization starts
                        m.WEEKS = WEEKS
                        m.rules = RULE
                        # Customization ends
                        m.set_params(name_prefix="M1PO",
                                        time_format='seconds',
                                        total_time=3600*24*365, # one week (in seconds)
                                        time_step=300, # 5 minutes need to be put in seconds
                                        category="user_defined",
                                        reference_date="01/01/2020")
                        m.set_stats(stat_ntrip_path="distntrips-week-mt-im24-2.csv",
                                        stat_dest_path="condtripdest-ntrk-week-mt-im24-2.csv",
                                        stat_km_duration_path="conddistduration-week-mt-im24-2.csv",
                                        time_format='hours')
                        m.set_rules("user_defined")
                        m.run()
                        m.save_profile("db_mt_po_1", description="Metropolises-cluster1-private")
                except ValueError:
                        continue
        # Metropolises, cluster 2
        for i in range(n_profiles_clust2):
                try:
                        m = Mobility(config_folder='config_files')
                        # Customization starts
                        m.WEEKS = WEEKS
                        m.rules = RULE
                        # Customization ends
                        m.set_params(name_prefix="M2PO",
                                     time_format='seconds',
                                     total_time=3600*24*365, # one week (in seconds)
                                     time_step=300, # 5 minutes need to be put in seconds
                                     category="user_defined",
                                     reference_date="01/01/2020")
                        m.set_stats(stat_ntrip_path="distntrips-week-mt-im24-3.csv",
                                        stat_dest_path="condtripdest-ntrk-week-mt-im24-3.csv",
                                        stat_km_duration_path="conddistduration-week-mt-im24-3.csv",
                                        time_format='hours')
                        m.set_rules("user_defined")
                        m.run()
                        m.save_profile("db_mt_po_2", description="Metropolises-cluster2-private")
                except ValueError:
                        continue
        # Metropolises, cluster 3
        for i in range(n_profiles_clust3):
                try:
                        m = Mobility(config_folder='config_files')
                        # Customization starts
                        m.WEEKS = WEEKS
                        m.rules = RULE
                        # Customization ends
                        m.set_params(name_prefix="M3PO",
                                        time_format='seconds',
                                        total_time=3600*24*365, # one week (in seconds)
                                        time_step=300, # 5 minutes need to be put in seconds
                                        category="user_defined",
                                        reference_date="01/01/2020")
                        m.set_stats(stat_ntrip_path="distntrips-week-mt-im24-4.csv",
                                        stat_dest_path="condtripdest-ntrk-week-mt-im24-4.csv",
                                        stat_km_duration_path="conddistduration-week-mt-im24-4.csv",
                                        time_format='hours')
                        m.set_rules("user_defined")
                        m.run()
                        m.save_profile("db_mt_po_3", description="Metropolises-cluster3-private")
                except ValueError:
                        continue
        # Metropolises, cluster 4
        for i in range(n_profiles_clust4):
                try:
                        m = Mobility(config_folder='config_files')
                        # Customization starts
                        m.WEEKS = WEEKS
                        m.rules = RULE
                        # Customization ends
                        m.set_params(name_prefix="M4PO",
                                        time_format='seconds',
                                        total_time=3600*24*365, # one week (in seconds)
                                        time_step=300, # 5 minutes need to be put in seconds
                                        category="user_defined",
                                        reference_date="01/01/2020")
                        m.set_stats(stat_ntrip_path="distntrips-week-mt-im24-5.csv",
                                        stat_dest_path="condtripdest-ntrk-week-mt-im24-5.csv",
                                        stat_km_duration_path="conddistduration-week-mt-im24-5.csv",
                                        time_format='hours')
                        m.set_rules("user_defined")
                        m.run()
                        m.save_profile("db_mt_po_4", description="Metropolises-cluster4-private")
                except ValueError:
                        continue
        # Big cities, cluster 1
        for i in range(n_profiles_clust1):
                try:
                        m = Mobility(config_folder='config_files')
                        # Customization starts
                        m.WEEKS = WEEKS
                        m.rules = RULE
                        # Customization ends
                        m.set_params(name_prefix="B1PO",
                                        time_format='seconds',
                                        total_time=3600*24*365, # one week (in seconds)
                                        time_step=300, # 5 minutes need to be put in seconds
                                        category="user_defined",
                                        reference_date="01/01/2020")
                        m.set_stats(stat_ntrip_path="distntrips-week-b-im24-2.csv",
                                        stat_dest_path="condtripdest-ntrk-week-b-im24-2.csv",
                                        stat_km_duration_path="conddistduration-week-b-im24-2.csv",
                                        time_format='hours')
                        m.set_rules("user_defined")
                        m.run()
                        m.save_profile("db_b_po_1", description="Big-cluster1-private")
                except ValueError:
                        continue
        # Big cities, cluster 2
        for i in range(n_profiles_clust2):
                try:
                        m = Mobility(config_folder='config_files')
                        # Customization starts
                        m.WEEKS = WEEKS
                        m.rules = RULE
                        # Customization ends
                        m.set_params(name_prefix="B2PO",
                                        time_format='seconds',
                                        total_time=3600*24*365, # one week (in seconds)
                                        time_step=300, # 5 minutes need to be put in seconds
                                        category="user_defined",
                                        reference_date="01/01/2020")
                        m.set_stats(stat_ntrip_path="distntrips-week-b-im24-3.csv",
                                        stat_dest_path="condtripdest-ntrk-week-b-im24-3.csv",
                                        stat_km_duration_path="conddistduration-week-b-im24-3.csv",
                                        time_format='hours')
                        m.set_rules("user_defined")
                        m.run()
                        m.save_profile("db_b_po_2", description="Big-cluster2-private")
                except ValueError:
                        continue
        # Big cities, cluster 3
        for i in range(n_profiles_clust3):
                try:
                        m = Mobility(config_folder='config_files')
                        # Customization starts
                        m.WEEKS = WEEKS
                        m.rules = RULE
                        # Customization ends
                        m.set_params(name_prefix="B3PO",
                                        time_format='seconds',
                                        total_time=3600*24*365, # one week (in seconds)
                                        time_step=300, # 5 minutes need to be put in seconds
                                        category="user_defined",
                                        reference_date="01/01/2020")
                        m.set_stats(stat_ntrip_path="distntrips-week-b-im24-4.csv",
                                        stat_dest_path="condtripdest-ntrk-week-b-im24-4.csv",
                                        stat_km_duration_path="conddistduration-week-b-im24-4.csv",
                                        time_format='hours')
                        m.set_rules("user_defined")
                        m.run()
                        m.save_profile("db_b_po_3", description="Big-cluster3-private")
                except ValueError:
                        continue
        # Big cities, cluster 4
        for i in range(n_profiles_clust4):
                try:
                        m = Mobility(config_folder='config_files')
                        # Customization starts
                        m.WEEKS = WEEKS
                        m.rules = RULE
                        # Customization ends
                        m.set_params(name_prefix="B4PO",
                                        time_format='seconds',
                                        total_time=3600*24*365, # one week (in seconds)
                                        time_step=300, # 5 minutes need to be put in seconds
                                        category="user_defined",
                                        reference_date="01/01/2020")
                        m.set_stats(stat_ntrip_path="distntrips-week-b-im24-5.csv",
                                        stat_dest_path="condtripdest-ntrk-week-b-im24-5.csv",
                                        stat_km_duration_path="conddistduration-week-b-im24-5.csv",
                                        time_format='hours')
                        m.set_rules("user_defined")
                        m.run()
                        m.save_profile("db_b_po_4", description="Big-cluster4-private")
                except ValueError:
                        continue
        # Middle-size cities, cluster 1
        for i in range(n_profiles_clust1):
                try:
                        m = Mobility(config_folder='config_files')
                        # Customization starts
                        m.WEEKS = WEEKS
                        m.rules = RULE
                        # Customization ends
                        m.set_params(name_prefix="MD1PO",
                                        time_format='seconds',
                                        total_time=3600*24*365, # one week (in seconds)
                                        time_step=300, # 5 minutes need to be put in seconds
                                        category="user_defined",
                                        reference_date="01/01/2020")
                        m.set_stats(stat_ntrip_path="distntrips-week-md-im24-2.csv",
                                        stat_dest_path="condtripdest-ntrk-week-md-im24-2.csv",
                                        stat_km_duration_path="conddistduration-week-md-im24-2.csv",
                                        time_format='hours')
                        m.set_rules("user_defined")
                        m.run()
                        m.save_profile("db_md_po_1", description="Middle-cluster1-private")
                except ValueError: 
                        continue
       # Middle-size cities,, cluster 2
        for i in range(n_profiles_clust2):
                try: 
                        m = Mobility(config_folder='config_files')
                        # Customization starts
                        m.WEEKS = WEEKS
                        m.rules = RULE
                        # Customization ends
                        m.set_params(name_prefix="MD2PO",
                                        time_format='seconds',
                                        total_time=3600*24*365, # one week (in seconds)
                                        time_step=300, # 5 minutes need to be put in seconds
                                        category="user_defined",
                                        reference_date="01/01/2020")
                        m.set_stats(stat_ntrip_path="distntrips-week-md-im24-3.csv",
                                        stat_dest_path="condtripdest-ntrk-week-md-im24-3.csv",
                                        stat_km_duration_path="conddistduration-week-md-im24-3.csv",
                                        time_format='hours')
                        m.set_rules("user_defined")
                        m.run()
                        m.save_profile("db_md_po_2", description="Middle-cluster2-private")
                except ValueError:
                        continue
        # Middle-size cities, cluster 3
        for i in range(n_profiles_clust3):
                try:
                        m = Mobility(config_folder='config_files')
                        # Customization starts
                        m.WEEKS = WEEKS
                        m.rules = RULE
                        # Customization ends
                        m.set_params(name_prefix="MD3PO",
                                        time_format='seconds',
                                        total_time=3600*24*365, # one week (in seconds)
                                        time_step=300, # 5 minutes need to be put in seconds
                                        category="user_defined",
                                        reference_date="01/01/2020")
                        m.set_stats(stat_ntrip_path="distntrips-week-md-im24-4.csv",
                                        stat_dest_path="condtripdest-ntrk-week-md-im24-4.csv",
                                        stat_km_duration_path="conddistduration-week-md-im24-4.csv",
                                        time_format='hours')
                        m.set_rules("user_defined")
                        m.run()
                        m.save_profile("db_md_po_3", description="Middle-cluster3-private")
                except ValueError:
                        continue
        # Middle-size cities, cluster 4
        for i in range(n_profiles_clust4):
                try:
                        m = Mobility(config_folder='config_files')
                        # Customization starts
                        m.WEEKS = WEEKS
                        m.rules = RULE
                        # Customization ends
                        m.set_params(name_prefix="MD4PO",
                                        time_format='seconds',
                                        total_time=3600*24*365, # one week (in seconds)
                                        time_step=300, # 5 minutes need to be put in seconds
                                        category="user_defined",
                                        reference_date="01/01/2020")
                        m.set_stats(stat_ntrip_path="distntrips-week-md-im24-5.csv",
                                        stat_dest_path="condtripdest-ntrk-week-md-im24-5.csv",
                                        stat_km_duration_path="conddistduration-week-md-im24-5.csv",
                                        time_format='hours')
                        m.set_rules("user_defined")
                        m.run()
                        m.save_profile("db_md_po_4", description="Middle-cluster4-private")
                except ValueError:
                        continue
        # Small cities, cluster 1
        for i in range(n_profiles_clust1):
                try:
                        m = Mobility(config_folder='config_files')
                        # Customization starts
                        m.WEEKS = WEEKS
                        m.rules = RULE
                        # Customization ends
                        m.set_params(name_prefix="S1PO",
                                        time_format='seconds',
                                        total_time=3600*24*365, # one week (in seconds)
                                        time_step=300, # 5 minutes need to be put in seconds
                                        category="user_defined",
                                        reference_date="01/01/2020")
                        m.set_stats(stat_ntrip_path="distntrips-week-s-im24-2.csv",
                                        stat_dest_path="condtripdest-ntrk-week-s-im24-2.csv",
                                        stat_km_duration_path="conddistduration-week-s-im24-2.csv",
                                        time_format='hours')
                        m.set_rules("user_defined")
                        m.run()
                        m.save_profile("db_s_po_1", description="Small-cluster1-private")
                except ValueError:
                        continue
        # Small cities, cluster 2
        for i in range(n_profiles_clust2):
                try:
                        m = Mobility(config_folder='config_files')
                        # Customization starts
                        m.WEEKS = WEEKS
                        m.rules = RULE
                        # Customization ends
                        m.set_params(name_prefix="S2PO",
                                        time_format='seconds',
                                        total_time=3600*24*365, # one week (in seconds)
                                        time_step=300, # 5 minutes need to be put in seconds
                                        category="user_defined",
                                        reference_date="01/01/2020")
                        m.set_stats(stat_ntrip_path="distntrips-week-s-im24-3.csv",
                                        stat_dest_path="condtripdest-ntrk-week-s-im24-3.csv",
                                        stat_km_duration_path="conddistduration-week-s-im24-3.csv",
                                        time_format='hours')
                        m.set_rules("user_defined")
                        m.run()
                        m.save_profile("db_s_po_2", description="Small-cluster2-private")
                except ValueError:
                        continue
        # Small cities, cluster 3
        for i in range(n_profiles_clust3):
                try:
                        m = Mobility(config_folder='config_files')
                        # Customization starts
                        m.WEEKS = WEEKS
                        m.rules = RULE
                        # Customization ends
                        m.set_params(name_prefix="S3PO",
                                        time_format='seconds',
                                        total_time=3600*24*365, # one week (in seconds)
                                        time_step=300, # 5 minutes need to be put in seconds
                                        category="user_defined",
                                        reference_date="01/01/2020")
                        m.set_stats(stat_ntrip_path="distntrips-week-s-im24-4.csv",
                                        stat_dest_path="condtripdest-ntrk-week-s-im24-4.csv",
                                        stat_km_duration_path="conddistduration-week-s-im24-4.csv",
                                        time_format='hours')
                        m.set_rules("user_defined")
                        m.run()
                        m.save_profile("db_s_po_3", description="Small-cluster3-private")
                except ValueError:
                        continue
        # Small cities, cluster 4
        for i in range(n_profiles_clust4):
                try:
                        m = Mobility(config_folder='config_files')
                        # Customization starts
                        m.WEEKS = WEEKS
                        m.rules = RULE
                        # Customization ends
                        m.set_params(name_prefix="S4PO",
                                        time_format='seconds',
                                        total_time=3600*24*365, # one week (in seconds)
                                        time_step=300, # 5 minutes need to be put in seconds
                                        category="user_defined",
                                        reference_date="01/01/2020")
                        m.set_stats(stat_ntrip_path="distntrips-week-s-im24-5.csv",
                                        stat_dest_path="condtripdest-ntrk-week-s-im24-5.csv",
                                        stat_km_duration_path="conddistduration-week-s-im24-5.csv",
                                        time_format='hours')
                        m.set_rules("user_defined")
                        m.run()
                        m.save_profile("db_s_po_4", description="Small-cluster4-private")
                except ValueError:
                        continue
        # Rural areas, cluster 1
        for i in range(n_profiles_clust1):
                try:
                        m = Mobility(config_folder='config_files')
                        # Customization starts
                        m.WEEKS = WEEKS
                        m.rules = RULE
                        # Customization ends
                        m.set_params(name_prefix="R1PO",
                                        time_format='seconds',
                                        total_time=3600*24*365, # one week (in seconds)
                                        time_step=300, # 5 minutes need to be put in seconds
                                        category="user_defined",
                                        reference_date="01/01/2020")
                        m.set_stats(stat_ntrip_path="distntrips-week-r-im24-2.csv",
                                        stat_dest_path="condtripdest-ntrk-week-r-im24-2.csv",
                                        stat_km_duration_path="conddistduration-week-r-im24-2.csv",
                                        time_format='hours')
                        m.set_rules("user_defined")
                        m.run()
                        m.save_profile("db_r_po_1", description="Rural-cluster1-private")
                except ValueError:
                        continue
        # Rural areas, cluster 2
        for i in range(n_profiles_clust2):
                try:
                        m = Mobility(config_folder='config_files')
                        # Customization starts
                        m.WEEKS = WEEKS
                        m.rules = RULE
                        # Customization ends
                        m.set_params(name_prefix="R2PO",
                                        time_format='seconds',
                                        total_time=3600*24*365, # one week (in seconds)
                                        time_step=300, # 5 minutes need to be put in seconds
                                        category="user_defined",
                                        reference_date="01/01/2020")
                        m.set_stats(stat_ntrip_path="distntrips-week-r-im24-3.csv",
                                        stat_dest_path="condtripdest-ntrk-week-r-im24-3.csv",
                                        stat_km_duration_path="conddistduration-week-r-im24-3.csv",
                                        time_format='hours')
                        m.set_rules("user_defined")
                        m.run()
                        m.save_profile("db_r_po_2", description="Rural-cluster2-private")
                except ValueError:
                        continue
        # Rural areas, cluster 3
        for i in range(n_profiles_clust3):
                try:
                        m = Mobility(config_folder='config_files')
                        # Customization starts
                        m.WEEKS = WEEKS
                        m.rules = RULE
                        # Customization ends
                        m.set_params(name_prefix="R3PO",
                                        time_format='seconds',
                                        total_time=3600*24*365, # one week (in seconds)
                                        time_step=300, # 5 minutes need to be put in seconds
                                        category="user_defined",
                                        reference_date="01/01/2020")
                        m.set_stats(stat_ntrip_path="distntrips-week-r-im24-4.csv",
                                        stat_dest_path="condtripdest-ntrk-week-r-im24-4.csv",
                                        stat_km_duration_path="conddistduration-week-r-im24-4.csv",
                                        time_format='hours')
                        m.set_rules("user_defined")
                        m.run()
                        m.save_profile("db_r_po_3", description="Rural-cluster3-private")
                except ValueError:
                        continue
        # Rural areas, cluster 4
        for i in range(n_profiles_clust4):
                try:
                        m = Mobility(config_folder='config_files')
                        # Customization starts
                        m.WEEKS = WEEKS
                        m.rules = RULE
                        # Customization ends
                        m.set_params(name_prefix="R4PO",
                                        time_format='seconds',
                                        total_time=3600*24*365, # one week (in seconds)
                                        time_step=300, # 5 minutes need to be put in seconds
                                        category="user_defined",
                                        reference_date="01/01/2020")
                        m.set_stats(stat_ntrip_path="distntrips-week-r-im24-5.csv",
                                        stat_dest_path="condtripdest-ntrk-week-r-im24-5.csv",
                                        stat_km_duration_path="conddistduration-week-r-im24-5.csv",
                                        time_format='hours')
                        m.set_rules("user_defined")
                        m.run()
                        m.save_profile("db_r_po_4", description="Rural-cluster4-private")
                except ValueError:
                        continue
