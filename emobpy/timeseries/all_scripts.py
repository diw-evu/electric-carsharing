import os

#Step 1 - Mobility
os.system('python Step1Mobility.py')
os.system('python Step1MobilityCS.py')

# Step 2 - DrivingConsumption
os.system('python Step2DrivingConsumption.py')

#Step 3 - GridAvailability
os.system('python Step3GridAvailability.py')
os.system('python Step3GridAvailabilityCS.py')

#Step 4 - GridDemand
os.system('python Step4GridDemand.py')

#Export 
os.system('python Export.py')