from emobpy import Availability, DataBase

# Initialize seed
from emobpy.tools import set_seed

set_seed()
if __name__ == '__main__':
    databases=['db_mt_4_cs_slow_sr5', 'db_b_4_cs_slow_sr5', 'db_md_4_cs_slow_sr5', 'db_s_4_cs_slow_sr5',
               'db_mt_14_cs_fast_sr5', 'db_b_24_cs_fast_sr5', 'db_md_34_cs_fast_sr5']
    for db in databases:
        DB = DataBase(db)                                  # Instance of profiles' database whose input is the pickle files' folder
        DB.loadfiles_batch(kind="consumption")                # loading consumption pickle files to the database
        cnames = list(DB.db.keys())                       # getting the id of the first consumption profile

        for i in cnames:
            station_distribution = {                                   # Dictionary with charging stations type probability distribution per the purpose of the trip (location or destination)
                'prob_charging_point': {
                    'home': {'public': 0.0, 'fast75': 1.0, 'workplace': 0.0, 'none': 0.0},
                    'work': {'public': 0.0, 'fast75': 1.0, 'workplace': 0.0, 'none': 0.0},
                    'errands': {'public': 0.0, 'fast75': 1.0, 'workplace': 0.0, 'none': 0.0},
                    'leisure': {'public': 0.0, 'fast75': 1.0, 'workplace': 0.0, 'none': 0.0},
                    'driving': {'none': 0.99, 'fast75': 0.005, 'fast150': 0.005}},
                'capacity_charging_point': {                                       # Nominal power rating of charging station in kW
                    'public': 22,
                    'home': 3.7,
                    'workplace': 11,
                    'none': 0,  # dummy station
                    'fast75': 75,
                    'fast150': 150}
            }

            ga = Availability(i, DB)
            ga.set_scenario(station_distribution)
            ga.battery_capacity = 100
            ga.run()
            ga.save_profile(db)
