from emobpy import Charging, DataBase

# Initialize seed
from emobpy.tools import set_seed
set_seed()

if __name__ == "__main__":

    databases=['db_mt_po_1', 'db_mt_po_2', 'db_mt_po_3', 'db_mt_po_4', 
               'db_b_po_1', 'db_b_po_2', 'db_b_po_3', 'db_b_po_4', 
               'db_md_po_1', 'db_md_po_2', 'db_md_po_3', 'db_md_po_4',
               'db_s_po_1', 'db_s_po_2', 'db_s_po_3', 'db_s_po_4',
               'db_r_po_1', 'db_r_po_2', 'db_r_po_3', 'db_r_po_4',
               'db_mt_4_cs_slow_sr5', 'db_b_4_cs_slow_sr5', 'db_md_4_cs_slow_sr5', 'db_s_4_cs_slow_sr5',
               'db_mt_14_cs_fast_sr5', 'db_b_24_cs_fast_sr5', 'db_md_34_cs_fast_sr5']
    for db in databases:
        DB = DataBase(db)                                  # Instance of profiles' database whose input is the pickle files' folder
        DB.loadfiles_batch(kind="availability")                     # loading availability pickle files to the database
        aname = list(DB.db.keys())                            

        #strategies = ["immediate", "balanced", "from_0_to_24_at_home", "from_23_to_8_at_home"]

        for i in aname:
            c = Charging(i)
            c.load_scenario(DB)
            c.set_sub_scenario("balanced")
            c.run()
            c.save_profile(db)

#%%
"""     databases=['db_mt_po_1', 'db_mt_po_2', 'db_mt_po_3', 'db_mt_po_4', 
               'db_b_po_1', 'db_b_po_2', 'db_b_po_3', 'db_b_po_4', 
               'db_md_po_1', 'db_md_po_2', 'db_md_po_3', 'db_md_po_4',
               'db_s_po_1', 'db_s_po_2', 'db_s_po_3', 'db_s_po_4',
               'db_r_po_1', 'db_r_po_2', 'db_r_po_3', 'db_r_po_4',
               'db_mt_4_cs_slow_sr5', 'db_b_4_cs_slow_sr5', 'db_md_4_cs_slow_sr5', 'db_s_4_cs_slow_sr5',
               'db_mt_14_cs_fast_sr5', 'db_b_24_cs_fast_sr5', 'db_md_34_cs_fast_sr5'] """
