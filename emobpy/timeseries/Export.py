from emobpy import Export
from emobpy import DataBase

dictionary ={'db_mt_po_1' : ['bev_time_series_po_metro_1_sr0.csv', 'bev_data_input_po_metro_1_sr0.csv'], 
             'db_mt_po_2' : ['bev_time_series_po_metro_2_sr0.csv', 'bev_data_input_po_metro_2_sr0.csv'],
             'db_mt_po_3' : ['bev_time_series_po_metro_3_sr0.csv', 'bev_data_input_po_metro_3_sr0.csv'],
             'db_mt_po_4' : ['bev_time_series_po_metro_4_sr0.csv', 'bev_data_input_po_metro_4_sr0.csv'],
             'db_b_po_1'  : ['bev_time_series_po_big_1_sr0.csv', 'bev_data_input_po_big_1_sr0.csv'],
             'db_b_po_2'  : ['bev_time_series_po_big_2_sr0.csv', 'bev_data_input_po_big_2_sr0.csv'],
             'db_b_po_3'  : ['bev_time_series_po_big_3_sr0.csv', 'bev_data_input_po_big_3_sr0.csv'],
             'db_b_po_4'  : ['bev_time_series_po_big_4_sr0.csv', 'bev_data_input_po_big_4_sr0.csv'],
             'db_md_po_1' : ['bev_time_series_po_middle_1_sr0.csv', 'bev_data_input_po_middle_1_sr0.csv'], 
             'db_md_po_2' : ['bev_time_series_po_middle_2_sr0.csv', 'bev_data_input_po_middle_2_sr0.csv'],
             'db_md_po_3' : ['bev_time_series_po_middle_3_sr0.csv', 'bev_data_input_po_middle_3_sr0.csv'],
             'db_md_po_4' : ['bev_time_series_po_middle_4_sr0.csv', 'bev_data_input_po_middle_4_sr0.csv'],
             'db_s_po_1'  : ['bev_time_series_po_small_1_sr0.csv', 'bev_data_input_po_small_1_sr0.csv'],
             'db_s_po_2'  : ['bev_time_series_po_small_2_sr0.csv', 'bev_data_input_po_small_2_sr0.csv'],
             'db_s_po_3'  : ['bev_time_series_po_small_3_sr0.csv', 'bev_data_input_po_small_3_sr0.csv'],
             'db_s_po_4'  : ['bev_time_series_po_small_4_sr0.csv', 'bev_data_input_po_small_4_sr0.csv'],
             'db_r_po_1'  : ['bev_time_series_po_rural_1_sr0.csv', 'bev_data_input_po_rural_1_sr0.csv'],
             'db_r_po_2'  : ['bev_time_series_po_rural_2_sr0.csv', 'bev_data_input_po_rural_2_sr0.csv'],
             'db_r_po_3'  : ['bev_time_series_po_rural_3_sr0.csv', 'bev_data_input_po_rural_3_sr0.csv'],
             'db_r_po_4'  : ['bev_time_series_po_rural_4_sr0.csv', 'bev_data_input_po_rural_4_sr0.csv'],
             'db_mt_4_cs_slow_sr5' : ['bev_time_series_cs_slow_metro_4_sr5.csv', 'bev_data_input_cs_slow_metro_4_sr5.csv'],
             'db_b_4_cs_slow_sr5'  : ['bev_time_series_cs_slow_big_4_sr5.csv', 'bev_data_input_cs_slow_big_4_sr5.csv'],
             'db_md_4_cs_slow_sr5' : ['bev_time_series_cs_slow_middle_4_sr5.csv', 'bev_data_input_cs_slow_middle_4_sr5.csv'], 
             'db_s_4_cs_slow_sr5'  : ['bev_time_series_cs_slow_small_4_sr5.csv', 'bev_data_input_cs_slow_small_4_sr5.csv'],
             'db_mt_14_cs_fast_sr5' : ['bev_time_series_cs_fast_metro_14_sr5.csv', 'bev_data_input_cs_fast_metro_14_sr5.csv'], 
             'db_b_24_cs_fast_sr5'  : ['bev_time_series_cs_fast_big_24_sr5.csv', 'bev_data_input_cs_fast_big_24_sr5.csv'],
             'db_md_34_cs_fast_sr5' : ['bev_time_series_cs_fast_middle_34_sr5.csv', 'bev_data_input_cs_fast_middle_34_sr5.csv']
} 

for db in dictionary.keys():
    DB = DataBase(db)
    DB.update()
    Exp = Export()
    Exp.loaddata(DB)
    Exp.to_csv()
    Exp.save_files(dictionary[db][0], dictionary[db][1], repository="/home/agueret/Documents/bev_timeseries/emobpy/export/")