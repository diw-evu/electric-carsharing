#%% Load emobpy and set seed
from emobpy import Consumption, HeatInsulation, BEVspecs, DataBase
# Initialize seed
from emobpy.tools import set_seed
set_seed()

#%%
if __name__ == '__main__':
    database_batterylarge=['db_mt_po_1',  
                            'db_b_po_1',  
                            'db_md_po_1', 
                            'db_s_po_1', 
                            'db_r_po_1', 
                            'db_mt_4_cs_slow_sr5', 'db_b_4_cs_slow_sr5', 'db_md_4_cs_slow_sr5', 'db_s_4_cs_slow_sr5',
                            'db_mt_14_cs_fast_sr5', 'db_b_24_cs_fast_sr5', 'db_md_34_cs_fast_sr5']
    database_batterysmall=['db_mt_po_2', 'db_mt_po_3', 'db_mt_po_4', 
                            'db_b_po_2', 'db_b_po_3', 'db_b_po_4', 
                            'db_md_po_2', 'db_md_po_3', 'db_md_po_4',
                            'db_s_po_2', 'db_s_po_3', 'db_s_po_4',
                            'db_r_po_2', 'db_r_po_3', 'db_r_po_4']
    for dbl in database_batterylarge:
        DB = DataBase(dbl)                                     # Instance of profiles' database whose input is the pickle files' folder
        DB.loadfiles_batch(kind="driving")                   # loading mobility pickle files to the database
        names = list(DB.db.keys())                           # getting the id of the first mobility profile

        for i in names:
            HI = HeatInsulation(True)                            # Creating the heat insulation configuration by copying the default configuration
            BEVS = BEVspecs()                                    # Database that contains BEV models
            VW_ID3CS = BEVS.model(('Volkswagen','ID.3CS',2020))      # Model instance that contains vehicle parameters
            #VW_ID3CS.parameters['battery_cap']=100
            c = Consumption(i, VW_ID3CS)
            c.load_setting_mobility(DB)
            c.run(
                heat_insulation=HI,
                weather_country='DE',
                weather_year=2016,
                passenger_mass=75,                   # kg
                passenger_sensible_heat=70,          # W
                passenger_nr=1.5,                    # Passengers per vehicle including driver
                air_cabin_heat_transfer_coef=20,     # W/(m2K). Interior walls
                air_flow = 0.02,                     # m3/s. Ventilation
                driving_cycle_type='WLTC',           # Two options "WLTC" or "EPA"
                road_type=0,                         # For rolling resistance, Zero represents a new road.
                road_slope=0
                )
            c.save_profile(dbl)
    for dbs in database_batterysmall:
        DB = DataBase(dbs)                                     # Instance of profiles' database whose input is the pickle files' folder
        DB.loadfiles_batch(kind="driving")                   # loading mobility pickle files to the database
        names = list(DB.db.keys())                           # getting the id of the first mobility profile

        for i in names:
            HI = HeatInsulation(True)                            # Creating the heat insulation configuration by copying the default configuration
            BEVS = BEVspecs()                                    # Database that contains BEV models
            VW_ID3 = BEVS.model(('Volkswagen','ID.3',2020))      # Model instance that contains vehicle parameters
            c = Consumption(i, VW_ID3)
            c.load_setting_mobility(DB)
            c.run(
                heat_insulation=HI,
                weather_country='DE',
                weather_year=2016,
                passenger_mass=75,                   # kg
                passenger_sensible_heat=70,          # W
                passenger_nr=1.5,                    # Passengers per vehicle including driver
                air_cabin_heat_transfer_coef=20,     # W/(m2K). Interior walls
                air_flow = 0.02,                     # m3/s. Ventilation
                driving_cycle_type='WLTC',           # Two options "WLTC" or "EPA"
                road_type=0,                         # For rolling resistance, Zero represents a new road.
                road_slope=0
                )
            c.save_profile(dbs)

#%% Get relevant information
""" DB = DataBase('db_r_po_3')
DB.loadfiles_batch(kind="consumption")
DB.db.keys()
test = DB.db['R3PO_W53_1090e_Volkswagen_ID.3_2020_be8ba']
test['vehicle'].parameters['battery_cap'] """