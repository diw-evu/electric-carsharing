###################################
####### Mobility time series ######
###################################

#%% Load emobpy and initialize seed
from emobpy import MobilityCS
from emobpy.tools import set_seed
set_seed()

#%% Redefine typology of day and destination types
WEEKS = {
    0:{'day': 'Sunday',    'week': 'sunday',   'day_code': 'sunday'},
    1:{'day': 'Monday',    'week': 'weekday',  'day_code': 'weekday'},
    2:{'day': 'Tuesday',   'week': 'weekday',  'day_code': 'weekday'},
    3:{'day': 'Wednesday', 'week': 'weekday',  'day_code': 'weekday'},
    4:{'day': 'Thursday',  'week': 'weekday',  'day_code': 'weekday'},
    5:{'day': 'Friday',    'week': 'weekday',  'day_code': 'weekday'},
    6:{'day': 'Saturday',  'week': 'saturday', 'day_code': 'saturday'}
}

RULE = {
    'weekday':
            {'n_trip_out': [],
             'max_same_trip_times': False,
             'last_trip_to':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'first_trip_to':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'not_last_trip_to':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'at_least_one_trip':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'overall_min_time_at':{'home': False,'work': False, 'errands': False,'leisure': False, 'driving':False},
             'overall_max_time_at':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'min_state_duration':{'home':False,'work':False, 'errands':False,'leisure':False,'driving':False},
             'max_state_duration':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'equal_state_and_destination':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False}
            },
    'saturday':
            {'n_trip_out': [],
             'max_same_trip_times': False,
             'last_trip_to':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'first_trip_to':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'not_last_trip_to':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'at_least_one_trip':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'overall_min_time_at':{'home': False,'work': False, 'errands': False,'leisure': False, 'driving':False},
             'overall_max_time_at':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'min_state_duration':{'home':False,'work':False, 'errands':False,'leisure':False,'driving':False},
             'max_state_duration':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'equal_state_and_destination':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False}
            },
    'sunday':
            {'n_trip_out': [],
             'max_same_trip_times': False,
             'last_trip_to':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'first_trip_to':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'not_last_trip_to':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'at_least_one_trip':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'overall_min_time_at':{'home': False,'work': False, 'errands': False,'leisure': False, 'driving':False},
             'overall_max_time_at':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'min_state_duration':{'home':False,'work':False,'errands':False,'leisure':False,'driving':False},
             'max_state_duration':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False},
             'equal_state_and_destination':{'home': False,'work': False, 'errands': False,'leisure': False,'driving':False}
            },
       }

if __name__ == '__main__':
        n_profiles = 60 #60
        substrate = 5
        ###### Slow carsharing uptake #######
        # Metropolises, cluster 4
        for i in range(n_profiles):
                try:
                        m = MobilityCS(config_folder='config_files')
                        # Customization starts
                        m.WEEKS = WEEKS
                        m.rules = RULE
                        # Customization ends
                        m.set_params(name_prefix="M4CSslo",
                                        time_format='seconds',
                                        total_time=3600*24*365, # one week (in seconds)
                                        time_step=300, # 5 minutes need to be put in seconds
                                        category="user_defined",
                                        reference_date="01/01/2020")
                        m.set_stats(stat_ntrip_path="cs-distntrips-week-mt-im24-5-sr{}.csv".format(substrate),
                                        stat_dest_path="cs-disttripdest-week-mt-im24-clust5.csv",
                                        stat_km_duration_path="cs-conddistduration-week-mt-im24-55.csv",
                                        time_format='hours')
                        m.set_rules("user_defined")
                        m.run()
                        m.save_profile("db_mt_4_cs_slow_sr{}".format(substrate), description="Metro-cluster4-shared-slow")
                except ValueError:
                        continue
        # Big cities, cluster 4
        for i in range(n_profiles):
                try:
                        m = MobilityCS(config_folder='config_files')
                        # Customization starts
                        m.WEEKS = WEEKS
                        m.rules = RULE
                        # Customization ends
                        m.set_params(name_prefix="B4CSslo",
                                        time_format='seconds',
                                        total_time=3600*24*365, # one week (in seconds)
                                        time_step=300, # 5 minutes need to be put in seconds
                                        category="user_defined",
                                        reference_date="01/01/2020")
                        m.set_stats(stat_ntrip_path="cs-distntrips-week-b-im24-5-sr{}.csv".format(substrate),
                                        stat_dest_path="cs-disttripdest-week-b-im24-clust5.csv",
                                        stat_km_duration_path="cs-conddistduration-week-b-im24-55.csv",
                                        time_format='hours')
                        m.set_rules("user_defined")
                        m.run()
                        m.save_profile("db_b_4_cs_slow_sr{}".format(substrate), description="Big-cluster4-shared-slow")
                except ValueError:
                        continue
        # Middle-size cities, cluster 4
        for i in range(n_profiles):
                try:
                        m = MobilityCS(config_folder='config_files')
                        # Customization starts
                        m.WEEKS = WEEKS
                        m.rules = RULE
                        # Customization ends
                        m.set_params(name_prefix="MD4CSslo",
                                        time_format='seconds',
                                        total_time=3600*24*365, # one week (in seconds)
                                        time_step=300, # 5 minutes need to be put in seconds
                                        category="user_defined",
                                        reference_date="01/01/2020")
                        m.set_stats(stat_ntrip_path="cs-distntrips-week-md-im24-5-sr{}.csv".format(substrate),
                                        stat_dest_path="cs-disttripdest-week-md-im24-clust5.csv",
                                        stat_km_duration_path="cs-conddistduration-week-md-im24-55.csv",
                                        time_format='hours')
                        m.set_rules("user_defined")
                        m.run()
                        m.save_profile("db_md_4_cs_slow_sr{}".format(substrate), description="Middle-cluster4-shared-slow")
                except ValueError:
                        continue
        # Small cities, cluster 4
        for i in range(n_profiles):
                try:
                        m = MobilityCS(config_folder='config_files')
                        # Customization starts
                        m.WEEKS = WEEKS
                        m.rules = RULE
                        # Customization ends
                        m.set_params(name_prefix="S4CSslo",
                                        time_format='seconds',
                                        total_time=3600*24*365, # one week (in seconds)
                                        time_step=300, # 5 minutes need to be put in seconds
                                        category="user_defined",
                                        reference_date="01/01/2020")
                        m.set_stats(stat_ntrip_path="cs-distntrips-week-s-im24-5-sr{}.csv".format(substrate),
                                        stat_dest_path="cs-disttripdest-week-s-im24-clust5.csv",
                                        stat_km_duration_path="cs-conddistduration-week-s-im24-55.csv",
                                        time_format='hours')
                        m.set_rules("user_defined")
                        m.run()
                        m.save_profile("db_s_4_cs_slow_sr{}".format(substrate), description="Small-cluster4-shared-slow")
                except ValueError:
                        continue
        ###### Fast carsharing uptake #######
        # Metropolises, clusters 1 to 4
        for i in range(n_profiles):
                try:
                        m = MobilityCS(config_folder='config_files')
                        # Customization starts
                        m.WEEKS = WEEKS
                        m.rules = RULE
                        # Customization ends
                        m.set_params(name_prefix="M14CSfast",
                                        time_format='seconds',
                                        total_time=3600*24*365, # one week (in seconds)
                                        time_step=300, # 5 minutes need to be put in seconds
                                        category="user_defined",
                                        reference_date="01/01/2020")
                        m.set_stats(stat_ntrip_path="cs-distntrips-week-mt-im24-25-sr{}.csv".format(substrate),
                                        stat_dest_path="cs-disttripdest-week-mt-im24-group25.csv",
                                        stat_km_duration_path="cs-conddistduration-week-mt-im24-25.csv",
                                        time_format='hours')
                        m.set_rules("user_defined")
                        m.run()
                        m.save_profile("db_mt_14_cs_fast_sr{}".format(substrate), description="Metro-cluster14-shared-fast")
                except ValueError:
                        continue
        # Big cities, clusters 2 to 4
        for i in range(n_profiles):
                try:
                        m = MobilityCS(config_folder='config_files')
                        # Customization starts
                        m.WEEKS = WEEKS
                        m.rules = RULE
                        # Customization ends
                        m.set_params(name_prefix="B24CSfast",
                                        time_format='seconds',
                                        total_time=3600*24*365, # one week (in seconds)
                                        time_step=300, # 5 minutes need to be put in seconds
                                        category="user_defined",
                                        reference_date="01/01/2020")
                        m.set_stats(stat_ntrip_path="cs-distntrips-week-b-im24-35-sr{}.csv".format(substrate),
                                        stat_dest_path="cs-disttripdest-week-b-im24-group35.csv",
                                        stat_km_duration_path="cs-conddistduration-week-b-im24-35.csv",
                                        time_format='hours')
                        m.set_rules("user_defined")
                        m.run()
                        m.save_profile("db_b_24_cs_fast_sr{}".format(substrate), description="Big-cluster24-shared-fast")
                except ValueError:
                        continue
        # Middle cities, clusters 3 and 4
        for i in range(n_profiles):
                try:
                        m = MobilityCS(config_folder='config_files')
                        # Customization starts
                        m.WEEKS = WEEKS
                        m.rules = RULE
                        # Customization ends
                        m.set_params(name_prefix="MD34CSfast",
                                        time_format='seconds',
                                        total_time=3600*24*365, # one week (in seconds)
                                        time_step=300, # 5 minutes need to be put in seconds
                                        category="user_defined",
                                        reference_date="01/01/2020")
                        m.set_stats(stat_ntrip_path="cs-distntrips-week-md-im24-45-sr{}.csv".format(substrate),
                                        stat_dest_path="cs-disttripdest-week-md-im24-group45.csv",
                                        stat_km_duration_path="cs-conddistduration-week-md-im24-45.csv",
                                        time_format='hours')
                        m.set_rules("user_defined")
                        m.run()
                        m.save_profile("db_md_34_cs_fast_sr{}".format(substrate), description="Middle-cluster34-shared-fast")
                except ValueError:
                        continue


