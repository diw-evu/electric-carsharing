#%% Load modules
import pandas as pd
import os
from pathlib import Path

#%% >>>> Define work environment (PC or Tesla) <<<<<
work_environment = 'Tesla' #Need to specify 'PC' or 'Tesla'

path_git = {'PC'    : 'C:\Adeline\Git',
            'Tesla' : '/home/agueret/Documents/Git/'}

#%% Define a function: collect specific ev time series for a given location x scenario and a cluster range
def collect_timeseries(name: str, ntotal_profiles: int, location: str, cluster_range: list, sample: int =1, scenario_data_name: str = 'iteration_data.csv', data_input_csv_path: Path = '/home/agueret/Documents/bev_timeseries/emobpy/export/csv'): 
    path = path_git[work_environment]
    all_scenario = pd.read_csv(os.path.join(path, 'ecarsharing_project', 'postprocessing', scenario_data_name), sep=',')
    scenario = all_scenario[(all_scenario['scenario']==name) & 
                            (all_scenario['nprofiles_baseline']==ntotal_profiles) &
                            (all_scenario['location']==location) &
                            (all_scenario['cluster'].isin(cluster_range))]
    scenario['nprofiles_cum'] = scenario['nprofiles'].cumsum()
    nprofiles_range = scenario['nprofiles'].sum()
    hours =['t{0:04}'.format(x) for x in range(1,8761)] 
    timeseries_input = pd.DataFrame(columns=[hours], index=[x for x in range(1,(nprofiles_range)+1)])
    param = ['ev_ed' for i in range(nprofiles_range)] 
    timeseries_input.insert(0, 'headers_time_ev', param)
    timeseries_input.insert(1, 'n', 'DE')
    ev =['ev{0:03}'.format(x) for x in range(1,nprofiles_range+1)]
    timeseries_input.insert(2, 'ev', ev)
    timeseries_input.insert(3, 'share_unweighted', 0)
    timeseries_input.insert(4, 'ev_quant', 0)
    nrows=scenario.shape[0]
    s = sample
    for i in range(nrows):
        vehicle_type = scenario.iloc[i]['type']
        location = scenario.iloc[i]['location']
        cluster = scenario.iloc[i]['cluster']
        nprofiles = scenario.iloc[i]['nprofiles']
        sr = scenario.iloc[i]['profile_substrate']
        share_u = scenario.iloc[i]['share_unweighted']
        nevs = scenario.iloc[i]['ev_quant']
        name_in = 'bev_time_series_gd_{}_{}_{}_sr{}.csv'.format(vehicle_type, location, cluster, sr) 
        data = pd.read_csv(os.path.join(data_input_csv_path, name_in), sep=',', low_memory=False, header=None)
        extract = data.iloc[nprofiles*(s-1):nprofiles*s]
        if i==0:
            timeseries_input.iloc[0:nprofiles,5:] = extract
            timeseries_input.iloc[0:nprofiles,3] = share_u
            timeseries_input.iloc[0:nprofiles,4] = nevs
        else:
            first = scenario.iloc[i-1]['nprofiles_cum'] 
            last = scenario.iloc[i]['nprofiles_cum'] 
            timeseries_input.iloc[first:last,5:] = extract
            timeseries_input.iloc[first:last,3] = share_u
            timeseries_input.iloc[first:last,4] = nevs
    cluster_min = min(cluster_range)
    cluster_max = max(cluster_range)
    path_out = '/home/agueret/Documents/bev_timeseries/emobpy/output_for_dieter/nprofiles_exploration'
    name_out = 'ev_ged_{}_{}_{}_cluster{}{}_sample{}.csv'.format(name, ntotal_profiles, location, cluster_min, cluster_max, sample)
    timeseries_input.to_csv(os.path.join(path_out, name_out), index=False)

#%% Define a function: collect time series for a given scenario x cluster
def collect_timeseries_cluster(name: str, ntotal_profiles: int, location:str, cluster: int, sample: int, scenario_data_name: str = 'iteration_data.csv', data_input_csv_path: Path = '/home/agueret/Documents/bev_timeseries/emobpy/export/csv'): 
    path = path_git[work_environment]
    all_scenario = pd.read_csv(os.path.join(path, 'ecarsharing_project', 'postprocessing', scenario_data_name), sep=',')
    scenario = all_scenario[(all_scenario['scenario']==name) &  
                            (all_scenario['location']==location) & 
                            (all_scenario['cluster']==cluster) & 
                            (all_scenario['nprofiles_baseline']==ntotal_profiles)]
    s = sample
    vehicle_type = scenario.iloc[0]['type']
    location = scenario.iloc[0]['location']
    clust = scenario.iloc[0]['cluster']
    nprofiles = scenario.iloc[0]['nprofiles']
    sr = scenario.iloc[0]['profile_substrate']
    share_u = scenario.iloc[0]['share_unweighted']
    nevs = scenario.iloc[0]['ev_quant']
    name_in = 'bev_time_series_gd_{}_{}_{}_sr{}.csv'.format(vehicle_type, location, clust, sr) 
    data = pd.read_csv(os.path.join(data_input_csv_path, name_in), sep=',', low_memory=False, header=None)
    extract = data.iloc[nprofiles*(s-1):nprofiles*s]
# Create dataframe 
    hours =['t{0:04}'.format(x) for x in range(1,8761)] 
    timeseries_input = pd.DataFrame(columns=[hours], index=[x for x in range(1,(nprofiles)+1)])
    param = ['ev_ed' for i in range(nprofiles)]
    timeseries_input.insert(0, 'headers_time_ev', param)
    timeseries_input.insert(1, 'n', 'DE')
    ev =['ev{0:03}'.format(x) for x in range(1,nprofiles+1)]
    timeseries_input.insert(2, 'ev', ev)
    timeseries_input.insert(3, 'share_unweighted', 0)
    timeseries_input.insert(4, 'ev_quant', 0)
    timeseries_input.iloc[0:nprofiles,5:] = extract
    timeseries_input.iloc[0:nprofiles,3] = share_u
    timeseries_input.iloc[0:nprofiles,4] = nevs
    path_out = '/home/agueret/Documents/bev_timeseries/emobpy/output_for_dieter/nprofiles_exploration'
    name_out = 'ev_ged_{}_{}_{}_cluster{}_sample{}.csv'.format(name, ntotal_profiles, location, cluster, sample)
    timeseries_input.to_csv(os.path.join(path_out, name_out), index=False)

#%% Define a function: compute the weighted yearly consumption for a location x cluster range (GWh)
def yearly_ev_consumption(name: str, ntotal_profiles: int, location: str, cluster_range: list, cluster:int, sample: int, path_in: Path='/home/agueret/Documents/bev_timeseries/emobpy/output_for_dieter/nprofiles_exploration'):
    collect_timeseries(name, ntotal_profiles, location, cluster_range, sample)
    name_in = 'ev_ged_{}_{}_{}_cluster{}_sample{}.csv'.format(name, ntotal_profiles, location, cluster, sample)
    data = pd.read_csv(os.path.join(path_in, name_in), sep=',', low_memory=False)
    data['unweighted_sum'] = data.iloc[:,5:].sum(axis=1) 
    data['weighted_sum'] = data['unweighted_sum'] * data['share_unweighted'] * data['ev_quant']
    total_ev_cons = data['weighted_sum'].sum() / 1000
    return total_ev_cons

#%% Define a function: compute the weighted yearly consumption for only a given cluster x location x scenario (GWh)
def yearly_ev_consumption_cluster(name: str, ntotal_profiles: int, location: str, cluster: int, sample: int, path_in: Path='/home/agueret/Documents/bev_timeseries/emobpy/output_for_dieter/nprofiles_exploration'):
    collect_timeseries_cluster(name, ntotal_profiles, location, cluster, sample)
    name_in = 'ev_ged_{}_{}_{}_cluster{}_sample{}.csv'.format(name, ntotal_profiles, location, cluster, sample)
    data = pd.read_csv(os.path.join(path_in, name_in), sep=',', low_memory=False, index_col=[0])
    data['unweighted_sum'] = data.iloc[:,5:].sum(axis=1) 
    data['weighted_sum'] = data['unweighted_sum'] * data['share_unweighted'] * data['ev_quant']
    total_ev_cons =  data['weighted_sum'].sum() / 1000
    return total_ev_cons

#%% Define a function: compute the correction factor 
def correction_factor_slow(scenario_name: str, ntotal_profiles: int, location: str, cluster: int, sample: int):
    match_baseline = {'scen1' : 'baseline',
                      'scen2' : 'baseline2', 
                      'scen3' : 'baseline3', 
                      'scen4' : 'baseline4'}  
    r = yearly_ev_consumption_cluster(match_baseline[scenario_name], ntotal_profiles, location, cluster, sample) / yearly_ev_consumption_cluster(scenario_name, ntotal_profiles, location, cluster, sample)
    return r

def correction_factor_fast(scenario_name: str, ntotal_profiles: int, location:str, cluster: int, sample: int):
    match_baseline = {'scen3' : 'baseline3',
                      'scen4' : 'baseline4'}
    match_clusters = {14 : [1,2,3,4],
                      24 : [2,3,4],
                      34 : [3,4],
                      44 : [4]} 
    match_clust = {14 : 14,
                   24 : 24,
                   34 : 34,
                   44 : 4} 
    r = yearly_ev_consumption(match_baseline[scenario_name], ntotal_profiles, location, match_clusters[cluster], cluster, sample) / yearly_ev_consumption_cluster(scenario_name, ntotal_profiles, location, match_clust[cluster], sample)
    return r

#%% Generate the correction factors for scenario 1 and 2
scenarios_slow = ['scen1', 'scen2'] 
locations =['metro', 'big', 'middle', 'small'] 
path = path_git[work_environment]
# Cluster 4 (carsharing switchers)
all_scenario = pd.read_csv(os.path.join(path, 'ecarsharing_project', 'postprocessing', 'iteration_data.csv'), sep=',')
for s in scenarios_slow:
    for l in locations:
        c = correction_factor_slow(s, 76, l, 4, 1)
        all_scenario.loc[((all_scenario['scenario']==s) & (all_scenario['location']==l) & (all_scenario['cluster']==4)), 'correction_factor'] = c 
all_scenario.to_csv(os.path.join(path, 'ecarsharing_project', 'postprocessing', 'iteration_data.csv'), sep=',', index=False)
# All other clusters (for the shared + other BEVs setting)
locations =['metro', 'big', 'middle', 'small', 'rural'] 
all_scenario = pd.read_csv(os.path.join(path, 'ecarsharing_project', 'postprocessing', 'iteration_data.csv'), sep=',')
for cluster in range(1,4): 
    for l in locations:
        c = correction_factor_slow('scen2', 76, l, cluster, 1)
        all_scenario.loc[((all_scenario['scenario']=='scen2') & (all_scenario['location']==l) & (all_scenario['cluster']==cluster)), 'correction_factor'] = c 
all_scenario.to_csv(os.path.join(path, 'ecarsharing_project', 'postprocessing', 'iteration_data.csv'), sep=',', index=False)
# Scenario 2, cluster 4, rural
all_scenario = pd.read_csv(os.path.join(path, 'ecarsharing_project', 'postprocessing', 'iteration_data.csv'), sep=',')
all_scenario.loc[((all_scenario['scenario']=='scen2') & (all_scenario['location']=='rural') & (all_scenario['cluster']==4)), 'correction_factor'] = correction_factor_slow('scen2', 76, 'rural', 4, 1)
all_scenario.to_csv(os.path.join(path, 'ecarsharing_project', 'postprocessing', 'iteration_data.csv'), sep=',', index=False)

#%% Generate the correction factors for scenario 3 and 4
scenarios_fast = ['scen3', 'scen4'] 
match_scen_clust = {'metro' : 14,
                    'big' : 24,
                    'middle' : 34,
                    'small' : 44}
match_clust = {14 : 14,
               24 : 24,
               34 : 34,
               44 : 4}  
path = path_git[work_environment]
# Carsharing switchers 
all_scenario = pd.read_csv(os.path.join(path, 'ecarsharing_project', 'postprocessing', 'iteration_data.csv'), sep=',')
for s in scenarios_fast:
    for l in match_scen_clust.keys():
        c = correction_factor_fast(s, 76, l, match_scen_clust[l], 1)
        all_scenario.loc[((all_scenario['scenario']==s) & (all_scenario['location']==l) & (all_scenario['cluster']==match_clust[match_scen_clust[l]])), 'correction_factor'] = c 
all_scenario.to_csv(os.path.join(path, 'ecarsharing_project', 'postprocessing', 'iteration_data.csv'), sep=',', index=False)
# Others in scen4
match_location_clust = {1 : ['big', 'middle', 'small', 'rural'],
                        2 : ['middle', 'small', 'rural'],
                        3 : ['small', 'rural']}
all_scenario = pd.read_csv(os.path.join(path, 'ecarsharing_project', 'postprocessing', 'iteration_data.csv'), sep=',')
for cluster in match_location_clust.keys(): 
    for l in match_location_clust[cluster]:
        c = correction_factor_slow('scen4', 76, l, cluster, 1)
        all_scenario.loc[((all_scenario['scenario']=='scen4') & (all_scenario['location']==l) & (all_scenario['cluster']==cluster)), 'correction_factor'] = c 
all_scenario.to_csv(os.path.join(path, 'ecarsharing_project', 'postprocessing', 'iteration_data.csv'), sep=',', index=False)
# Scenario 4, cluster 4, rural
all_scenario = pd.read_csv(os.path.join(path, 'ecarsharing_project', 'postprocessing', 'iteration_data.csv'), sep=',')
c = correction_factor_slow('scen4', 76, 'rural', 4, 1)
all_scenario.loc[((all_scenario['scenario']=='scen4') & (all_scenario['location']=='rural') & (all_scenario['cluster']==4)), 'correction_factor'] = c 
all_scenario.to_csv(os.path.join(path, 'ecarsharing_project', 'postprocessing', 'iteration_data.csv'), sep=',', index=False)

#%% Define a function : compute the adjusted shares
def evcons_adjusted_shares(data_name: str):
    path = path_git[work_environment]
    data = pd.read_csv(os.path.join(path, 'ecarsharing_project', 'postprocessing', data_name), sep=',')
    data['share_weighted'] = data['share_unweighted'] * data['correction_factor']
    data.to_csv(os.path.join(path, 'ecarsharing_project', 'postprocessing', 'iteration_data.csv'), sep=',', index=False)
    return data

# %% Generate the adjusted shares (does not work yet on iterating on samples)
evcons_adjusted_shares('iteration_data.csv')

# %% Define a function: compute the EV consumption difference after scaling 
def yearly_ev_consumption_rescaled(name: str, ntotal_profiles: int, location: str, cluster:int, sample: int, path_in: Path='/home/agueret/Documents/bev_timeseries/emobpy/output_for_dieter/nprofiles_exploration'):
    iteration_data = pd.read_csv(os.path.join(path_git[work_environment], 'ecarsharing_project', 'postprocessing', 'iteration_data.csv'), sep=',', low_memory=False)
    name_in = 'ev_ged_{}_{}_{}_cluster{}_sample{}.csv'.format(name, ntotal_profiles, location, cluster, sample)
    data = pd.read_csv(os.path.join(path_in, name_in), sep=',', low_memory=False, index_col=[0])
    s = iteration_data[(iteration_data['scenario']==name) & 
                       (iteration_data['nprofiles_baseline']==ntotal_profiles) & 
                       (iteration_data['location']==location) & 
                       (iteration_data['cluster']==cluster)].share_weighted.values[0]
    data['unweighted_sum'] = data.iloc[:,5:].sum(axis=1) 
    data['weighted_sum'] = data['unweighted_sum'] * s * data['ev_quant']
    total_ev_cons = data['weighted_sum'].sum() / 1000
    return total_ev_cons

# %% Test for the difference after rescaling 
baseline = {'baseline3' : 'scen3',
            'baseline4' : 'scen4'} 
location_cluster = {'metro' : [[1,2,3,4], 14],
                    'big' :   [[2,3,4], 24],
                    'middle' : [[3,4], 34]} 
diff = [] 
for i in location_cluster.keys() : 
    for j in baseline.keys() : 
        l = yearly_ev_consumption(j, 76, i, location_cluster[i][0], location_cluster[i][1], 1) -  yearly_ev_consumption_rescaled(baseline[j], 76, i, location_cluster[i][1], 1)
        diff.append(l)

baseline = yearly_ev_consumption('baseline3', 76, 'small', [4], 44, 1)
for i in location_cluster.keys() : 
    baseline = baseline + yearly_ev_consumption('baseline3', 76, i, location_cluster[i][0], location_cluster[i][1], 1)
rescale = yearly_ev_consumption_rescaled('scen3', 76, 'small', 4, 1)
for i in location_cluster.keys() : 
    rescale = rescale + yearly_ev_consumption_rescaled('scen3', 76, i, location_cluster[i][1], 1)
    
small_a = yearly_ev_consumption('baseline3', 76, 'small', [4], 44, 1) -  yearly_ev_consumption_rescaled('scen3', 76, 'small', 4, 1)
small_b = yearly_ev_consumption('baseline4', 76, 'small', [4], 44, 1) -  yearly_ev_consumption_rescaled('scen4', 76, 'small', 4, 1)

# %%
locations = ['metro', 'big', 'middle', 'small']
for l in locations: 
    small_b = yearly_ev_consumption_cluster('baseline2', 76, l, 4, 1) -  yearly_ev_consumption_rescaled('scen2', 76, l, 4, 1)
    print(small_b)
