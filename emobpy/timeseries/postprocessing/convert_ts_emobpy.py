#%% Load modules
import pandas as pd
import os
from pathlib import Path

#%% Define a function to convert csv outputs into more convenient format
def convert_timeseries(path: Path, type: str, location: str, cluster: int, substrate: int):
    name_in = 'bev_time_series_{}_{}_{}_sr{}.csv'.format(type, location, cluster, substrate) 
    data = pd.read_csv((os.path.join(path, name_in)), sep=',', low_memory=False)
    ncol = len(data.columns)
    nprofiles = int(float((ncol - 1)/3))
    # Generate column separators
    col_sep_1= 1 + nprofiles
    col_sep_2= col_sep_1 + nprofiles
    col_sep_3= col_sep_2 + nprofiles
    # Split the database in three
    data_gd = data.iloc[3:,1:col_sep_1].reset_index(drop=True).astype(float)      
    data_ga = data.iloc[3:,col_sep_1:col_sep_2].reset_index(drop=True).astype(float)   
    data_cd = data.iloc[3:,col_sep_2:col_sep_3].reset_index(drop=True).astype(float)  
    # Create new column for minutes  
    data_gd['minutes'] = data_gd.index 
    data_ga['minutes'] = data_ga.index 
    data_cd['minutes'] = data_cd.index 
    # Create new column for hours
    data_gd['hours'] = (data_gd['minutes'] // 12)+1
    data_ga['hours'] = (data_ga['minutes'] // 12)+1
    data_cd['hours'] = (data_cd['minutes'] // 12)+1
    # Remove minutes column and transpose dataframe
    data_gd = data_gd.groupby('hours').sum().drop(columns='minutes').transpose()
    data_ga = data_ga.groupby('hours').mean().drop(columns='minutes').transpose()
    data_cd = data_cd.groupby('hours').mean().drop(columns='minutes').transpose()
    # Export to csv 
    name_out_gd = "bev_time_series_gd_{}_{}_{}_sr{}.csv".format(type, location, cluster, substrate)
    name_out_ga = "bev_time_series_ga_{}_{}_{}_sr{}.csv".format(type, location, cluster, substrate)
    name_out_cd = "bev_time_series_cd_{}_{}_{}_sr{}.csv".format(type, location, cluster, substrate)
    data_gd.to_csv(os.path.join(path, 'csv', name_out_gd), header=None, index=False) 
    data_ga.to_csv(os.path.join(path, 'csv', name_out_ga), header=None, index=False) 
    data_cd.to_csv(os.path.join(path, 'csv', name_out_cd), header=None, index=False) 
    return 

#%% Compute csv files for all locations and clusters
location =['metro', 'big', 'middle', 'small', 'rural']
cluster =[1, 2, 3, 4]  
# For privately-owned vehicles
for l in location:
    for c in cluster:
        convert_timeseries('/home/agueret/Documents/bev_timeseries/emobpy/export/', 'po', l, c, 0)
#%% For shared vehicles (slow uptake scenario)
location =['metro', 'big', 'middle', 'small']
for l in location:
    convert_timeseries('/home/agueret/Documents/bev_timeseries/emobpy/export', 'cs_slow', l, 4, 5)
# For shared vehicles (fast uptake scenario)
convert_timeseries('/home/agueret/Documents/bev_timeseries/emobpy/export/', 'cs_fast', 'metro', 14, 5)
convert_timeseries('/home/agueret/Documents/bev_timeseries/emobpy/export/', 'cs_fast', 'big', 24, 5)
convert_timeseries('/home/agueret/Documents/bev_timeseries/emobpy/export/', 'cs_fast', 'middle', 34, 5)

#%% Define a function: get the total number of profiles for each setting 
def get_ntotal_profiles(path: Path, type: str, location: str, cluster: int, substrate: int):
    name_in = 'bev_time_series_{}_{}_{}_sr{}.csv'.format(type, location, cluster, substrate) 
    data = pd.read_csv((os.path.join(path, name_in)), sep=',', low_memory=False)
    ncol = len(data.columns)
    nprofiles = int(float((ncol - 1)/3))
    return nprofiles
#%% Relevant lists
location_po =['metro', 'big', 'middle', 'small', 'rural']
location_cs =['metro', 'big', 'middle', 'small']
cluster =[1, 2, 3, 4] 
# Create dataframe for privately-owned vehicles - locations as columns, clusters as rows
nprofiles_po_summary = pd.DataFrame(columns=['metro', 'big', 'middle', 'small', 'rural'], index=[1,2,3,4])
# Fill in the dataframe
for l in location_po:
    for c in cluster:
        nprofiles_po_summary.loc[c, l] = get_ntotal_profiles('/home/agueret/Documents/bev_timeseries/emobpy/export/', 'po', l, c, 0)
nprofiles_po_summary.index = ['cluster1_po', 'cluster2_po', 'cluster3_po', 'cluster4_po'] 
# Create dataframe for shared vehicles (cs slow) - locations as columns, clusters as rows
nprofiles_cs_slow_summary = pd.DataFrame(columns=['metro', 'big', 'middle', 'small', 'rural'], index=[4])
nprofiles_cs_slow_summary.loc[4, 'rural'] = 0
# Fill in the dataframe
for l in location_cs:
    nprofiles_cs_slow_summary.loc[4, l] = get_ntotal_profiles('/home/agueret/Documents/bev_timeseries/emobpy/export/', 'cs_slow', l, 4, 5)
nprofiles_cs_slow_summary.index = ['cluster4_cs_slow_sr5'] 
# Create dataframe for shared vehicles (cs fast) - locations as columns, clusters as rows
nprofiles_cs_fast_summary = pd.DataFrame(columns=['metro', 'big', 'middle', 'small', 'rural'], index=[1])
nprofiles_cs_fast_summary.loc[1, 'rural'] = 0
nprofiles_cs_fast_summary.loc[1, 'small'] = 0
# Fill in the dataframe
cs_fast_dict = {'metro' : 14,
                'big'   : 24,
                'middle': 34} 
for l in cs_fast_dict.keys():
    nprofiles_cs_fast_summary.loc[1, l] = get_ntotal_profiles('/home/agueret/Documents/bev_timeseries/emobpy/export/', 'cs_fast', l, cs_fast_dict[l], 5)
nprofiles_cs_fast_summary.index = ['cluster_cs_fast_sr5'] 
# Collect all three dataframes in one 
nprofiles_summary = pd.concat([nprofiles_po_summary, nprofiles_cs_slow_summary, nprofiles_cs_fast_summary])
# %%
