#%% Load modules
import pandas as pd
import os
from pathlib import Path
#%% >>>> Define work environment (PC or Tesla) <<<<<
work_environment = 'PC' #Need to specify 'PC' or 'Tesla'

path_git = {'PC'    : 'C:\Adeline\Git',
            'Tesla' : '/home/agueret/Documents/Git/'}
#%% Define a function to build the ordered list of shares and battery capacity parameters for each scenario
def collect_parameters(scenario_name: str, csv_file_name: str = 'iteration_data.csv'):
    path = path_git[work_environment]
    data_shares = pd.read_csv(os.path.join(path, 'ecarsharing_project', 'postprocessing', csv_file_name), sep=',')
    data_extract = data_shares[data_shares['scenario']==scenario_name]
    nprofiles_baseline = data_extract.iloc[0]['nprofiles_baseline']
    nprofiles_scen = data_extract.iloc[0]['nprofiles_scen']
    diff = nprofiles_baseline - nprofiles_scen
    ev =['ev{0:03}'.format(x) for x in range(1,nprofiles_baseline+1)]
    df = pd.DataFrame(columns=['share_weighted', 'batt_cap', 'scenario'], index=ev)
    nrows=data_extract.shape[0]
    shares_w = []
    batt_cap = []
    for i in range(nrows):
        s=data_extract.iloc[i]['share_weighted']
        c=data_extract.iloc[i]['battery_capacity']
        nprof=data_extract.iloc[i]['nprofiles']
        for p in range(nprof):
            shares_w.append(s)
            batt_cap.append(c)
    for i in range(diff):
        shares_w.append(0)
        batt_cap.append(0)
    df['share_weighted'] = shares_w
    df['batt_cap'] = batt_cap 
    df['scenario'] = scenario_name
    return df

#%% Generate Excel files
scenarios = ['scen1', 'baseline2', 'scen2', 'baseline3', 'scen3', 'baseline4', 'scen4']
share_list = collect_parameters('baseline')
for s in scenarios:
    n = collect_parameters(s)
    share_list = pd.concat([share_list, n])
share_list.to_excel(os.path.join(path_git[work_environment], 'ecarsharing_project', 'postprocessing', 'share_list.xlsx'), index=True)
# %% Define a function : generate shares and battery capacity data for iteration table 
def iteration_table(nprofiles_baseline):
    phi = ["phi_ev('DE','ev{0:03}')".format(x) for x in range(1,nprofiles_baseline+1)]
    n = ["n_ev_e('DE','ev{0:03}')".format(x) for x in range(1,nprofiles_baseline+1)]
    ind = phi + n
    scenario_name = ['scen1', 'baseline2', 'scen2', 'baseline3', 'scen3', 'baseline4', 'scen4']
    table = pd.DataFrame(columns=[scenario_name], index=[ind])
    data = pd.read_excel(os.path.join(path_git[work_environment], 'ecarsharing_project', 'postprocessing', 'share_list.xlsx'), index_col=[0])
    for (i,j) in enumerate(scenario_name):
        scenario = data[data['scenario']==j]
        shares_w = scenario['share_weighted'].reindex().round(12)
        battery_cap = scenario['batt_cap'].reindex().round(3)
        table.iloc[0:nprofiles_baseline,i] = shares_w
        table.iloc[nprofiles_baseline:(nprofiles_baseline*2)+1,i] = battery_cap
    return table

table = iteration_table(76)
table.to_excel(os.path.join(path_git[work_environment], 'ecarsharing_project', 'postprocessing', 'iteration_table_adj_shares.xlsx'), index=True)
# %%
