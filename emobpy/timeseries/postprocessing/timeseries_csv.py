#%% Load modules
import pandas as pd
import os
import copy
from pathlib import Path

#%% >>>> Define work environment (PC or Tesla) <<<<<
work_environment = 'Tesla' #Need to specify 'PC' or 'Tesla'

path_git = {'PC'    : 'C:\Adeline\Git',
            'Tesla' : '/home/agueret/Documents/Git/'}

#%% Define a function: build the baseline time series input csv file for DIETER  
def create_baseline(ntotal_profiles: int, sample: int, scenario_data_name: str = 'iteration_data.csv', data_input_csv_path: Path = '/home/agueret/Documents/bev_timeseries/emobpy/export/csv'): 
    path = path_git[work_environment]
    all_scenario = pd.read_csv(os.path.join(path, 'ecarsharing_project', 'postprocessing', scenario_data_name), sep=',')
# Create dataframe 
    hours =['t{0:04}'.format(x) for x in range(1,8761)] 
    timeseries_input = pd.DataFrame(columns=[hours], index=[x for x in range(1,(ntotal_profiles*3)+1)])
    param = ['ev_ed', 'ev_ged_exog', 'n_ev_p']
    params = [item for item in param for i in range(ntotal_profiles)] 
    timeseries_input.insert(0, 'headers_time_ev', params)
    timeseries_input.insert(1, 'n', 'DE')
    ev =['ev{0:03}'.format(x) for x in range(1,ntotal_profiles+1)]
    evs= ev * 3
    timeseries_input.insert(2, 'ev', evs)
# Baseline 
    scenario = all_scenario[(all_scenario['scenario']=='baseline') & (all_scenario['nprofiles_baseline']==ntotal_profiles)]
    scenario['nprofiles_cum'] = scenario['nprofiles'].cumsum()
    nrows=scenario.shape[0]
    variables = ['gd','cd', 'ga']
    s = sample
    for (v, var) in enumerate(variables) :     
        for i in range(nrows):
            vehicle_type = scenario.iloc[i]['type']
            location = scenario.iloc[i]['location']
            cluster = scenario.iloc[i]['cluster']
            nprofiles = scenario.iloc[i]['nprofiles']
            sr = scenario.iloc[i]['profile_substrate']
            name_in = 'bev_time_series_{}_{}_{}_{}_sr{}.csv'.format(var, vehicle_type, location, cluster, sr) 
            data = pd.read_csv(os.path.join(data_input_csv_path, name_in), sep=',', low_memory=False, header=None)
            extract = data.iloc[nprofiles*(s-1):nprofiles*s]
            if i==0:
                timeseries_input.iloc[0+ntotal_profiles*v:nprofiles+ntotal_profiles*v,3:] = extract
            else:
                first = scenario.iloc[i-1]['nprofiles_cum'] + ntotal_profiles*v
                last = scenario.iloc[i]['nprofiles_cum'] + ntotal_profiles*v
                timeseries_input.iloc[first:last,3:] = extract
    path_out = '/home/agueret/Documents/bev_timeseries/emobpy/output_for_dieter/'
    name_out = 'ev_time_data_upload_{}_sample{}.csv'.format(ntotal_profiles, sample)
    timeseries_input.to_csv(os.path.join(path_out, name_out), index=False)

#%% Define a function : build iteration_data csv file 
def create_iteration_data_scenario(scenario_name: str, ntotal_profiles: int, substrate: int, sample_po: int, sample_cs: int, scenario_data_name: str='iteration_data.csv', data_input_csv_path: Path = '/home/agueret/Documents/bev_timeseries/emobpy/export/csv'):
    path = path_git[work_environment]
    all_scenario = pd.read_csv(os.path.join(path, 'ecarsharing_project', 'postprocessing', scenario_data_name), sep=',')
    #Create dataframe scen 1
    hours =['t{0:04}'.format(x) for x in range(1,8761)] 
    iteration_data = pd.DataFrame(columns=[hours], index=[x for x in range(1,(ntotal_profiles*3)+1)])
    n_ev_p =["""n_ev_p('DE','ev{0:03}',h)""".format(x) for x in range(1,ntotal_profiles+1)]
    ev_ed =["""ev_ed('DE','ev{0:03}',h)""".format(x) for x in range(1,ntotal_profiles+1)]
    ev_ged_exog =["""ev_ged_exog('DE','ev{0:03}',h)""".format(x) for x in range(1,ntotal_profiles+1)]
    params=  ev_ed + ev_ged_exog + n_ev_p
    iteration_data.insert(0, 'parameter', params)
    n_ev_p_id =["""n_ev_p_de_ev{0:03}_h""".format(x) for x in range(1,ntotal_profiles+1)]
    ev_ed_id =["""ev_ed_de_ev{0:03}_h""".format(x) for x in range(1,ntotal_profiles+1)]
    ev_ged_exog_id =["""ev_ged_exog_de_ev{0:03}_h""".format(x) for x in range(1,ntotal_profiles+1)]
    ids=  ev_ed_id + ev_ged_exog_id + n_ev_p_id
    iteration_data.insert(1, 'identifier', ids)
    iteration_data.insert(2, 'scenario', scenario_name)
    # Import time series data 
    scenario = all_scenario[(all_scenario['scenario']==scenario_name) & (all_scenario['nprofiles_baseline']==ntotal_profiles) & (all_scenario['scen_substrate']==substrate)]
    scenario['nprofiles_cum'] = scenario['nprofiles'].cumsum()
    nrows=scenario.shape[0]
    variables = ['gd','cd', 'ga']
    for (v, var) in enumerate(variables) :     
        for i in range(nrows):
            vehicle_type = scenario.iloc[i]['type']
            location = scenario.iloc[i]['location']
            cluster = scenario.iloc[i]['cluster']
            nprofiles = scenario.iloc[i]['nprofiles']
            sr = scenario.iloc[i]['profile_substrate']
            if vehicle_type == 'po':
                s = sample_po
            else:
                s = sample_cs
            name_in = 'bev_time_series_{}_{}_{}_{}_sr{}.csv'.format(var, vehicle_type, location, cluster, sr) 
            data = pd.read_csv(os.path.join(data_input_csv_path, name_in), sep=',', low_memory=False, header=None)
            extract = data.iloc[nprofiles*(s-1):nprofiles*s]
            if i==0:
                iteration_data.iloc[0+ntotal_profiles*v:nprofiles+ntotal_profiles*v,3:] = extract
            else:
                first = scenario.iloc[i-1]['nprofiles_cum'] + ntotal_profiles*v
                last = scenario.iloc[i]['nprofiles_cum'] + ntotal_profiles*v
                if last == scenario.iloc[nrows-1]['nprofiles_cum']:
                    if last == ntotal_profiles: 
                        iteration_data.iloc[first:last,3:] = extract
                    else:
                        iteration_data.iloc[first:last,3:] = extract
                        iteration_data.iloc[last:nprofiles+1,3:] = 0
                else:
                    iteration_data.iloc[first:last,3:] = extract
            iteration_data = iteration_data.fillna(0)          
    return iteration_data
#%% Define a function: append iteration data with flat hydrogen demand 
def append_iteration_data_h2_demand(data: pd.DataFrame, yearly_h2_demand: int, scenario_name: str):
    new_h2_row = [yearly_h2_demand * 1000000 / 8760] * 8760
    new_h2_row.insert(0, """d_h2_parsimonious('DE',h)""")
    new_h2_row.insert(1, 'h2_demand_de_h')
    new_h2_row.insert(2, scenario_name) 
    df = pd.DataFrame(data)
    df.loc[len(df)+1] = new_h2_row
    return df

#%% Define a function: append iteration data with one week of energy drought
def append_iteration_data_energydrought(data: pd.DataFrame, hstart_drought: int, hend_drought:int, scenario_name: str):
    path = path_git[work_environment]
    phires_data_ed = pd.read_csv(os.path.join(path, 'ecarsharing_project', 'postprocessing', 'energydrought.csv'), sep=',')
    phires_data_ed.iloc[:,2] = scenario_name
    phires_data_ed.iloc[:, hstart_drought+3:hend_drought+4] = 0
    df = pd.DataFrame(data)
    df.loc[len(df)+1] = phires_data_ed.iloc[0,:].values.tolist()
    df.loc[len(df)+1] = phires_data_ed.iloc[1,:].values.tolist()
    df.loc[len(df)+1] = phires_data_ed.iloc[2,:].values.tolist()
    return df
#%% Define a function: append iteration data with PECD time series for pv, wind, ror
def append_iteration_data_pecd(data: pd.DataFrame, scenario_name: str, year: int):
    path = path_git[work_environment]
    data_year = 'weatherdata_pecd{}.csv'.format(year)
    pecd_weather_data = pd.read_csv(os.path.join(path, 'ecarsharing_project', 'postprocessing', data_year), sep=';')
    pecd_weather_data.iloc[:,2] = scenario_name
    df = pd.DataFrame(data)
    df.loc[len(df)+1] = pecd_weather_data.iloc[0,:].values.tolist()
    df.loc[len(df)+1] = pecd_weather_data.iloc[1,:].values.tolist()
    df.loc[len(df)+1] = pecd_weather_data.iloc[2,:].values.tolist()  
    df.loc[len(df)+1] = pecd_weather_data.iloc[3,:].values.tolist()      
    return df

#%% Generate the baseline EV time series csv file
create_baseline(76, 1)
#%% Generate data for one sample only
scenarios = {'baseline2' : 0, 
             'scen2' : 5, 
             'baseline3' : 0, 
             'scen3' : 5, 
             'baseline4' : 0, 
             'scen4' : 5}

iteration_data_76_sr5_sample1 = create_iteration_data_scenario('scen1', ntotal_profiles=76, substrate=5, sample_po=1, sample_cs=1)
for (s, subst) in scenarios.items():
    df = create_iteration_data_scenario(s, 76, subst, 1, 1)
    iteration_data_76_sr5_sample1 = pd.concat([iteration_data_76_sr5_sample1, df])
iteration_data_76_sr5_sample1.to_csv(os.path.join('/home/agueret/Documents/bev_timeseries/emobpy/output_for_dieter/', 'iteration_data_76_sr5_sample1.csv'), index=False)

#%% Generate iteration data with non-zero flat annual hydrogen demand for some scenarios
scenarios_h2 = {'baseline2' : 0, 
                'scen2' : 5, 
                'baseline3' : 0, 
                'scen3' : 5, 
                'baseline4' : 0, 
                'scen4' : 5}

rename_scenarios = {'scen1'     : 'scen5', 
                        'baseline2' : 'baseline6', 
                        'scen2'     : 'scen6',
                        'baseline3' : 'baseline7',
                        'scen3'     : 'scen7', 
                        'baseline4' : 'baseline8', 
                        'scen4'     : 'scen8'}

iteration_data_76_sr5_sample1_h2 = append_iteration_data_h2_demand(create_iteration_data_scenario('scen1', ntotal_profiles=76, substrate=5, sample_po=1, sample_cs=1), 30, 'scen1')
for (s, subst) in scenarios_h2.items():
    df = append_iteration_data_h2_demand(create_iteration_data_scenario(s, 76, subst, 1, 1), 30, s)
    iteration_data_76_sr5_sample1_h2 = pd.concat([iteration_data_76_sr5_sample1_h2, df])
iteration_data_76_sr5_sample1_h2.replace(rename_scenarios, inplace=True)
#Add the hydrogen demand for the baseline run
new_h2_row = [30 * 1000000 / 8760] * 8760
new_h2_row.insert(0, """d_h2_parsimonious('DE',h)""")
new_h2_row.insert(1, 'h2_demand_de_h')
new_h2_row.insert(2, 'baseline5') 
iteration_data_76_sr5_sample1_h2.loc[len(iteration_data_76_sr5_sample1_h2)+1] = new_h2_row
iteration_data_76_sr5_sample1_h2.to_csv(os.path.join('/home/agueret/Documents/bev_timeseries/emobpy/output_for_dieter/', 'iteration_data_76_sr5_sample1_h2.csv'), index=False)

#%% Generate iteration data with energy drought week
path = path_git[work_environment]
scenarios_ed = {'baseline2' : 0, 
                'scen2' : 5, 
                'baseline3' : 0, 
                'scen3' : 5, 
                'baseline4' : 0, 
                'scen4' : 5}

iteration_data_76_sr5_sample1_ed = append_iteration_data_energydrought(create_iteration_data_scenario('scen1', ntotal_profiles=76, substrate=5, sample_po=1, sample_cs=1), 7848, 8016, 'scen1')
for (s, subst) in scenarios_ed.items():
    df = append_iteration_data_energydrought(create_iteration_data_scenario(s, 76, subst, 1, 1), 7848, 8016, s)
    iteration_data_76_sr5_sample1_ed = pd.concat([iteration_data_76_sr5_sample1_ed, df])
# Add energy drought time series for the baseline 
phires_data_ed = pd.read_csv(os.path.join(path, 'ecarsharing_project', 'postprocessing', 'energydrought.csv'), sep=',')
phires_data_ed.iloc[:,2] = 'baseline'
phires_data_ed.iloc[:, 7848+3:8016+4] = 0
df = pd.DataFrame(iteration_data_76_sr5_sample1_ed)
df.loc[len(df)+1] = phires_data_ed.iloc[0,:].values.tolist()
df.loc[len(df)+1] = phires_data_ed.iloc[1,:].values.tolist()
df.loc[len(df)+1] = phires_data_ed.iloc[2,:].values.tolist()
# Export csv file
df.to_csv(os.path.join('/home/agueret/Documents/bev_timeseries/emobpy/output_for_dieter/', 'iteration_data_76_sr5_sample1_ed.csv'), index=False)

#%% Generate iteration data with PECD 2016 time series for pv, wind onshore and offshore, run-of-river
path = path_git[work_environment]
scenarios_pecd = {'baseline2' : 0, 
                'scen2' : 5, 
                'baseline3' : 0, 
                'scen3' : 5, 
                'baseline4' : 0, 
                'scen4' : 5}

iteration_data_76_sr5_sample1_pecd2016 = append_iteration_data_pecd(create_iteration_data_scenario('scen1', ntotal_profiles=76, substrate=5, sample_po=1, sample_cs=1), 'scen1', 2016)
for (s, subst) in scenarios_pecd.items():
    df = append_iteration_data_pecd(create_iteration_data_scenario(s, 76, subst, 1, 1), s, 2016)
    iteration_data_76_sr5_sample1_pecd2016 = pd.concat([iteration_data_76_sr5_sample1_pecd2016, df])
# Add PECD 2016 time series for the baseline 
pecd_weather_data = pd.read_csv(os.path.join(path, 'ecarsharing_project', 'postprocessing', 'weatherdata_pecd2016.csv'), sep=';')
pecd_weather_data.iloc[:,2] = 'baseline'
df = pd.DataFrame(iteration_data_76_sr5_sample1_pecd2016)
df.loc[len(df)+1] = pecd_weather_data.iloc[0,:].values.tolist()
df.loc[len(df)+1] = pecd_weather_data.iloc[1,:].values.tolist()
df.loc[len(df)+1] = pecd_weather_data.iloc[2,:].values.tolist()
df.loc[len(df)+1] = pecd_weather_data.iloc[3,:].values.tolist()
# Export csv file
df.to_csv(os.path.join('/home/agueret/Documents/bev_timeseries/emobpy/output_for_dieter/', 'iteration_data_76_sr5_sample1_pecd2016.csv'), index=False)

#%% Generate iteration data for weather sensitivity runs 
path = path_git[work_environment]
years = [1996, 2000, 2003, 2006, 2009, 2012, 2015, 2017]
years_addition =[1997, 1998, 1999, 2001, 2002, 2004, 2005, 2007, 2008, 2010, 2011, 2013, 2014] 
for y in years_addition:
    iteration_data_76_sr5_sample1_year = append_iteration_data_pecd(create_iteration_data_scenario('baseline4', ntotal_profiles=76, substrate=0, sample_po=1, sample_cs=1), 'baseline4', y)
    df = append_iteration_data_pecd(create_iteration_data_scenario('scen4', 76, 5, 1, 1), 'scen4', y)
    iteration_data_76_sr5_sample1_year = pd.concat([iteration_data_76_sr5_sample1_year, df])
    name_export = 'iteration_data_76_sr5_sample1_pecd{}.csv'.format(y)
    iteration_data_76_sr5_sample1_year.to_csv(os.path.join('/home/agueret/Documents/bev_timeseries/emobpy/output_for_dieter/', name_export), index=False)


#%% Generation iteration data for driving electricity consumption sensitivity runs 
path = path_git[work_environment]
list_derate = ['ev_ed_de_ev037_h', 'ev_ed_de_ev038_h', 'ev_ed_de_ev039_h', 'ev_ed_de_ev040_h', 'ev_ed_de_ev041_h',
               'ev_ed_de_ev042_h', 'ev_ed_de_ev043_h', 'ev_ed_de_ev044_h', 'ev_ed_de_ev045_h', 'ev_ed_de_ev046_h',
               'ev_ed_de_ev047_h', 'ev_ed_de_ev048_h', 'ev_ed_de_ev049_h', 'ev_ed_de_ev050_h'] 

iteration_data_76_sr5_sample1_eved = create_iteration_data_scenario('baseline4', ntotal_profiles=76, substrate=0, sample_po=1, sample_cs=1)
df = create_iteration_data_scenario('scen4', 76, 5, 1, 1)
iteration_data_76_sr5_sample1_eved = pd.concat([iteration_data_76_sr5_sample1_eved, df])
iteration_data_76_sr5_sample1_eved.to_csv(os.path.join('/home/agueret/Documents/bev_timeseries/emobpy/output_for_dieter/', 'iteration_data_76_sr5_sample1_eved.csv'), index=False)

df_import = pd.read_csv(os.path.join('/home/agueret/Documents/bev_timeseries/emobpy/output_for_dieter/', 'iteration_data_76_sr5_sample1_eved.csv'), sep=',')
row_index = df_import[(df_import['identifier'].isin(list_derate))&(df_import['scenario']=='scen4')].index.to_list()
df_import.iloc[row_index,3:] = df_import.iloc[row_index,3:] * 0.9
df_import.to_csv(os.path.join('/home/agueret/Documents/bev_timeseries/emobpy/output_for_dieter/', 'iteration_data_76_sr5_sample1_eved_derated.csv'), index=False)

#%% Generate the iteration_table for each separate scenario 
# Scenario 1, nprofiles=70, subsrate=5
iteration_data_scen1_70_sr5_sample1 = create_iteration_data_scenario('scen1', ntotal_profiles=70, substrate=5, sample_po=1, sample_cs=1)
iteration_data_scen1_70_sr5_sample2 = create_iteration_data_scenario('scen1', ntotal_profiles=70, substrate=5, sample_po=1, sample_cs=2)
iteration_data_scen1_70_sr5_sample2 = iteration_data_scen1_70_sr5_sample2.replace('scen1', 'scen12')
iteration_data_scen1_70_sr5_sample3 = create_iteration_data_scenario('scen1', ntotal_profiles=70, substrate=5, sample_po=1, sample_cs=3)
iteration_data_scen1_70_sr5_sample3 = iteration_data_scen1_70_sr5_sample3.replace('scen1', 'scen13')
iteration_data_scen1_70_sr5_sample4 = create_iteration_data_scenario('scen1', ntotal_profiles=70, substrate=5, sample_po=1, sample_cs=4)
iteration_data_scen1_70_sr5_sample4 = iteration_data_scen1_70_sr5_sample4.replace('scen1', 'scen14')
iteration_data_scen1_70_sr5_sample5 = create_iteration_data_scenario('scen1', ntotal_profiles=70, substrate=5, sample_po=1, sample_cs=5)
iteration_data_scen1_70_sr5_sample5 = iteration_data_scen1_70_sr5_sample5.replace('scen1', 'scen15')
iteration_data_scen1_70_sr5_sample6 = create_iteration_data_scenario('scen1', ntotal_profiles=70, substrate=5, sample_po=1, sample_cs=6)
iteration_data_scen1_70_sr5_sample6 = iteration_data_scen1_70_sr5_sample6.replace('scen1', 'scen16')
iteration_data_scen1_70_sr5_sample7 = create_iteration_data_scenario('scen1', ntotal_profiles=70, substrate=5, sample_po=1, sample_cs=7)
iteration_data_scen1_70_sr5_sample7 = iteration_data_scen1_70_sr5_sample7.replace('scen1', 'scen17')
iteration_data_scen1_70_sr5_sample8 = create_iteration_data_scenario('scen1', ntotal_profiles=70, substrate=5, sample_po=1, sample_cs=8)
iteration_data_scen1_70_sr5_sample8 = iteration_data_scen1_70_sr5_sample8.replace('scen1', 'scen18')
iteration_data_scen1_70_sr5_sample9 = create_iteration_data_scenario('scen1', ntotal_profiles=70, substrate=5, sample_po=1, sample_cs=9)
iteration_data_scen1_70_sr5_sample9 = iteration_data_scen1_70_sr5_sample9.replace('scen1', 'scen19')
iteration_data_scen1_70_sr5_sample10 = create_iteration_data_scenario('scen1', ntotal_profiles=70, substrate=5, sample_po=1, sample_cs=10)
iteration_data_scen1_70_sr5_sample10 = iteration_data_scen1_70_sr5_sample10.replace('scen1', 'scen110')

# Baseline 2
iteration_data_base2_70_sr5_sample1 = create_iteration_data_scenario('baseline2', 70, 0, 1, 1)

# Scenario 2, nprofiles=70, subsrate=5
iteration_data_scen2_70_sr5_sample1 = create_iteration_data_scenario('scen2', 70, 5, 1, 1)
iteration_data_scen2_70_sr5_sample2 = create_iteration_data_scenario('scen2', 70, 5, 1, 2)
iteration_data_scen2_70_sr5_sample2 = iteration_data_scen2_70_sr5_sample2.replace('scen2', 'scen22')
iteration_data_scen2_70_sr5_sample3 = create_iteration_data_scenario('scen2', 70, 5, 1, 3)
iteration_data_scen2_70_sr5_sample3 = iteration_data_scen2_70_sr5_sample3.replace('scen2', 'scen23')
iteration_data_scen2_70_sr5_sample4 = create_iteration_data_scenario('scen2', 70, 5, 1, 4)
iteration_data_scen2_70_sr5_sample4 = iteration_data_scen2_70_sr5_sample4.replace('scen2', 'scen24')
iteration_data_scen2_70_sr5_sample5 = create_iteration_data_scenario('scen2', 70, 5, 1, 5)
iteration_data_scen2_70_sr5_sample5 = iteration_data_scen2_70_sr5_sample5.replace('scen2', 'scen25')
iteration_data_scen2_70_sr5_sample6 = create_iteration_data_scenario('scen2', 70, 5, 1, 6)
iteration_data_scen2_70_sr5_sample6 = iteration_data_scen2_70_sr5_sample6.replace('scen2', 'scen26')
iteration_data_scen2_70_sr5_sample7 = create_iteration_data_scenario('scen2', 70, 5, 1, 7)
iteration_data_scen2_70_sr5_sample7 = iteration_data_scen2_70_sr5_sample7.replace('scen2', 'scen27')
iteration_data_scen2_70_sr5_sample8 = create_iteration_data_scenario('scen2', 70, 5, 1, 8)
iteration_data_scen2_70_sr5_sample8 = iteration_data_scen2_70_sr5_sample8.replace('scen2', 'scen28')
iteration_data_scen2_70_sr5_sample9 = create_iteration_data_scenario('scen2', 70, 5, 1, 9)
iteration_data_scen2_70_sr5_sample9 = iteration_data_scen2_70_sr5_sample9.replace('scen2', 'scen29')
iteration_data_scen2_70_sr5_sample10 = create_iteration_data_scenario('scen2', 70, 5, 1, 10)
iteration_data_scen2_70_sr5_sample10 = iteration_data_scen2_70_sr5_sample10.replace('scen2', 'scen210')

# Baseline 3
iteration_data_base3_70_sr5_sample1 = create_iteration_data_scenario('baseline3', 70, 0, 1, 1)

# Scenario 3, nprofiles=70, subsrate=5
iteration_data_scen3_70_sr5_sample1 = create_iteration_data_scenario('scen3', 70, 5, 1, 1)
iteration_data_scen3_70_sr5_sample2 = create_iteration_data_scenario('scen3', 70, 5, 1, 2)
iteration_data_scen3_70_sr5_sample2 = iteration_data_scen3_70_sr5_sample2.replace('scen3', 'scen32')
iteration_data_scen3_70_sr5_sample3 = create_iteration_data_scenario('scen3', 70, 5, 1, 3)
iteration_data_scen3_70_sr5_sample3 = iteration_data_scen3_70_sr5_sample3.replace('scen3', 'scen33')
iteration_data_scen3_70_sr5_sample4 = create_iteration_data_scenario('scen3', 70, 5, 1, 4)
iteration_data_scen3_70_sr5_sample4 = iteration_data_scen3_70_sr5_sample4.replace('scen3', 'scen34')
iteration_data_scen3_70_sr5_sample5 = create_iteration_data_scenario('scen3', 70, 5, 1, 5)
iteration_data_scen3_70_sr5_sample5 = iteration_data_scen3_70_sr5_sample5.replace('scen3', 'scen35')
iteration_data_scen3_70_sr5_sample6 = create_iteration_data_scenario('scen3', 70, 5, 1, 6)
iteration_data_scen3_70_sr5_sample6 = iteration_data_scen3_70_sr5_sample6.replace('scen3', 'scen36')
iteration_data_scen3_70_sr5_sample7 = create_iteration_data_scenario('scen3', 70, 5, 1, 7)
iteration_data_scen3_70_sr5_sample7 = iteration_data_scen3_70_sr5_sample7.replace('scen3', 'scen37')
iteration_data_scen3_70_sr5_sample8 = create_iteration_data_scenario('scen3', 70, 5, 1, 8)
iteration_data_scen3_70_sr5_sample8 = iteration_data_scen3_70_sr5_sample8.replace('scen3', 'scen38')
iteration_data_scen3_70_sr5_sample9 = create_iteration_data_scenario('scen3', 70, 5, 1, 9)
iteration_data_scen3_70_sr5_sample9 = iteration_data_scen3_70_sr5_sample9.replace('scen3', 'scen39')
iteration_data_scen3_70_sr5_sample10 = create_iteration_data_scenario('scen3', 70, 5, 1, 10)
iteration_data_scen3_70_sr5_sample10 = iteration_data_scen3_70_sr5_sample10.replace('scen3', 'scen310')

# Baseline 4
iteration_data_base4_70_sr5_sample1 = create_iteration_data_scenario('baseline4', 70, 0, 1, 1)

# Scenario 4, nprofiles=70, subsrate=5
iteration_data_scen4_70_sr5_sample1 = create_iteration_data_scenario('scen4', 70, 5, 1, 1)
iteration_data_scen4_70_sr5_sample2 = create_iteration_data_scenario('scen4', 70, 5, 1, 2)
iteration_data_scen4_70_sr5_sample2 = iteration_data_scen4_70_sr5_sample2.replace('scen4', 'scen42')
iteration_data_scen4_70_sr5_sample3 = create_iteration_data_scenario('scen4', 70, 5, 1, 3)
iteration_data_scen4_70_sr5_sample3 = iteration_data_scen4_70_sr5_sample3.replace('scen4', 'scen43')
iteration_data_scen4_70_sr5_sample4 = create_iteration_data_scenario('scen4', 70, 5, 1, 4)
iteration_data_scen4_70_sr5_sample4 = iteration_data_scen4_70_sr5_sample4.replace('scen4', 'scen44')
iteration_data_scen4_70_sr5_sample5 = create_iteration_data_scenario('scen4', 70, 5, 1, 5)
iteration_data_scen4_70_sr5_sample5 = iteration_data_scen4_70_sr5_sample5.replace('scen4', 'scen45')
iteration_data_scen4_70_sr5_sample6 = create_iteration_data_scenario('scen4', 70, 5, 1, 6)
iteration_data_scen4_70_sr5_sample6 = iteration_data_scen4_70_sr5_sample6.replace('scen4', 'scen46')
iteration_data_scen4_70_sr5_sample7 = create_iteration_data_scenario('scen4', 70, 5, 1, 7)
iteration_data_scen4_70_sr5_sample7 = iteration_data_scen4_70_sr5_sample7.replace('scen4', 'scen47')
iteration_data_scen4_70_sr5_sample8 = create_iteration_data_scenario('scen4', 70, 5, 1, 8)
iteration_data_scen4_70_sr5_sample8 = iteration_data_scen4_70_sr5_sample8.replace('scen4', 'scen48')
iteration_data_scen4_70_sr5_sample9 = create_iteration_data_scenario('scen4', 70, 5, 1, 9)
iteration_data_scen4_70_sr5_sample9 = iteration_data_scen4_70_sr5_sample9.replace('scen4', 'scen49')
iteration_data_scen4_70_sr5_sample10 = create_iteration_data_scenario('scen4', 70, 5, 1, 10)
iteration_data_scen4_70_sr5_sample10 = iteration_data_scen4_70_sr5_sample10.replace('scen4', 'scen410')

# Build iteration_data file with all three scenarios 
iteration_data_70_sr5 = pd.concat([iteration_data_scen1_70_sr5_sample1, 
                                           iteration_data_scen1_70_sr5_sample2, 
                                           iteration_data_scen1_70_sr5_sample3, 
                                           iteration_data_scen1_70_sr5_sample4,
                                           iteration_data_scen1_70_sr5_sample5,
                                           iteration_data_scen1_70_sr5_sample6,
                                           iteration_data_scen1_70_sr5_sample7,
                                           iteration_data_scen1_70_sr5_sample8,
                                           iteration_data_scen1_70_sr5_sample9, 
                                           iteration_data_scen1_70_sr5_sample10, 
                                           iteration_data_base2_70_sr5_sample1, 
                                           iteration_data_scen2_70_sr5_sample1,
                                           iteration_data_scen2_70_sr5_sample2,
                                           iteration_data_scen2_70_sr5_sample3,
                                           iteration_data_scen2_70_sr5_sample4,
                                           iteration_data_scen2_70_sr5_sample5,
                                           iteration_data_scen2_70_sr5_sample6,
                                           iteration_data_scen2_70_sr5_sample7,
                                           iteration_data_scen2_70_sr5_sample8,
                                           iteration_data_scen2_70_sr5_sample9, 
                                           iteration_data_scen2_70_sr5_sample10, 
                                           iteration_data_base3_70_sr5_sample1, 
                                           iteration_data_scen3_70_sr5_sample1, 
                                           iteration_data_scen3_70_sr5_sample2, 
                                           iteration_data_scen3_70_sr5_sample3, 
                                           iteration_data_scen3_70_sr5_sample4, 
                                           iteration_data_scen3_70_sr5_sample5, 
                                           iteration_data_scen3_70_sr5_sample6, 
                                           iteration_data_scen3_70_sr5_sample7, 
                                           iteration_data_scen3_70_sr5_sample8, 
                                           iteration_data_scen3_70_sr5_sample9, 
                                           iteration_data_scen3_70_sr5_sample10, 
                                           iteration_data_base4_70_sr5_sample1, 
                                           iteration_data_scen4_70_sr5_sample1,
                                           iteration_data_scen4_70_sr5_sample2,
                                           iteration_data_scen4_70_sr5_sample3,
                                           iteration_data_scen4_70_sr5_sample4,
                                           iteration_data_scen4_70_sr5_sample5,
                                           iteration_data_scen4_70_sr5_sample6,
                                           iteration_data_scen4_70_sr5_sample7,
                                           iteration_data_scen4_70_sr5_sample8,
                                           iteration_data_scen4_70_sr5_sample9,
                                           iteration_data_scen4_70_sr5_sample10])
# Export to csv
iteration_data_70_sr5.to_csv(os.path.join('/home/agueret/Documents/bev_timeseries/emobpy/output_for_dieter/', 'iteration_data_70_sr5.csv'), index=False)
