# Not flexible enough? Impacts of electric carsharing on a power sector with variable renewables

This repository contains data and code from the paper "Impacts of electric carsharing on a power sector with variable renewables" published in _Cell Reports Sustainability_ and available here: [10.1016/j.crsus.2024.100241](url). For more information on the folder structure and content, please refer to the Wiki of this project. 

## Authors and acknowledgment
This project has been developed by Adeline Guéret, Wolf-Peter Schill and Carlos Gaete-Morales of the Transformation of the Energy Economy research unit of the Energy, Transport, Environment department at DIW Berlin.

## Contact
For further information, you can contact us at agueret@diw.de

## License
The project is under a CC-BY 4.0 license.